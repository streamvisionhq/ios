//
//  UserSettingViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
    UserSettingViewController is used for login with authontication functionality from MainPage setting menu.
                        ------
 */

#import <UIKit/UIKit.h>

@protocol UserSettingDelegate;

@interface UserSettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *update;
-(void )doLogin;

@property (nonatomic,strong) id<UserSettingDelegate> delegate;

@end

@protocol UserSettingDelegate <NSObject>

@optional
-(void) launchDeviceSelection;

@end
