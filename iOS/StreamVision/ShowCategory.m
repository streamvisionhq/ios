//
//  ShowCategory.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ShowCategory.h"
#import "Show.h"


@implementation ShowCategory

@dynamic categoryName;
@dynamic shows;

@end
