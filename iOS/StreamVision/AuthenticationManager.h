//
//  AuthenticationManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 17/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
            AuthenticationManager is used to authonticate CustomerId.
                        ------                                                    
 */

#import <Foundation/Foundation.h>

@interface AuthenticationManager : NSObject

+(instancetype) sharedInstance;


-(void) getRegisteredDevicesForId:(NSString *) customerId Pass:(NSString *)pass onSuccess:(void(^)(NSArray *devicesList))completionBlock
             onFailure: (ErrorBlock)failureBlock;


-(void)setCallingViewControllerObj:(UIViewController*)controllerObj;
@end
