//
//  ChannelParser.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelParser : NSObject

-(BOOL) parseChannelResponse:(NSData *) channelData;

@end
