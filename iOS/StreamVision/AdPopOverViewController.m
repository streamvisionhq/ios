
//
//  AdPopOverViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AdPopOverViewController.h"

@interface AdPopOverViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *adImageView;

@end

@implementation AdPopOverViewController

@synthesize adURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.adImageView setImage:[UIImage imageNamed:@"Ad"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)goToAdPage:(id)sender {
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.adURL]]) {
        NSLog(@"Failed to open url:");
    }
}

@end
