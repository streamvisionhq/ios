//
//  ProgramGuideCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  The cell provides two pre-defined layouts.
 */
typedef NS_ENUM(NSUInteger, ProgramGuideCellStyle){
    /**
     *  Default style, it will make titleLabel as wide as possible. DetailLabel is also as wide as possible and is located und. The imageView is still in the view hierarchy but with hidden (size 0,0)
     */
    ProgramGuideCellStyleLabelsOnly,
    /**
     *  Icon to the left, titleLabel and detailLabel made narrower to make room for it
     */
    ProgramGuideCellStyleLabelsAndIcon
};

/**
 *  Cell to be consumed by the Program Guide. In similar fashion to UITableViewCell, the cell an be set up with two different styles. It uses autolayout.
 */
@interface ProgramGuideCell : UICollectionViewCell
/**
 *  Title Label, this will probably be used in all cases.
 */
@property (nonatomic, strong, readonly) UILabel *titleLabel;
/**
 *  Detail label, secondary label
 */
@property (nonatomic, strong, readonly) UILabel *detailLabel;
/**
 *  Image view, always present in the cell's view hierarchy, however it will only be displayed if the style is ProgramGuideCellStyleIcon
 */
@property (nonatomic, strong, readonly) UIImageView *imageView;
/**
 *  The cell style.
 */
@property (nonatomic, assign) ProgramGuideCellStyle cellStyle;

/**
 *  A constant string that identifies the cell class uniquely
 *
 *  @return a string that can be used as reuse identifier
 */
+ (NSString *) cellReuseIdentifier;
@end
