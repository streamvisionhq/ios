//
//  ChannelParser.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ChannelParser.h"
#import "CoreDataManager.h"
#import "StreamVisionParserConstants.h"

@implementation ChannelParser

-(BOOL ) parseChannelResponse:(NSData *) channelData{

    
    CoreDataManager *coreDataManager = [CoreDataManager sharedInstance];
    
    [coreDataManager deleteAllChannelsData];
    
    NSError *error;
    NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData:channelData options:kNilOptions error:&error];
    
    NSArray *allChannels = nil;
    BOOL success = [[JSONResponseDict valueForKey:kSuccessKey] boolValue];
    
    if (success) {
        allChannels = [JSONResponseDict valueForKey:kDataKey];
       [coreDataManager createChannelsWithData:allChannels];
         return YES;
        
    }else{
        
        return NO;
        
    }

    
   
    
}

@end
