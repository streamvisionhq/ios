//
//  User.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 11/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * registrationNumber;
@property (nonatomic, retain) NSString * serviceProvider;

@end
