//
//  ProgramGuideTimeRulerCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramGuideTimeRulerCell : UICollectionReusableView
@property (nonatomic, strong, readonly) UILabel *label;

/**
 *  A constant string that identifies the cell class uniquely
 *
 *  @return a string that can be used as reuse identifier
 */
+ (NSString *) cellReuseIdentifier;
@end
