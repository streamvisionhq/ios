//
//  Device.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Device.h"


@implementation Device

@dynamic deviceIdentifier;
@dynamic deviceLocation;
@dynamic deviceServer;
@dynamic deviceType;
@dynamic isOnline;
@dynamic rokuDeviceId;
@dynamic serviceType;
@dynamic isRegistered;
@dynamic accountId;

@end
