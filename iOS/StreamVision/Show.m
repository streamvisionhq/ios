//
//  Show.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 31/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Show.h"
#import "Audio.h"
#import "Channel.h"
#import "ClosedCaption.h"
#import "Episode.h"
#import "Rating.h"
#import "ShowCategory.h"


@implementation Show

@dynamic adjustedStartTime;
@dynamic cast;
@dynamic cc;
@dynamic displayGenre;
@dynamic duration;
@dynamic endTimeUTC;
@dynamic eventId;
@dynamic isHd;
@dynamic originalAirDate;
@dynamic parentProgramId;
@dynamic programId;
@dynamic showDescription;
@dynamic showType;
@dynamic startTimeUtc;
@dynamic title;
@dynamic year;
@dynamic videoURL;
@dynamic audio;
@dynamic categories;
@dynamic channel;
@dynamic closedCaption;
@dynamic episodes;
@dynamic rating;

@end
