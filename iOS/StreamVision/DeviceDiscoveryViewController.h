//
//  DeviceDiscoveryViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 23/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
 DeviceDiscoveryViewController is used for displaying all devices with status when logged in.
                        ------
 */

#import <UIKit/UIKit.h>
#import <ConnectSDK/ConnectSDK.h>

@interface DeviceDiscoveryViewController : UIViewController 

@end
