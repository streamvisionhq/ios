//
//  MainViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "MainViewController.h"
#import "ControlTouchpadView.h"
#import "ChannelsTableViewController.h"
#import "AppsCustomLayout.h"
#import "AppCollectionViewCell.h"
#import "AppCollectionHeader.h"
#import "UIImageView+WebCache.h"
#import "ChannelServiceManager.h"
#import <CoreLocation/CoreLocation.h>
#import "EPGViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
#import "SVNetworkCheck.h"
#import "ServiceProviderManager.h"

#import "AdPopOverViewController.h"
#import "CoreDataManager.h"
#import "SVConnectedDevicesManager.h"
#import "UserSettingViewController.h"
#import "RokuListingViewController.h"
#import "StreamVisionDeviceManager.h"
#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en1"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"
#define LISTEN_PORT 10000
#define ADTIMERCOUNT 30
#define POLLTIMEOUT 10


typedef enum
{
    FivewayKeyUp = 0,
    FivewayKeyDown,
    FivewayKeyLeft,
    FivewayKeyRight,
} FivewayKey;

#define FIVEWAY_DELAY 0.2

static NSString * const reuseIdentifierCell = @"AppCell";
static NSString * const reuseIdentifierHeader = @"AppHeader";

@interface MainViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate,AppCollectionHeaderDelegate,ChannelsTableVCDelegate,UIPopoverControllerDelegate,UserSettingDelegate,EPGLaunchChannelDelegate>{
    
    NSMutableArray *_appList;
    int _currentApp;
    
    NSTimer *_timer;

    
    ServiceSubscription *_runningAppSubscription;
    LaunchSession *_myAppSession;
    ServiceSubscription *_volumeSubscription;
    CLLocationManager *locationManager;
   
}

@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *clickButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIButton *starButton;


//Video controls
@property (weak, nonatomic) IBOutlet UIButton *prevButton;
@property (weak, nonatomic) IBOutlet UIButton *rewindButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;


@property (weak, nonatomic) IBOutlet ControlTouchpadView *touchpad;
@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UIButton *userChannelMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *overAirMenuButton;

@property (weak, nonatomic) IBOutlet UIView *currentProgramContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *currentImageView;
@property (weak, nonatomic) IBOutlet UITextView *currentDescription;

//top bar


@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;

@property (weak, nonatomic) IBOutlet UILabel *buildLabel;

@property(nonatomic,strong) ChannelsTableViewController *channelsTableViewController;

@property (weak, nonatomic) IBOutlet UICollectionView *appsCollectionView;

@property (nonatomic,strong) NSString *deviceTargetURL,*locationNameStr;
@property BOOL isPaused;
@property BOOL listenForConnections;
@property BOOL userChangedId;
@property BOOL isDisconnected;

- (IBAction)instantplay:(id)sender;
- (IBAction)info:(id)sender;

@property (nonatomic,strong) NSString *todayString;
@property (nonatomic,strong) NSString *temperatureStrng;
@property (weak, nonatomic) IBOutlet UIImageView *streamingNowImageView;

@property (nonatomic,strong) AppInfo *svApp;

@property BOOL svAppIsStreaming;
@property (nonatomic,strong) NSString *ipAddress;

@property (nonatomic,strong) NSString *adRedirectURL;
@property (nonatomic,strong) UIPopoverController *adPopOverController;
@property (nonatomic,strong) AdPopOverViewController *adVC;
@property (nonatomic,strong) NSTimer *adTimer;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property NSUInteger timeoutCounter;
@property (nonatomic,strong) SVConnectedDevicesManager *svDeviceManager;
@property BOOL newUser;

@property (nonatomic,strong) NSTimer *pollTimer;
@property NSUInteger pollTimerCount;
@property (nonatomic,strong) StreamVisionDeviceManager *pollDeviceManager;
@property (nonatomic,strong) EPGViewController * epgViewController;
@property (nonatomic,strong) NSDate *currentDate;
@property BOOL isEpg;
@property BOOL isDateChanged;
@end

@implementation MainViewController

@synthesize device = _device,connectButtonTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.svDeviceManager = [SVConnectedDevicesManager sharedInstance];
    self.svAppIsStreaming = NO;
    self.newUser = NO;
    self.userChangedId = NO;
    self.isDisconnected = NO;
    self.epgViewController = NULL;
     // Do any additional setup after loading the view.
    self.currentDate = [NSDate date];

    self.connectButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    self.connectButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self updateConnectButtonLabel];
    
   self.ipAddress = [self getIPAddress:YES];
    
    self.isPaused = YES;
    self.isEpg = NO;
    self.isDateChanged = NO;
    
    self.navigationController.navigationBarHidden = YES;
    
    [self setDateLabelText];
   // [self setWeather];
    
   

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectDevice:) name:kDisconnectDeviceNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newDeviceConnected:) name:kDidConnectToNewDeviceNotification object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lostDevice:) name:kLostDeviceNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wiFiHasChanged:) name:kWiFiChangedNotification object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginPolling:) name:kBeginPollingNotification object:nil];
    
    [self addSubscriptions];
    
    [self setAllChannels];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.deviceTargetURL = [defaults valueForKey:kDeviceTargetURL];
    
    [NSTimer scheduledTimerWithTimeInterval: 1.0
                                     target: self
                                   selector: @selector(updateDateLabel:)
                                   userInfo: nil
                                    repeats: YES];
    
    [self setUpAdPopOver];
    [self startPollTimer];
    [self setupBuildLabel];
}

-(void)setupBuildLabel
{
    NSString *heading = [ServiceProviderManager sharedInstance].serviceProvider.heading;
    NSString *VersionNo = [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *BuildNo = [[[NSBundle mainBundle]infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
    
    NSString *str = [NSString stringWithFormat:@"%@ - %@",heading,VersionNo];
    _buildLabel.text = str;

}

-(void) setAllChannels
{
    self.channelsTableViewController = [[ChannelsTableViewController alloc] init];
    [self.channelsTableViewController.tableView setFrame:CGRectMake(704, 148, 300, 558)];
    [self.view addSubview:self.channelsTableViewController.tableView];
     self.channelsTableViewController.delegate = self;
    [self.channelsTableViewController.tableView setBackgroundColor:[UIColor clearColor]];
    [self.channelsTableViewController.tableView setBounces:false];
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       
                       [SVProgressHUD showWithStatus:kWaitMessage maskType:SVProgressHUDMaskTypeClear];
                       
                   });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        
        [[ChannelServiceManager sharedInstance] getAllChannels:^(void){
            [self.channelsTableViewController reloadChannels];
            [SVProgressHUD dismiss];
            
        } onFailure:^(NSError *error){
            
            [SVProgressHUD dismiss];
           // [SVProgressHUD showErrorWithStatus:kFailedChannelRequest];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                            message:@"Please try again later."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    });

}

-(void) updateConnectButtonLabel{
    
    Device *device = [self.svDeviceManager getCurrentSVDevice];
    NSLog(@"Connect Button Title : %@",device.deviceLocation);
    if (device.deviceLocation != nil) {
        [self.connectButton setTitle:device.deviceLocation forState:UIControlStateNormal];
    }else{
        [self.connectButton setTitle:@"Connected Roku device Identifier" forState:UIControlStateNormal];
    }
    
}

-(void) startPollTimer{
    
    
        self.pollTimerCount = 0;
        self.pollTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                               target: self
                                                             selector: @selector(isPollingTimedOut:)
                                                             userInfo: nil
                                                              repeats: YES];
}

-(void)beginPolling:(NSNotification *) note{
    
    [self startPollTimer];
}

-(void)isPollingTimedOut:(NSTimer *)timer {
   // self.pollTimerCount++;
   // NSLog(@"Polling counting : %d",self.pollTimerCount);
    //if (self.pollTimerCount > POLLTIMEOUT) {
     //   self.pollTimerCount = 0;
        //NSLog(@"Poll Time Out");
        //self.pollDeviceManager = [StreamVisionDeviceManager sharedInstance];
        //[self.pollDeviceManager pollForDevice];
        
        bool comnnectivity = [SVNetworkCheck hasConnectivity];
        if (comnnectivity == false)
        {
            [self.pollTimer invalidate];
            [SVProgressHUD dismiss];

           //  [SVProgressHUD showErrorWithStatus:kNoNetworkMessage maskType:SVProgressHUDMaskTypeGradient];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kConnectionUnAvailable message:nil delegate:self cancelButtonTitle:@"try again" otherButtonTitles: @"quit",nil];
            [alert show];
        }//else
         //   [SVProgressHUD dismiss];

    
       // [self startPollTimer];
  //  }
    
    
    NSDate* date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];

    if (self.isEpg == YES && ![[dateFormat stringFromDate:self.currentDate] isEqualToString:[dateFormat stringFromDate:date]])
    {
       self.currentDate = date;
       self.isDateChanged = YES;
      // [self.navigationController popToRootViewControllerAnimated:YES];
        //[self performSegueWithIdentifier:kEPGSegue sender:self];
    }
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"try again"])
    {
        [self startPollTimer];
        [SVProgressHUD showWithStatus:@"Connecting to network . . ."];
    }
    else if([title isEqualToString:@"quit"])
    {
        exit(0);
    }
}

- (void)updateDateLabel:(NSTimer *)timer {
    [self setDateLabelText];
}

-(void) viewWillAppear:(BOOL)animated{
    
     self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setDateLabelText{
    
    NSDateFormatter* theDateFormatter = [[NSDateFormatter alloc] init];
    
    [theDateFormatter setDateFormat:@"EEEE"];
    
    NSString *weekDay =  [theDateFormatter stringFromDate:[NSDate date]];
    
    [theDateFormatter setDateFormat:@"hh:mm a"];
    
    [theDateFormatter setAMSymbol:@"AM"];
    [theDateFormatter setPMSymbol:@"PM"];
    
    NSString *weekDay_Time =  [theDateFormatter stringFromDate:[NSDate date]];
    
    self.todayString =[NSString stringWithFormat:@"%@ , %@",weekDay,weekDay_Time];
    
    self.dateLabel.text=[NSString stringWithFormat:@"%@ , %@",weekDay,weekDay_Time];

}

-(void) setWeather{
    
    
    [self LocationStart];
    
}

- (void) addSubscriptions
{
    if (self.device)
    {
        _appList = [[NSMutableArray alloc] init];
        
        if ([self.device hasCapability:kLauncherAppList])
        {
            [self.device.launcher getAppListWithSuccess:^(NSArray *appList)
             {
                 NSLog(@"Get app list success");
                 
                 
                 _appList = [self filterSVAppfromAppList:appList];
                 [self reloadAppData];
             } failure:^(NSError *err)
             {
                 [SVProgressHUD showWithStatus:@"Getting application list, please wati ..."];
                 [self.device.launcher getAppListWithSuccess:^(NSArray *appList)
                  {
                      NSLog(@"Get app list success");
                      
                      
                      _appList = [self filterSVAppfromAppList:appList];
                      [self reloadAppData];
                  } failure:^(NSError *err)
                  {
                      
                      
                      NSLog(@"Get app list Error %@", err.description);
                      [SVProgressHUD dismiss];
                      
                      UIAlertView *noAppAlert = [[UIAlertView alloc] initWithTitle:@"Could not get App List" message:@"Unable to get app list, please check your network connectivity" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                      [noAppAlert show];
                  }];
                 
                 NSLog(@"Get app list Error %@", err.description);
             }];
        }
        
        _currentApp = -1;
        
        if ([self.device hasCapability:kLauncherRunningAppSubscribe])
        {
            _runningAppSubscription = [self.device.launcher subscribeRunningAppWithSuccess:^(AppInfo *appInfo)
                                       {
                                           NSLog(@"Running app changed %@", appInfo);
                                           
                                           [_appList enumerateObjectsUsingBlock:^(AppInfo *app, NSUInteger idx, BOOL *stop)
                                            {
                                                if ([app isEqual:appInfo])
                                                {
                                                    _currentApp = idx;
                                                    
                                                    [self reloadAppData];
                                                    *stop = YES;
                                                }
                                            }];
                                       } failure:^(NSError *err)
                                       {
                                           NSLog(@"Running app subscription err %@", err);
                                       }];
        }
        
       /* NSString *dialAppName = [[NSUserDefaults standardUserDefaults] stringForKey:@"dialAppName"];
        NSString *dialAppCapability = [NSString stringWithFormat:@"Launcher.%@", dialAppName];*/
        
        
        if ([self.device hasCapability:kMouseControlConnect])
        {
            [self.device.mouseControl connectMouseWithSuccess:^(id responseObject)
             {
                 NSLog(@"mouse connection success");
                 
                 _touchpad.mouseControl = self.device.mouseControl;
                 _touchpad.userInteractionEnabled = YES;
             } failure:^(NSError *error)
             {
                 NSLog(@"mouse connection error %@", error.localizedDescription);
             }];
        }
        
        
        if ([self.device hasCapability:kVolumeControlMuteSet]) {
            
            [self.volumeSlider setEnabled:YES];
            [self.muteButton setEnabled:YES];
            
        }
        
        if ([self.device hasCapability:kVolumeControlVolumeSubscribe])
        {
            _volumeSubscription = [self.device.volumeControl subscribeVolumeWithSuccess:^(float volume)
                                   {
                                       [_volumeSlider setValue:volume];
                                       [_volumeSlider setEnabled:YES];
                                       NSLog(@"volume changed to %f", volume);
                                   } failure:^(NSError *error)
                                   {
                                       NSLog(@"Subscribe Vol Error %@", error.localizedDescription);
                                   }];
        } else if ([self.device hasCapability:kVolumeControlVolumeGet])
        {
            [self.device.volumeControl getVolumeWithSuccess:^(float volume)
             {
                 [_volumeSlider setValue:volume];
                 NSLog(@"Get vol %f", volume);
             } failure:^(NSError *error)
             {
                 NSLog(@"Get Vol Error %@", error.localizedDescription);
             }];
        }


        if ([self.device hasCapability:kKeyControlUp]) [_upButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlDown]) [_downButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlLeft]) [_leftButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlRight]) [_rightButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlOK]) [_clickButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlHome]) [_homeButton setEnabled:YES];
        if ([self.device hasCapability:kKeyControlBack]) [_backButton setEnabled:YES];
        if ([self.device hasCapability:kMediaControlPlay]) [self.playButton setEnabled:YES];
        if ([self.device hasCapability:kMediaControlPause]) [self.pauseButton setEnabled:YES];
        if ([self.device hasCapability:kMediaControlStop]) [self.stopButton setEnabled:YES];
        if ([self.device hasCapability:kMediaControlRewind]) [self.rewindButton setEnabled:YES];
        if ([self.device hasCapability:kMediaControlFastForward]) [self.forwardButton setEnabled:YES];
             
    }
}

-(NSMutableArray *)filterSVAppfromAppList:(NSArray *) allAppsList{
    
    NSMutableArray *returnValue = [[NSMutableArray alloc] init];
    
    for(AppInfo *app in allAppsList){
    if([app.name isEqualToString:kStreamVisionAppName]){
          self.svApp = app;
        }else{
            [returnValue addObject:app];
        }
    }
    
    return returnValue;
}

- (void) reloadAppData
{
    NSSortDescriptor *titleSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:titleSortDescriptor];
    _appList = [[NSMutableArray alloc] initWithArray:[_appList sortedArrayUsingDescriptors:sortDescriptors]];
    
    [self.appsCollectionView reloadData];
    
}

#pragma mark Button Actions


- (IBAction)clickClicked:(id)sender
{
    [self.device.keyControl okWithSuccess:nil failure:nil];
}

- (IBAction)homeClicked:(id)sender
{
    [self.streamingNowImageView setImage:nil];
    [self.device.keyControl homeWithSuccess:nil failure:nil];
}

- (IBAction)backClicked:(id)sender
{
    [self.device.keyControl backWithSuccess:nil failure:nil];
}

- (IBAction)upDown:(id)sender
{
    [self.device.keyControl upWithSuccess:nil failure:nil];
    
    if(_timer != nil)
        [_timer invalidate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:FIVEWAY_DELAY target:self selector:@selector(hButtonHold) userInfo:@{@"keyCode":@(FivewayKeyUp)} repeats:YES];
}

- (IBAction)downDown:(id)sender
{
    [self.device.keyControl downWithSuccess:nil failure:nil];
    
    if(_timer != nil)
        [_timer invalidate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:FIVEWAY_DELAY target:self selector:@selector(hButtonHold) userInfo:@{@"keyCode":@(FivewayKeyDown)} repeats:YES];
}

- (IBAction)leftDown:(id)sender
{
    [self.device.keyControl leftWithSuccess:nil failure:nil];
    
    if(_timer != nil)
        [_timer invalidate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:FIVEWAY_DELAY target:self selector:@selector(hButtonHold) userInfo:@{@"keyCode":@(FivewayKeyLeft)} repeats:YES];
}

- (IBAction)rightDown:(id)sender
{
    [self.device.keyControl rightWithSuccess:nil failure:nil];
    
    if(_timer != nil)
        [_timer invalidate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:FIVEWAY_DELAY target:self selector:@selector(hButtonHold) userInfo:@{@"keyCode":@(FivewayKeyRight)} repeats:YES];
}

- (IBAction)buttonUp:(id)sender
{
    [_timer invalidate];
    _timer = nil;
}


#pragma mark - Mouse methods
- (void) hButtonHold
{
    NSNumber *buttonKeyCode = [[_timer userInfo] objectForKey:@"keyCode"];
    int buttonKey = [buttonKeyCode intValue];
    
    switch (buttonKey)
    {
        case FivewayKeyUp: [self.device.keyControl upWithSuccess:nil failure:nil]; break;
        case FivewayKeyDown: [self.device.keyControl downWithSuccess:nil failure:nil]; break;
        case FivewayKeyLeft: [self.device.keyControl leftWithSuccess:nil failure:nil]; break;
        case FivewayKeyRight: [self.device.keyControl rightWithSuccess:nil failure:nil]; break;
        default:break;
    }
}


- (IBAction)showEPG:(id)sender {
    [self performSegueWithIdentifier:kEPGSegue sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    self.isEpg = NO;
    if ([segue.identifier isEqualToString:kEPGSegue]) {
        self.isEpg = YES;
        bool exist = false;
        NSArray* arr ;
        if(self.epgViewController != NULL && self.isDateChanged == NO)
        {
            if(self.epgViewController.isServerError == NO){
                exist = true;
                arr = [self.epgViewController oldChannels];
            }
            
        }
       self.epgViewController = (EPGViewController *)segue.destinationViewController;
       self.epgViewController.todayString = self.todayString;
       self.epgViewController.temperatureString = self.temperatureStrng;
       self.epgViewController.device = self.device;
       self.epgViewController.svApp = self.svApp;
       self.epgViewController.svAppRunning = self.svAppIsStreaming;
       self.epgViewController.mainviewController = self;
       if(exist) [self.epgViewController setChannelArray:arr];
       self.isDateChanged  = NO;
    }
    if ([segue.identifier isEqualToString:kSettingsSegue]) {
        
        UserSettingViewController *destVC = (UserSettingViewController *)segue.destinationViewController;
        destVC.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:kRokuListingSegue]) {
        RokuListingViewController *destVC = (RokuListingViewController *)segue.destinationViewController;
        destVC.newUser = self.newUser;
    }

}


#pragma mark Video Controls

- (IBAction)prevButtonTapped:(id)sender {

}




- (IBAction)playButtonTapped:(id)sender {
    
    
}
- (IBAction)recordButtonTapped:(id)sender {
    
    
}
- (IBAction)stopButtonTapped:(id)sender {
    
    
}


- (IBAction)rewindButtonTapped:(id)sender {
    
    
    [self.device.mediaControl rewindWithSuccess:^(id responseObject)
     {
         NSLog(@"rewind success");
     } failure:^(NSError *error)
     {
         NSLog(@"rewind failure: %@", error.localizedDescription);
     }];
    
}

- (IBAction)pauseButtonTapped:(id)sender {
    
    
    
    if (self.isPaused==YES)
    {
        self.isPaused=NO;
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        
        
        [self.device.mediaControl playWithSuccess:^(id responseObject)
         {
             
             NSLog(@"play success");
         } failure:^(NSError *error)
         {
             NSLog(@"play failure: %@", error.localizedDescription);
         }];
        
    }
    else
    {
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        self.isPaused=YES;
        [self.device.mediaControl pauseWithSuccess:^(id responseObject)
         {
             NSLog(@"pause success");
         } failure:^(NSError *error)
         {
             NSLog(@"pause failure: %@", error.localizedDescription);
         }];
        
        
    }
    
    
    
}

- (IBAction)forwardButtonTapped:(id)sender {
    
    
    [self.device.mediaControl fastForwardWithSuccess:^(id responseObject)
     {
         NSLog(@"fast forward success");
     } failure:^(NSError *error)
     {
         NSLog(@"fast forward failure: %@", error.localizedDescription);
     }];
    
    
}

- (IBAction)nextButtonTapped:(id)sender {
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (_appList)
        return _appList.count;
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AppCollectionViewCell *cell = (AppCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifierCell forIndexPath:indexPath];
    
    // Configure the cell
    
    AppInfo *appInfo = (AppInfo *) [_appList objectAtIndex:(NSUInteger) indexPath.row];
    
    NSString *imageURLStr = [NSString stringWithFormat:@"%@/query/icon/%@",self.deviceTargetURL,appInfo.id];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:imageURLStr] placeholderImage:[UIImage imageNamed:@"sample_app_icon"] options:SDWebImageProgressiveDownload];
    
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (kind == UICollectionElementKindSectionHeader) {
       
            AppCollectionHeader *header =[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifierHeader forIndexPath:indexPath];
            header.delegate = self;
            header.deviceTargetURL = self.deviceTargetURL;
            [header setUpwithApp:self.svApp];
            
            return header;
       
       
    }
    
    
    
    return nil;
}



#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Collection view Tapped at Index");
    
    
    AppInfo *app = (AppInfo *) [_appList objectAtIndex:(NSUInteger) indexPath.row];
    
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Launching App %@",app.name]];

    
    [self.device.launcher launchAppWithInfo:app success:^(LaunchSession *launchSession)
     {
         [self setCurrentStreamingNow:app];

         [SVProgressHUD dismiss];
         self.svAppIsStreaming = NO;
         NSLog(@"Launched application %@", launchSession);
     }                               failure:^(NSError *error)
     {
         
         NSLog(@"no launchApp %@", error);
         dispatch_async(dispatch_get_main_queue(), ^{
             //Your main thread code goes in here
            
             [SVProgressHUD dismiss];
             UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Unable to Launch App" message:@"Please check your network or if the device is on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [lost show];
         });
         
     }];

    
}

-(void) setCurrentStreamingNow:(AppInfo *) appInfo{
    
    NSString *imageURLStr = [NSString stringWithFormat:@"%@/query/icon/%@",self.deviceTargetURL,appInfo.id];
    [self.streamingNowImageView sd_setImageWithURL:[NSURL URLWithString:imageURLStr] placeholderImage:[UIImage imageNamed:@"sample_app_icon"] options:SDWebImageProgressiveDownload];
    
}


#pragma mark AppCollectionHeaderDelegate

-(void) headerSelected:(AppInfo *)app{
    
    
    if(!self.isDisconnected){
    
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Launching App %@",app.name]];
    
    
    
    [self.device.launcher launchAppWithInfo:app success:^(LaunchSession *launchSession)
     {
         [self setCurrentStreamingNow:app];
         [SVProgressHUD dismiss];
         self.svAppIsStreaming = YES;
         NSLog(@"Launched application %@", launchSession);
     }                               failure:^(NSError *error)
     {
         [SVProgressHUD dismiss];
         self.svAppIsStreaming = NO;
         NSLog(@"no launchApp %@", error);
         
         dispatch_async(dispatch_get_main_queue(), ^{
             //Your main thread code goes in here
             
             [SVProgressHUD dismiss];
             UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Unable to Launch App" message:@"Please check your network or if the device is on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [lost show];
         });

     }];
        
    }else{
        
        
        UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Unable to Launch App" message:@"You are not connected to any Roku device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [lost show];
        
    }
    
}


#pragma mark ChannelsTableVCDelegate


-(void) setStreaminNowLogo:(Channel *) selectedChannel{
    
    if(!self.isDisconnected)
    [self.streamingNowImageView sd_setImageWithURL:[NSURL URLWithString:selectedChannel.channelLogo]];
}

-(void) launchChannelWithId:(NSString *)channelId{
    
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAdSpace:) name:kAdURLReceivedNotification object:nil];
   // [self listen];
    
    
    if(!self.isDisconnected){
    
   if (self.svAppIsStreaming) {
       
     [self launchChannel:channelId];
    
  }
  else
  {
        
        
        [self setCurrentStreamingNow:self.svApp];
        
        [self.device.launcher launchAppWithInfo:self.svApp success:^(LaunchSession *launchSession)
         {
             [SVProgressHUD dismiss];
             self.svAppIsStreaming = YES;
             [self launchChannel:channelId];
             
             NSLog(@"Launched application %@", launchSession);
         }                               failure:^(NSError *error)
         {
             [SVProgressHUD dismiss];
             self.svAppIsStreaming = NO;
             
             NSLog(@"no launchApp %@", error);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Your main thread code goes in here
                 
                 [SVProgressHUD dismiss];
                 UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Unable to Launch Channel" message:@"Please check your network or if the device is on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                 [lost show];
             });
             
             
         }];
        

    }
    }else{
        
         [SVProgressHUD dismiss];
        
        UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Unable to Launch Channel" message:@"You are not connected to any Roku device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [lost show];

    }
    
}

-(void) launchChannel:(NSString *) channelId{
    
    //below for Ad support
   // NSString *post =[NSString stringWithFormat:@"%@/input?op=%@_%@",self.deviceTargetURL,channelId,self.ipAddress];
    NSString *post =[NSString stringWithFormat:@"%@/input?op=%@",self.deviceTargetURL,channelId];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:post]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:6];
    [request addValue:@"text/plain;charset=\"utf-8\"" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if (connectionError)
         {
             [SVProgressHUD dismiss];
             //Handle Error
             
              UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Lost Connection To Device" message:@"Please check your network or if the device is on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [lost show];
             
         } else
         {
             [self updateConnectButtonLabel];
             if ([httpResponse statusCode] < 200 || [httpResponse statusCode] >= 300)
             {
                 [SVProgressHUD dismiss];
                 //Handle Error
                 return;
             }
             
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             
             NSLog(@"Return Data : %@",dataString);
             
             [SVProgressHUD dismiss];
             
             
         }
         
     }];
}

- (IBAction)showSettings:(id)sender {
    
    [self.pollTimer invalidate];
    
    [self performSegueWithIdentifier:kSettingsSegue sender:self];
}

- (void)showSettingsFromEpg
{
    [self.pollTimer invalidate];
    [self performSegueWithIdentifier:kSettingsSegue sender:self];
}


#pragma mark VOLUME CONTROL

- (IBAction)volumeChanged:(UISlider *)sender
{
    float vol = [self.volumeSlider value];
    
    [self.device.volumeControl setVolume:vol success:^(id responseObject)
     {
         NSLog(@"Vol Change Success %f", vol);
     } failure:^(NSError *setVolumeError)
     {
         // For devices which don't support setVolume, we'll disable
         // slider and should encourage volume up/down instead
         
         NSLog(@"Vol Change Error %@", setVolumeError.description);
         
         sender.enabled = NO;
         sender.userInteractionEnabled = NO;
         
         [self.device.volumeControl getVolumeWithSuccess:^(float volume)
          {
              NSLog(@"Vol rolled back to actual %f", volume);
              
              sender.value = volume;
          } failure:^(NSError *getVolumeError)
          {
              NSLog(@"Vol serious error: %@", getVolumeError.localizedDescription);
          }];
     }];
}


- (IBAction)muteButtonTapped:(id)sender {
}

- (IBAction)instantplay:(id)sender {
    
    [self.device.keyControl instatplayWithSuccess:nil failure:nil];
}
- (IBAction)info:(id)sender {
    
    [self.device.keyControl infoWithSuccess:nil failure:nil];
    
    
}

#pragma mark Location and Temperature methods



-(void)GetCurrentTemp
{
    
    NSString *GetStr =[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/find?q=%@&units=metric",self.locationNameStr];
    
    NSURL *url = [NSURL URLWithString:GetStr];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0f];
    [request setHTTPMethod:@"GET"];
    [request setHTTPShouldHandleCookies:NO];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *jsonDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    NSMutableArray *DataArrayTemp =  [jsonDict objectForKey:@"list"];
    
    NSMutableArray *MainDataArrayTemp=[DataArrayTemp valueForKey:@"main"];
    
    NSString *str=[NSString stringWithFormat:@"%@",[MainDataArrayTemp valueForKey:@"temp"]];
    
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"/:;$&@\",?!\'[]{}#%^*=_|~<>€£¥•\n() "];
    str = [[str componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    
    self.temperatureStrng =[NSString stringWithFormat:@"%@°C",str];
    
    self.timeLabel.text=[NSString stringWithFormat:@"%@°C",str];
    
}

- (void) checkLocationServicesTurnedOn {
    if (![CLLocationManager locationServicesEnabled]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"== Opps! =="
                                                        message:@"'Location Services' need to be on."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void) checkApplicationHasLocationServicesPermission {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"== Opps! =="
                                                        message:@"This application needs 'Location Services' to be turned on."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)LocationStart
{
    [self checkLocationServicesTurnedOn];
    [self checkApplicationHasLocationServicesPermission];
    locationManager = [[CLLocationManager alloc] init];
    if ([self->locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self->locationManager requestWhenInUseAuthorization];
    }
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}


// this delegate is called when the app successfully finds your current location
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
  /*  UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];*/
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                       }
                       
                       
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       [locationManager stopUpdatingLocation];
                       
                       
                       self.locationNameStr=[NSString stringWithFormat:@"%@,%@",placemark.locality,placemark.ISOcountryCode];
                       [self GetCurrentTemp];
                       
                   }];
    
    
    
}



- (IBAction)showRokuDeviceList:(id)sender {
    
    [self.pollTimer invalidate];
    [self performSegueWithIdentifier:kRokuListingSegue sender:nil];
}

-(void) disconnectDevice:(NSNotification *) note{
    
    [self.device disconnect];
    [self.streamingNowImageView setImage:nil];
    [_appList removeAllObjects];
    [self.appsCollectionView reloadData];
    
}

-(void) lostDevice:(NSNotification *) note{
 
    NSLog(@"Lost Device Main VIew COntroller");
    
    ConnectableDevice *lostDevice = (ConnectableDevice *)note.object;
   
    
    if ([self.device.address isEqualToString:lostDevice.address]) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:nil forKey:kCurrentSelectedDeviceIdKey];
        [defaults synchronize];
        
        [self.connectButton setTitle:@"" forState:UIControlStateNormal];

        
        [[CoreDataManager sharedInstance] updateOfflineStatusForDevice:self.device];
        [self.device disconnect];
       
        [self.streamingNowImageView setImage:nil];
        //[_appList removeAllObjects];
        [self.appsCollectionView reloadData];
        
        
        [SVProgressHUD dismiss];
        if (self.userChangedId) {
            self.isDisconnected = YES;
            UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Device has been disconnected" message:@"User Id has been changed, please select a new device to connect to." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [lost show];
            
            
            self.userChangedId = NO;
        }else{
            
            [self.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
            //bool comnnectivity = [SVNetworkCheck hasConnectivity];
           // if (comnnectivity == false)
            {
                UIAlertView *lost = [[UIAlertView alloc] initWithTitle:@"Lost Connection To Device" message:@"Please check your network or if the device is on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [lost show];
            }
            
        }
        
       // [SVProgressHUD dismiss];
    
    }
    
    
}

-(void) wiFiHasChanged:(NSNotification *) note{
    
    
    
}

-(void) newDeviceConnected:(NSNotification *) note{
    self.svAppIsStreaming = NO;
    [SVProgressHUD showWithStatus:kWaitMessage];
    self.device = (ConnectableDevice *)note.object;
    [self.streamingNowImageView setImage:nil];

    NSString *targetPath = [NSString stringWithFormat:@"http://%@:%@/", self.device.serviceDescription.address, @(self.device.serviceDescription.port)];
    self.deviceTargetURL = targetPath;
    
    [self updateConnectButtonLabel];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:targetPath forKey:kDeviceTargetURL];
    [defaults synchronize];
    
    if(self.epgViewController != NULL)
    {
        self.epgViewController.device = self.device;
        self.epgViewController.svApp = self.svApp;
        self.epgViewController.svAppRunning = self.svAppIsStreaming;
        self.epgViewController.deviceTargetURL = targetPath;
    }
    
    [_appList removeAllObjects];
    [self addSubscriptions];
    [self.appsCollectionView reloadData];
    [self.channelsTableViewController.tableView reloadData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[ChannelServiceManager sharedInstance] getAllChannels:^(void){
            [self.channelsTableViewController reloadChannels];
            [SVProgressHUD dismiss];
            
        } onFailure:^(NSError *error){
            
            [SVProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                            message:@"Please try again later."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    });
    self.isDisconnected = NO;
}


- (IBAction)shareContent:(id)sender {
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    
   
        [sharingItems addObject:@"Sharing from SV App"];
//    }
//    if (image) {
//        [sharingItems addObject:image];
//    }
//    if (url) {
//        [sharingItems addObject:url];
//    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    activityController.popoverPresentationController.sourceView = self.shareButton;
    [self presentViewController:activityController animated:YES completion:nil];
    
}


- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en1"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}



- (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_VPN @"/" IP_ADDR_IPv4, IOS_VPN @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6 ] :
    @[ IOS_VPN @"/" IP_ADDR_IPv6, IOS_VPN @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4 ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         if(address) *stop = YES;
     } ];
    return address ? address : @"0.0.0.0";
}

- (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}


-(void) listen{
    
    int listenfd = 0;
    struct sockaddr_in serv_addr;
    
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(10000);
    
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    
    _listenForConnections = true;
    listen(listenfd, 10);
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSLog(@"Waiting for connections...");
        while (_listenForConnections)
        {
            __block int connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
            NSLog(@"Connection accepted");
            
            char buffer[1024];
            bzero(buffer, 1024);
            NSString *message = @"";
            bool continueReading = true;
            
            do
            {
                recv(connfd , buffer , 1024 , 0);
                int size = strlen(buffer);
                if ((buffer[size-1] == '}' && buffer[size-2] == '{'))
                {
                    continueReading = false;
                    buffer[size-2] = '\0';
                }
                message = [NSString stringWithFormat: @"%@%s", message, buffer];
                NSLog(@"Message is : %@",message);
                if (message) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kAdURLReceivedNotification object:message];
                    //continueReading = false;
                }
                            
            }while (continueReading);
           
            NSLog(@"Got message from client");
            
            char* answer = "Hello World";
            write(connfd, answer, strlen(answer));
        }
        
        NSLog(@"Stop listening.");
        close(listenfd);
    });
    
}

-(NSString *) getAdURLFromMessage:(NSString *) message{
    
    NSRange r1 = [message rangeOfString:@"url="];
    NSRange r2 = [message rangeOfString:@" HTTP"];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *sub = [message substringWithRange:rSub];
    
    return sub;
}

-(void) showAdSpace:(NSNotification *) note{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAdURLReceivedNotification object:nil];
    
    NSString *decoded = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)[self getAdURLFromMessage:note.object], CFSTR(""), kCFStringEncodingUTF8);
    NSLog(@"decodedString %@", decoded);
    
    self.adRedirectURL = decoded;
    NSLog(@"Ad URL : %@",self.adRedirectURL);
    
   // [self showAddPopOver];
    //[self startAdTimer];
    
}

-(void) startAdTimer{
    
    self.timeoutCounter = 0;
    self.adTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                           target: self
                                                         selector: @selector(isAdTimedOut:)
                                                         userInfo: nil
                                                          repeats: YES];
    
}


-(void)isAdTimedOut:(NSTimer *)timer {
    
    NSLog(@"Ad Timer Counting");
    self.timeoutCounter++;
    if (self.timeoutCounter > ADTIMERCOUNT) {

        [self.adTimer invalidate];
        [self.adPopOverController dismissPopoverAnimated:YES];
    }
    
}

-(void) setUpAdPopOver{
    
     self.adVC = [[AdPopOverViewController alloc] initWithNibName:@"AdPopOverViewController" bundle:nil];
    self.adPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.adVC];
    self.adPopOverController.delegate = self;
    self.adPopOverController.popoverContentSize = CGSizeMake(300, 300); //your custom size.
    
}

-(void) showAddPopOver{
    
    self.adVC.adURL = self.adRedirectURL;
    [self.adPopOverController presentPopoverFromRect:self.overAirMenuButton.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
}

#pragma mark UIPopOverController Delegate

/* Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    
     [self.adTimer invalidate];
    return YES;
    
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    [self.adTimer invalidate];
    
}


#pragma mark UserSettingsDelegate

-(void) launchDeviceSelection{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:kCurrentSelectedDeviceIdKey];
    [defaults synchronize];
    
    self.userChangedId = YES;
    [self.device disconnect];
    //[_appList removeAllObjects];
    [self.streamingNowImageView setImage:nil];
    [self.appsCollectionView reloadData];
    
    [self.channelsTableViewController.tableView reloadData];
    
    self.newUser = YES;
    [self showRokuDeviceList:self];

}

#pragma mark EPGViewControllerDelegate

-(void) launchedSVChannel{
    
    self.svAppIsStreaming = YES;
}

@end
