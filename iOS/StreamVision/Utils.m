//
//  Utils.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Utils.h"


static NSDateFormatter * FORMATTER = nil;

static NSDateFormatter * EPGFORMATTER = nil;
static NSDateFormatter * WEEKDAYFORMATTER = nil;
static NSDateFormatter * CALANDERDAYFORMATTER = nil;
static NSDateFormatter * EXPIRATIONFORMATTER = nil;
static NSNumberFormatter * EPISODENUMBERFORMATTER = nil;

static NSCalendar * CALENDER = nil;
static NSDateComponents * TODAYCOMPONENTS = nil;

static NSString *const kVersionStringSeparator = @".";
static NSUInteger const kIOS8VersionNumber      = 8;
static NSUInteger const kIOSDefaultVersionNumber = 7;

@interface Utils( )
+ (BOOL) interfaceShouldBeConsideredAsRotated;
@end


@implementation Utils

+ (BOOL) interfaceShouldBeConsideredAsRotated
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    BOOL isIOS8 = [ [ self class ] systemIsIOS8 ];
    
    return ( UIInterfaceOrientationIsLandscape( orientation ) && !isIOS8 );
}

+ (CGRect) screenFrame
{
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGSize size = screenRect.size;
    
    if ( [ [ self class ] interfaceShouldBeConsideredAsRotated ] )
    {
        screenRect.size.width = size.height;
        screenRect.size.height = size.width;
    }
    
    return screenRect;
}

+ (BOOL) systemIsIOS8
{
    NSString *systemVersion = [ [ UIDevice currentDevice ] systemVersion ];
    NSString *versionFirstDigit = [ [ systemVersion componentsSeparatedByString: kVersionStringSeparator ] firstObject ];
    
    NSUInteger versionFirstNumber = ( versionFirstDigit ) ? [ versionFirstDigit integerValue ]: kIOSDefaultVersionNumber;
    
    return ( versionFirstNumber >= kIOS8VersionNumber );
}

+ (CGPoint) screenCenter
{
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGPoint center = CGPointMake((screenRect.size.width)/2 , (screenRect.size.height-64)/2);
    
    if ( [ [ self class ] interfaceShouldBeConsideredAsRotated ] )
    {
        center = CGPointMake((screenRect.size.height)/2 , (screenRect.size.width-64)/2);
    }
    
    return center;
}

+ (CGRect) mainPageFrame
{
    CGRect rect = [Utils screenFrame];
    rect.origin.y = STATUS_BAR_HEIGHT + NAVIGATION_BAR_HEIGHT_DEFAULT;
    rect.size.height -= (STATUS_BAR_HEIGHT + NAVIGATION_BAR_HEIGHT_DEFAULT);
    return rect;
}

+ (NSDateFormatter *) formatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        FORMATTER = [[NSDateFormatter alloc] init];
        [FORMATTER setDateFormat:@"yyyy"];
    });
    return FORMATTER;
}

+ (NSString *) formatForHeader:(NSDate *) date
{
    return [[Utils formatter] stringFromDate:date];
}

+ (CGSize) headerHeight:(NSString *) text
{
    return CGSizeZero;
}

+(NSDateFormatter *) epgFormatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        EPGFORMATTER = [[NSDateFormatter alloc] init];
        EPGFORMATTER.dateFormat = @"hh:mm a";
    });
    return EPGFORMATTER;
}

+ (NSDateFormatter *) weekdayFormatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        WEEKDAYFORMATTER = [[NSDateFormatter alloc] init];
        WEEKDAYFORMATTER.dateFormat = @"EEEE";  // show week days
    });
    return WEEKDAYFORMATTER;
}

+ (NSDateFormatter *) calanderFormatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CALANDERDAYFORMATTER = [[NSDateFormatter alloc] init];
        CALANDERDAYFORMATTER.dateFormat = @"MMM dd";
    });
    return CALANDERDAYFORMATTER;
}

+ (NSDateFormatter *)expirationFormatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        EXPIRATIONFORMATTER = [[NSDateFormatter alloc] init];
        EXPIRATIONFORMATTER.dateFormat = @"dd MMM, hh:mm aaa";
    });
    return EXPIRATIONFORMATTER;
}

+ (NSNumberFormatter *) episodeNumberFormatter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        EPISODENUMBERFORMATTER = [ [ NSNumberFormatter alloc ] init ];
        EPISODENUMBERFORMATTER.paddingPosition = NSNumberFormatterPadBeforePrefix;
        EPISODENUMBERFORMATTER.paddingCharacter = @"0";
        EPISODENUMBERFORMATTER.minimumIntegerDigits = 2;
    });
    return EPISODENUMBERFORMATTER;
}

+ (NSDate *) startOfTheDay
{
    static dispatch_once_t onceToken;
    //dispatch_once(&onceToken, ^{
        CALENDER = [NSCalendar currentCalendar];
        NSDate *nowDate = [NSDate date];
        TODAYCOMPONENTS = [CALENDER components:(  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:nowDate ];

   // });
    return [CALENDER dateFromComponents:TODAYCOMPONENTS];
}

+ (NSTimeInterval) sinceDayStart
{
    return [[NSDate date] timeIntervalSinceDate:[Utils startOfTheDay]];
}

+(NSDateFormatter *) hoursMinutesAmPmFormatter;
{
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [ [ NSDateFormatter alloc ] init ];
        formatter.dateFormat = @"hh:mm a";
    });
    return formatter;
}


+ (NSDateFormatter *) streamVsisionDateFormatter{

    //2015-05-29T07:00:00+0000
    
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [ [ NSDateFormatter alloc ] init ];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'+'0000";
    });
    return formatter;

    
    return nil;
}

+(NSString *) getEncodedString:(NSString *) rawString{
    
    
    CFStringRef safeString = CFURLCreateStringByAddingPercentEscapes (
                                                                      NULL,
                                                                      (CFStringRef)rawString,
                                                                      NULL,
                                                                      CFSTR("/%&=?$#+-~@<>|\\*,.()[]{}^!"),
                                                                      kCFStringEncodingUTF8
                                                                      );
    
    NSString *returnVal = (NSString *)CFBridgingRelease(safeString);
    
    return returnVal;
}


@end