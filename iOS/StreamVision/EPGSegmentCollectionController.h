//
//  EPGSegmentCollectionController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
 EPGSegmentCollectionController is used for displaying dates in top bar of EPG view.
                        ------
 */


#import <UIKit/UIKit.h>
#import "ProgramGuideDateSelectionDelegate.h"

@interface EPGSegmentCollectionController : UICollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) NSObject<ProgramGuideDateSelectionDelegate>* delegate;
@property (nonatomic, strong) NSIndexPath * currentSelected;

@property (nonatomic, assign) BOOL loadingMode;

- (void) setSelectedIndex: (NSUInteger) index;

@end


@interface EPGSegmentCollectionCell : UICollectionViewCell

@property (nonatomic, strong) UILabel * titleLabel;

@end
