//
//  Show.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 31/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Audio, Channel, ClosedCaption, Episode, Rating, ShowCategory;

@interface Show : NSManagedObject

@property (nonatomic, retain) NSString * adjustedStartTime;
@property (nonatomic, retain) NSString * cast;
@property (nonatomic, retain) NSNumber * cc;
@property (nonatomic, retain) NSString * displayGenre;
@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * endTimeUTC;
@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSNumber * isHd;
@property (nonatomic, retain) NSString * originalAirDate;
@property (nonatomic, retain) NSString * parentProgramId;
@property (nonatomic, retain) NSString * programId;
@property (nonatomic, retain) NSString * showDescription;
@property (nonatomic, retain) NSString * showType;
@property (nonatomic, retain) NSString * startTimeUtc;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * year;
@property (nonatomic, retain) NSString * videoURL;
@property (nonatomic, retain) Audio *audio;
@property (nonatomic, retain) NSSet *categories;
@property (nonatomic, retain) Channel *channel;
@property (nonatomic, retain) ClosedCaption *closedCaption;
@property (nonatomic, retain) NSSet *episodes;
@property (nonatomic, retain) Rating *rating;
@end

@interface Show (CoreDataGeneratedAccessors)

- (void)addCategoriesObject:(ShowCategory *)value;
- (void)removeCategoriesObject:(ShowCategory *)value;
- (void)addCategories:(NSSet *)values;
- (void)removeCategories:(NSSet *)values;

- (void)addEpisodesObject:(Episode *)value;
- (void)removeEpisodesObject:(Episode *)value;
- (void)addEpisodes:(NSSet *)values;
- (void)removeEpisodes:(NSSet *)values;

@end
