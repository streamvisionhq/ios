//
//  Channel.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
// @Test

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EPG, Show;

@interface Channel : NSManagedObject

@property (nonatomic, retain) NSString * broadcastType;
@property (nonatomic, retain) NSString * callsign;
@property (nonatomic, retain) NSString * channelDescription;
@property (nonatomic, retain) NSString * channelId;
@property (nonatomic, retain) NSString * channelNumber;
@property (nonatomic, retain) NSString * majorChannelNumber;
@property (nonatomic, retain) NSString * minorChannelNumber;
@property (nonatomic, retain) NSString * network;
@property (nonatomic, retain) NSString * channelLogo;
@property (nonatomic, retain) NSString * channelName;
@property (nonatomic, retain) EPG *epg;
@property (nonatomic, retain) NSSet *shows;
@end

@interface Channel (CoreDataGeneratedAccessors)

- (void)addShowsObject:(Show *)value;
- (void)removeShowsObject:(Show *)value;
- (void)addShows:(NSSet *)values;
- (void)removeShows:(NSSet *)values;

@end
