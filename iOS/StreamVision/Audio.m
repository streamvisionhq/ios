//
//  Audio.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Audio.h"
#import "Show.h"


@implementation Audio

@dynamic language;
@dynamic audioServiceType;
@dynamic numberOfChannels;
@dynamic bitRateKbps;
@dynamic exactBitRate;
@dynamic surround;
@dynamic fullService;
@dynamic show;

@end
