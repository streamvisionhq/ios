//
//  EPGSegmentCollectionController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "EPGSegmentCollectionController.h"
#import "Utils.h"

#import "ProgramGuideDateSelectionUtils.h"

static NSString * const kToday    = @"Today";
static NSString * const kTomorrow = @"Tomorrow";
static NSString * const kCellIdentifier = @"EPGSegmentCell";

static NSString * const kDateFormat = @"%@\n%@";
static const CGFloat kWidthOfPreviewSection = 250;

@implementation EPGSegmentCollectionController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ self.collectionView registerClass: [ EPGSegmentCollectionCell class ] forCellWithReuseIdentifier: kCellIdentifier ];
    self.collectionView.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"tableBg"]];
    //[ UIColor clearColor ];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = NO;
    self.collectionView.allowsMultipleSelection = NO;
    self.loadingMode = NO;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:0];   // select today as default
    self.currentSelected = [NSIndexPath indexPathForRow: 0  inSection: 0 ];
}


- (void) setLoadingMode:(BOOL)loadingMode
{
    _loadingMode = loadingMode;
    [ self.collectionView reloadData ];
}

#pragma mark - UICollectionViewDelegate
- (BOOL) collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( _loadingMode )
    {
        return NO;
    }
    
    if( indexPath.row != self.currentSelected.row )
    {
        return !_loadingMode;
    }
    
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //EPGSegmentCollectionCell * cell = (EPGSegmentCollectionCell *) [collectionView cellForItemAtIndexPath:indexPath];
    //cell.titleLabel.highlighted = YES;
   [ self.delegate didSelectSegment: indexPath.row ];
    self.currentSelected = indexPath;
    
    [self setSelectedIndex:indexPath.row];
}

-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    EPGSegmentCollectionCell * cell = (EPGSegmentCollectionCell *) [collectionView cellForItemAtIndexPath:indexPath];
    cell.titleLabel.highlighted = NO;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(SCREEN_FRAME_DEFAULT)-kWidthOfPreviewSection)/ProgramGuideDateSelectionUtilsSegCapacity, kSegmentHeight);
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return ProgramGuideDateSelectionUtilsSegCapacity;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    EPGSegmentCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [ UIColor clearColor ];
    ProgramGuideDateSelectionDate *date = [ ProgramGuideDateSelectionUtils segmentData: indexPath.row ];
    cell.titleLabel.text = [ NSString stringWithFormat: kDateFormat, date.dayName, date.date ];
    
    if (indexPath.row == self.currentSelected.row)
    {
        cell.highlighted = YES;
        cell.alpha = 1.0f;
       
    }
    else
    {
        cell.alpha = (self.loadingMode) ? 0.5f: 1.0f;
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - Public Methods

- (void) setSelectedIndex:(NSUInteger)index
{
    NSIndexPath * nextIndex = [NSIndexPath indexPathForRow: index inSection: 0 ];
    [self.collectionView deselectItemAtIndexPath: self.currentSelected animated:YES];
    [self.collectionView selectItemAtIndexPath: nextIndex animated:YES scrollPosition: UICollectionViewScrollPositionNone];
    self.currentSelected = nextIndex;
    [self.collectionView reloadData];
}

@end

@implementation EPGSegmentCollectionCell

-(instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self.contentView addSubview: self.titleLabel];
        //self.backgroundColor = [ UIColor redColor ];
        NSDictionary * views = @{@"label" : self.titleLabel};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views: views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views: views]];
    }
    return self;
}

- (UILabel *) titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [[ UILabel alloc ] init ];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.backgroundColor = [ UIColor clearColor ];
        _titleLabel.font = [UIFont systemFontOfSize:15.0];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [ UIColor whiteColor ];
        _titleLabel.numberOfLines = 2;
        _titleLabel.frame = self.contentView.frame;
        _titleLabel.highlightedTextColor = [ UIColor redColor ];
    }
    return _titleLabel;
}

@end
