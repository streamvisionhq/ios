//
//  AppDelegate.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AppDelegate.h"
#import "CoreDataManager.h"
@import SystemConfiguration.CaptiveNetwork;


@interface AppDelegate ()

@property (nonatomic,strong) NSString *prevSSID;
@property (nonatomic,strong) NSTimer *ssidTimer;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
   /* if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }*/
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [self registerDefaultsFromSettingsBundle];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.prevSSID = [defaults valueForKey:@"SSID"];
    
    self.ssidTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(fetchSSIDInfo) userInfo:nil repeats:YES];
    
   [NSThread sleepForTimeInterval:3.0];
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"Recieved Local Notification");
}


- (void)fetchSSIDInfo {
  /* NSArray *ifs = CFBridgingRelease(CNCopySupportedInterfaces());
  //  NSLog(@"Supported interfaces: %@", ifs);
    id info = nil;
    NSString *ifnam = @"";
    for (ifnam in ifs) {
        info = CFBridgingRelease(
                                 CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam));
       // NSLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
        
        
    }
    if ([info count] >= 1 && [ifnam caseInsensitiveCompare:self.prevSSID] !=  NSOrderedSame) {
        // Trigger some event
       
        
    }*/
    
    
    NSDictionary *connectionDetails = [NSDictionary dictionary];
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray) {
        CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        connectionDetails = (__bridge_transfer NSDictionary*)myDict;
    }
    
    NSLog(@"SSID : %@ , OLD SSID : %@",[connectionDetails valueForKey:@"BSSID"],self.prevSSID);
    
    NSString *ssid =[connectionDetails valueForKey:@"BSSID"];
    if([ssid caseInsensitiveCompare:self.prevSSID] !=  NSOrderedSame){
        
        if (!(self.prevSSID == nil)) {
            
            NSLog(@"Changed SSID %@",self.prevSSID);
            self.prevSSID = ssid;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:self.prevSSID forKey:@"SSID"];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kWiFiChangedNotification object:nil];

        }else{
             self.prevSSID = ssid;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:self.prevSSID forKey:@"SSID"];
            [defaults synchronize];

        }
        
    }
      
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.ssidTimer invalidate];
    [[CoreDataManager sharedInstance] saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.prevSSID = [defaults valueForKey:@"SSID"];
     self.ssidTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(fetchSSIDInfo) userInfo:nil repeats:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.prevSSID = [defaults valueForKey:@"SSID"];
     self.ssidTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(fetchSSIDInfo) userInfo:nil repeats:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[CoreDataManager sharedInstance] saveContext];
}

- (void)registerDefaultsFromSettingsBundle
{
    // this function writes default settings as settings
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    
    if(!settingsBundle)
    {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
            NSLog(@"writing as default %@ to the key %@",[prefSpecification objectForKey:@"DefaultValue"],key);
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
