//
//  ChannelTableCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 ChannelTableCell is used for displaying channel cells in MainViewController.
                        ------
 */

#import <UIKit/UIKit.h>

@interface ChannelTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *appLabel;
@property (strong, nonatomic) IBOutlet UIImageView *appImageView;

@end
