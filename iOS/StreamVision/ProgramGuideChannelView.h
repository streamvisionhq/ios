//
//  ProgramGuideChannelView.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//


#import <UIKit/UIKit.h>

/**
 *  The header cell provides two different layouts
 */
typedef NS_ENUM(NSUInteger, ProgramGuideHeaderStyle){
    /**
     *  Default style, provides only an imageView for an icon. Title label and detailLabel will be still be part of the view hierarchy
     */
    ProgramGuideHeaderStyleIconOnly,
    /**
     *  Layout displays a icon, a title label and a secondary label
     */
    ProgramGuideHeaderStyleIconAndLabels
};

@protocol ProgramGuideHeaderDelegate;

/**
 *  The reusable view to be used to display section headers (Channel information). Uses autolayout.
 */
@interface ProgramGuideChannelView : UICollectionReusableView

/**
 *  A label to set the channel name
 */
@property (nonatomic, strong, readonly) UILabel *titleLabel;

/**
 *  A label to display some extra information about the channel
 */
@property (nonatomic, strong, readonly) UILabel *detailLabel;

/**
 *  An imageView to display the channel logo
 */
@property (nonatomic, strong, readonly) UIImageView *imageView;

/**
 *  The cell style
 */
@property (nonatomic, assign) ProgramGuideHeaderStyle cellStyle;

/**
 *  Section header delegate, to notify of user interaction.
 */
@property (nonatomic, weak) NSObject <ProgramGuideHeaderDelegate>* delegate;

/**
 *  A constant string that identifies the cell class uniquely
 *
 *  @return a string that can be used as reuse identifier
 */
+ (NSString *) cellReuseIdentifier;
@end

/**
 *  Section header delegate.
 */
@protocol ProgramGuideHeaderDelegate <NSObject>
/**
 *  Notify of user tapping a section header
 *
 *  @param sectionHeader a reference to the selected section header
 */
- (void)didSelectSectionHeader: (ProgramGuideChannelView *) sectionHeader;
@end