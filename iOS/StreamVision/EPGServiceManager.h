//
//  EPGServiceManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 EPGServiceManager is used to fetch all shows in channels.
                        ------                                                     */

#import <Foundation/Foundation.h>

@interface EPGServiceManager : NSObject

+(instancetype) sharedInstance;

-(void) getEPGForStartDate:(NSDate *) startDate andEndDate:(NSDate *) endDate onSuccess:(void(^)(void))completionBlock onFailure: (ErrorBlock)failureBlock;


@end
