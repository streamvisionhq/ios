//
//  ChannelsTableViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 ChannelsTableViewController is tableview used for displaying channel cells MainViewController.
                        ------
 */

#import <UIKit/UIKit.h>
#import "AllEntities.h"

@protocol ChannelsTableVCDelegate;

@interface ChannelsTableViewController : UITableViewController

@property(nonatomic,strong) id<ChannelsTableVCDelegate> delegate;
-(void) reloadChannels;
-(void) deletaAllChannels;


@end

@protocol ChannelsTableVCDelegate <NSObject>

-(void) launchChannelWithId:(NSString *) channelId;
-(void) setStreaminNowLogo:(Channel *) selectedChannel;

@end
