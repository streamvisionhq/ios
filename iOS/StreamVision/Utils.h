//
//  Utils.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#define SCREEN_FRAME_DEFAULT [Utils screenFrame]
#define SCREEN_CENTER [Utils screenCenter]
#define MAIN_PAGE_FRAME [Utils mainPageFrame]
#define BAR_HEIGHT_DEFAULT  34
#define STATUS_BAR_HEIGHT 20
#define NAVIGATION_BAR_HEIGHT_DEFAULT  44
#define SCROLLVIEW_VERTICAL_DEVIATION_DEFAULT   0
#define SCROLLVIEW_VERTICAL_DEVIATION_MAX       380

static const CGFloat kScrollViewInitialOffset = 307;

// EPG
static const CGFloat kSegmentHeight = 43.0f;
static const NSInteger kLabelTag = 888;
static const NSInteger kImageTag = 889;

@interface Utils : NSObject

+ (CGRect) screenFrame;
+ (CGPoint) screenCenter;
+ (CGRect) mainPageFrame;
+ (NSString *) formatForHeader:(NSDate *) date;
+ (CGSize) headerHeight:(NSString *) text;
+ (NSDateFormatter *) epgFormatter;
+ (NSDateFormatter *) weekdayFormatter;
+ (NSDateFormatter *) calanderFormatter;
+ (NSDateFormatter *) expirationFormatter;
+ (NSNumberFormatter *) episodeNumberFormatter;
+ (NSDate *) startOfTheDay;
+ (NSTimeInterval) sinceDayStart;
+ (BOOL) systemIsIOS8;
+(NSDateFormatter *) hoursMinutesAmPmFormatter;
+ (NSDateFormatter *) streamVsisionDateFormatter;
+(NSString *) getEncodedString:(NSString *) rawString;
@end