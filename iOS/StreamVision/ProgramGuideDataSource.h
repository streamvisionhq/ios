//
//  ProgramGuideDataSource.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UICollectionView.h>

@class TimeRange;

/**
 *  Protocol specific to the ProgramGuide. It extends UICollectionViewDataSource and provides extra methods to allow the layout to get the information it needs to render its cells in the proper size
 */
@protocol ProgramGuideDataSource <UICollectionViewDataSource>

/**
 *  Get the TimeRange of a Program
 *
 *  @param collectionView the collectionview sending the message
 *  @param layout         the layout originating the message
 *  @param indexPath      the indexpath of the item
 *
 *  @return an instance of TimeRange containing the initial date of the item and its duration
 */
- (TimeRange *)collectionView: (UICollectionView *) collectionView layout: (UICollectionViewLayout *) layout timeRangeOfItemAtIndexPath: (NSIndexPath *)indexPath;

/**
 *  Get the TimeRange to be displayed in the collectionview. It can be useful to paginate a ProgramGuide by days, for example.
 *
 *  @param collectionView the collectionview sending the message
 *  @param layout         the layout sending the message
 *
 *  @return a TimeRange modeling the initial date and duration of the visible part of the ProgramGuide
 */
- (TimeRange *) timeRangeOfCollectionView: (UICollectionView *) collectionView layout: (UICollectionViewLayout *) layout;
@end
