//
//  EPG.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "EPG.h"
#import "Channel.h"


@implementation EPG

@dynamic epgValue;
@dynamic error;
@dynamic utcOffset;
@dynamic observesDls;
@dynamic startTimeUtc;
@dynamic duration;
@dynamic rawData;
@dynamic channels;

@end
