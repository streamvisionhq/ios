//
//  ProgramGuideCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//


#import "ProgramGuideCell.h"
#import "UIView+AutolayoutExtensions.h"


static const NSUInteger kCellContentInset       = 6;
static const NSUInteger kTitleLabelHeight       = 20;
static const NSUInteger kImageViewWidth         = 20;
static const NSUInteger kImageViewWidthHidden   = 0;
static const CGFloat kMotionEffectMaxValue      = 20.0f;

static NSString *const kImageWidthKey       = @"imageWidth";
static NSString *const kTitleLabelHeightKey = @"titleLabelHeight";
static NSString *const kCellContentInsetKey = @"contentInset";

static NSString *const kCellReuseIdentifier = @"programGuideCell";

@interface ProgramGuideCell( )
@property (nonatomic, strong, readwrite) UILabel *titleLabel;
@property (nonatomic, strong, readwrite) UILabel *detailLabel;
@property (nonatomic, strong, readwrite) UIImageView *imageView;

@property (nonatomic, strong, readwrite) NSMutableArray *labelsOnlyStyleConstraints;
@property (nonatomic, strong, readwrite) NSMutableArray *labelsAndIconStyleConstraints;

- (void) initFontListener;
- (void) initConstraints;
- (void) applyConstraints;

- (NSArray *) layoutConstraintsWithMetrics: (NSDictionary *) metrics;

- (void) configureView;
- (void) setLabelsFonts;
- (void) attachMotionEffects;

@end

@implementation ProgramGuideCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.cellStyle = ProgramGuideCellStyleLabelsOnly;
        [ self configureView ];
    }
    
    return self;
}

- (void) initFontListener
{
    [ [ NSNotificationCenter defaultCenter ] addObserver: self
                                                selector: @selector( setLabelsFonts )
                                                    name: UIContentSizeCategoryDidChangeNotification
                                                  object: nil ];
}

- (void) dealloc
{
    [ [ NSNotificationCenter defaultCenter ] removeObserver: self ];
}

- (void) configureView
{
    [ self.contentView addSubview: self.imageView ];
    [ self.contentView addSubview: self.titleLabel ];
    [ self.contentView addSubview: self.detailLabel ];
    
    self.labelsOnlyStyleConstraints     = [ [ NSMutableArray alloc ] init ];
    self.labelsAndIconStyleConstraints  = [ [ NSMutableArray alloc ] init ];
    
    [ self setLabelsFonts ];
    
    [ self initConstraints ];
    
    [ self applyConstraints ];
    
    [ self initFontListener ];
    
    if( [ self respondsToSelector:@selector(addMotionEffect:) ] )
    {
        [ self attachMotionEffects ];
    }
}

- (void) attachMotionEffects
{
    UIInterpolatingMotionEffect *interpolationHorizontal = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    interpolationHorizontal.minimumRelativeValue = @(-1*kMotionEffectMaxValue);
    interpolationHorizontal.maximumRelativeValue = @(kMotionEffectMaxValue);
    
    UIInterpolatingMotionEffect *interpolationVertical = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    interpolationVertical.minimumRelativeValue = @(-1* kMotionEffectMaxValue);
    interpolationVertical.maximumRelativeValue = @(kMotionEffectMaxValue);
    
    UIMotionEffectGroup *effectGroup = [[ UIMotionEffectGroup alloc ] init ];
    effectGroup.motionEffects = @[ interpolationHorizontal, interpolationVertical ];
    
    [ self.titleLabel addMotionEffect: effectGroup ];
    [ self.detailLabel addMotionEffect: effectGroup ];
    [ self.imageView addMotionEffect: effectGroup ];
}

- (void) setLabelsFonts
{
    [ self.titleLabel setFont:  [ UIFont systemFontOfSize:15.0 ] ];
    [ self.detailLabel setFont: [ UIFont systemFontOfSize:13.0 ] ];
}

- (UILabel *) titleLabel
{
    if( !_titleLabel )
    {
        _titleLabel = [ UILabel autoLayoutView ];
    }
    
    return _titleLabel;
}

- (UILabel *) detailLabel
{
    if( !_detailLabel )
    {
        _detailLabel = [ UILabel autoLayoutView ];
    }
    
    return _detailLabel;
}

- (UIImageView *) imageView
{
    if( !_imageView )
    {
        _imageView = [ UIImageView autoLayoutView ];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _imageView;
}

- (void) setCellStyle:(ProgramGuideCellStyle)cellStyle
{
    if( _cellStyle != cellStyle )
    {
        _cellStyle = cellStyle;
        
        [ self applyConstraints ];
    }
}

- (void) applyConstraints
{
    if( _cellStyle == ProgramGuideCellStyleLabelsOnly )
    {
        [ self.contentView removeConstraints: self.labelsAndIconStyleConstraints ];
        [ self.contentView addConstraints: self.labelsOnlyStyleConstraints ];
    }
    else
    {
        [ self.contentView removeConstraints: self.labelsOnlyStyleConstraints ];
        [ self.contentView addConstraints: self.labelsAndIconStyleConstraints ];
    }
    
    [ self layoutIfNeeded ];
}

- (void) initConstraints
{
    NSDictionary *labelsOnlyMetrics = @{ kImageWidthKey: @( kImageViewWidthHidden ), kCellContentInsetKey: @( kCellContentInset ), kTitleLabelHeightKey: @( kTitleLabelHeight ) };
    [ self.labelsOnlyStyleConstraints addObjectsFromArray: [ self layoutConstraintsWithMetrics: labelsOnlyMetrics ] ];
    
    NSDictionary *labelsAndIconMetrics = @{ kImageWidthKey: @( kImageViewWidth ), kCellContentInsetKey: @( kCellContentInset ), kTitleLabelHeightKey: @( kTitleLabelHeight ) };
    [ self.labelsAndIconStyleConstraints addObjectsFromArray: [ self layoutConstraintsWithMetrics: labelsAndIconMetrics ] ];
}

- (NSArray *) layoutConstraintsWithMetrics: (NSDictionary *) metrics
{
    NSMutableArray *constraints = [ [ NSMutableArray alloc ] init ];
    
    CGFloat defaultInset = [ metrics[ kCellContentInsetKey ] floatValue ];
    
    [ constraints addObjectsFromArray: [ self.imageView pinToSuperviewEdges: ViewLeftEdge inset: defaultInset ] ];
    [ constraints addObject: [ self.titleLabel pinEdge: NSLayoutAttributeLeft toEdge: NSLayoutAttributeRight ofItem: self.imageView inset: defaultInset ] ];
    [ constraints addObjectsFromArray: [ self.titleLabel pinToSuperviewEdges: ViewRightEdge inset: defaultInset ] ];
    
    [ constraints addObject: [ self.titleLabel constrainToHeight: [ metrics[ kTitleLabelHeightKey ] floatValue ] ] ];
    
    [ constraints addObject: [ self.titleLabel centerInContainerOnAxis: NSLayoutAttributeCenterY ] ];
    
    [ constraints addObject: [ self.imageView centerInContainerOnAxis: NSLayoutAttributeCenterY ] ];
    
    [ constraints addObject: [ self.imageView constrainToHeight: [ metrics[ kImageWidthKey ] floatValue ] ] ];
    [ constraints addObject: [ self.imageView constrainToWidth: [ metrics[ kImageWidthKey ] floatValue ] ] ];
    
    [ constraints addObjectsFromArray: [ self.detailLabel pinEdges: ViewLeftEdge | ViewRightEdge toSameEdgesOfView: self.titleLabel ] ];
    
    [ constraints addObject: [ self.detailLabel pinEdge: NSLayoutAttributeTop toEdge: NSLayoutAttributeBottom ofItem: self.titleLabel inset: defaultInset ] ];
    
    return constraints;
}

+ (NSString *) cellReuseIdentifier
{
    return kCellReuseIdentifier;
}
@end

