//
//  AsynchronousBlockOperation.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef void (^AsynOperationBlock)(void);

/**
 *  An asynchronous operation that will wait until itself being marked as finished upon completion of the block.
 */
@interface AsynchronousBlockOperation : NSOperation
@property (nonatomic, assign, readonly) BOOL isExecuting;
@property (nonatomic, assign, readonly) BOOL isFinished;

- (void) addOperationBlock: (AsynOperationBlock) block;
- (void) markAsFinished;

@end
