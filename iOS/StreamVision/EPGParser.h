//
//  EPGParser.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPGParser : NSObject


-(void) parseEPGData:(NSData *) data;

@end
