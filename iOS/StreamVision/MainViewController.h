//
//  MainViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
        MainViewController is main screen controller which displays roku apps and channels with their controlls.
                        ------
 */

#import <UIKit/UIKit.h>
#import <ConnectSDK/ConnectSDK.h>

@interface MainViewController : UIViewController<NSStreamDelegate>{
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    NSMutableArray * messages;

}

@property (nonatomic, assign) ConnectableDevice *device;
@property (nonatomic,strong) NSString *connectButtonTitle;
- (void)showSettingsFromEpg;
@end
