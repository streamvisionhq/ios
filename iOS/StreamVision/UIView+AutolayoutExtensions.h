//
//  UIView+AutolayoutExtensions.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Binary flags to refer to a view edges.
 */
typedef NS_OPTIONS(unsigned long, ViewEdges){
    /**
     *  Top edge
     */
    ViewTopEdge = 1 << 0,
    /**
     *  Right edge
     */
    ViewRightEdge = 1 << 1,
    /**
     *  Bottom edge
     */
    ViewBottomEdge = 1 << 2,
    /**
     *  Left edge
     */
    ViewLeftEdge = 1 << 3,
    /**
     *  All edges
     */
    ViewAllEdges = ~0UL
};

/**
 *  An class extension on UIView that provides helper methods to make autolayout easier to implement procedurally
 */
@interface UIView (AutolayoutExtensions)

/**
 *  Convenience constructor that instantiates a UIView prepared to be used in autolayout
 *
 *  @return an instance of UIView created with a frame equal to CGRectZero and translatesAutoresizingMaskIntoConstraints set to NO
 */
+(instancetype)autoLayoutView;

/**
 *  Center view in a view
 *
 *  @param superview the view we want to center into
 *
 *  @return an array of NSLayoutConstraints
 */
-(NSArray *)centerInView:(UIView*)superview;

/**
 *  Center a view in a given dimension
 *
 *  @param axis the dimension we want the view to be centered in
 *
 *  @return A layout constraint
 */
-(NSLayoutConstraint *)centerInContainerOnAxis:(NSLayoutAttribute)axis;

/**
 *  Pin a view to a combination of edges of its superview, using the layout guides from a UIViewContainer
 *
 *  @param edges          the edges to pin the view to
 *  @param inset          the distance to the superview edge
 *  @param viewController a viewcontroller to get layout guides from
 *
 *  @return an array of NSLayoutContraints
 */
-(NSArray*)pinToSuperviewEdges:(ViewEdges)edges inset:(CGFloat)inset usingLayoutGuidesFrom:(UIViewController*)viewController;

/**
 *  Pin view to the edges of its superview
 *
 *  @param edges the edges to pin the view to
 *  @param inset the distance to the edges
 *
 *  @return an array of NSLayoutConstraints
 */
-(NSArray*)pinToSuperviewEdges:(ViewEdges)edges inset:(CGFloat)inset;

/**
 *  Pin view to the edges of its superview with UIEdgeInsets
 *
 *  @param insets insets to be applied
 *
 *  @return a collection of NSLayoutConstraints
 */
-(NSArray*)pinToSuperviewEdgesWithInset:(UIEdgeInsets)insets;

/**
 *  Create contraints for a given edge, relative to other view's edge
 *
 *  @param edge     the view edge to be set
 *  @param toEdge   the view edge to pin to
 *  @param peerItem the item (it can be a view or a view controller) to pin to
 *  @param inset    the distance to the other view's edge
 *
 *  @return A layout constraint setting a given edge of a view relative to a given edge of another view
 */
-(NSLayoutConstraint *)pinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofItem:(id)peerItem inset:(CGFloat)inset;

/**
 *  Pin edges of a view relative to the edges of another view
 *
 *  @param edges    edges to add constraints to
 *  @param peerView edges that the new constraints will be relative to
 *
 *  @return a collection of NSLayoutContraints
 */
-(NSArray *)pinEdges:(ViewEdges)edges toSameEdgesOfView:(UIView *)peerView;

/**
 *  Pin edges of a view relative to the edges of another view, with a given inset
 *
 *  @param edges    edges to add constraints to
 *  @param peerView edges that the new constraints will be relative to
 *  @param inset    the distance between edges
 *
 *  @return a collection of NSLayoutContraints
 */
-(NSArray *)pinEdges:(ViewEdges)edges toSameEdgesOfView:(UIView *)peerView inset:(CGFloat)inset;

/**
 *  Constrain the size of a view to a given value
 *
 *  @param size the desired size
 *
 *  @return a collection of NSLaoutContraints
 */
-(NSArray *)constrainToSize:(CGSize)size;

/**
 *  Set a view's width
 *
 *  @param width the view width
 *
 *  @return a NSLayoutConstraint
 */
-(NSLayoutConstraint *)constrainToWidth:(CGFloat)width;

/**
 *  Set a view's height
 *
 *  @param height the view height
 *
 *  @return a NSLayoutConstraint
 */
-(NSLayoutConstraint *)constrainToHeight:(CGFloat)height;

/**
 *  Pin the view center's X coordinate to the center's X coordinate of a given view
 *
 *  @param peerView the view we want to pin to
 *
 *  @return a NSLayoutConstraint
 */
-(NSLayoutConstraint *) pinCenterXToView: (UIView *) peerView;

/**
 *  Pin the view center's Y coordinate to the center's Y coordinate of a given view
 *
 *  @param peerView the view we want to pin to
 *
 *  @return a NSLayoutConstraint
 */
-(NSLayoutConstraint *) pinCenterYToView: (UIView *) peerView;

/**
 *  Set a minimum and maximum size for a view
 *
 *  @param minimum the view's minimum size
 *  @param maximum the view's maximum size
 *
 *  @return a collection of NSLayoutContraints
 */
-(NSArray *)constrainToMinimumSize:(CGSize)minimum maximumSize:(CGSize)maximum;

/**
 *  Set a minimum size
 *
 *  @param minimum the view's minimum size
 *
 *  @return a collection of NSLayoutContraints
 */
-(NSArray *)constrainToMinimumSize:(CGSize)minimum;

/**
 *  Limit the view's maximum size
 *
 *  @param maximum the maximum size
 *
 *  @return a collection of NSLayoutConstraints
 */
-(NSArray *)constrainToMaximumSize:(CGSize)maximum;

@end