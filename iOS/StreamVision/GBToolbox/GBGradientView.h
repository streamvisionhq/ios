//
//  GBGradientView.h
//  Pods
//
//  Created by Luka Mirosevic on 13/11/2014.
//
//

#import <UIKit/UIKit.h>

@interface GBGradientView : UIView

/**
 Creates a UIView with it's background set to a gradient.
 */
+ (UIView *)viewWithGradientWithColors:(NSArray *)colors;

@end
