//
//  SettingsViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 08/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "RokuListingViewController.h"
#import "CoreDataManager.h"
#import "RokuListingTableCell.h"
#import "AuthenticationManager.h"
#import "StreamVisionDeviceManager.h"
#import "SVConnectedDevicesManager.h"
#define TIMER_LIMIT 5

static NSString *CellIdentifier = @"RokuListingCell";

@interface RokuListingViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ListingCellDelegate,ConnectableDeviceDelegate>
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UITableView *devicesList;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *currentDeviceId;
@property (nonatomic,strong) StreamVisionDeviceManager *deviceManager;
@property (nonatomic, strong) Device *selectedDevice;
@property (nonatomic, strong) ConnectableDevice *deviceToConnect;
@property (nonatomic,strong) NSTimer *discoveryTimer;
@property NSUInteger timeoutCounter;
@property (nonatomic,strong) SVConnectedDevicesManager *connectedDeviceManager;
@property NSUInteger selectedRow;


@end

@implementation RokuListingViewController

@synthesize newUser;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectedRow = -1;
    
    self.connectedDeviceManager = [SVConnectedDevicesManager sharedInstance];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(foundDeviceInNetwork:) name:kFoundDeviceNotification object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lostActiveDevice:) name:kLostDeviceNotification object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.currentDeviceId = [defaults valueForKey:kCurrentSelectedDeviceIdKey];
    //[defaults synchronize];

    NSLog(@"Current Device Id: %@",self.currentDeviceId);
    
    User *currentUser = [[CoreDataManager sharedInstance] getUser];
    self.userId = currentUser.registrationNumber;
    
    self.dataArray = [NSMutableArray arrayWithArray:[[CoreDataManager sharedInstance] getAllDevices]];
    
    [self.devicesList reloadData];
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    [self startFindingDevices];
    
}

-(void) startTimer{
    
    self.timeoutCounter = 0;
    self.discoveryTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                           target: self
                                                         selector: @selector(isDiscoveryTimedOut:)
                                                         userInfo: nil
                                                          repeats: YES];
    
}


-(void)isDiscoveryTimedOut:(NSTimer *)timer {
    
    self.timeoutCounter++;
    if (self.timeoutCounter > TIMER_LIMIT) {
        
        [self.discoveryTimer invalidate];
        [SVProgressHUD dismiss];
        
    }
   
}



-(void) startFindingDevices{
    
    [[CoreDataManager sharedInstance] setAllDevicesOffline];
    [self startTimer];
    self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    [self.deviceManager stopFinding];
    if (self.newUser) {
        [SVProgressHUD showWithStatus:@"Checking for new devices in network please wait ..."];
    }
    
    [self.deviceManager findDevice];
    [self.devicesList reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismiss:(id)sender {
    
    [self invalidateDiscovery];
    [SVProgressHUD dismiss];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
    }];
}

#pragma mark UITableViewDelegate

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    RokuListingTableCell *cell = (RokuListingTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Device *aDevice = [self.dataArray objectAtIndex:indexPath.row];

    cell.identfier.text = aDevice.deviceLocation;
    cell.deviceId.text = aDevice.deviceIdentifier;
    cell.serviceType.text = aDevice.serviceType;
    cell.registered.text = aDevice.accountId;
   
    [cell.selectButton setTag:indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    
     if ([aDevice.isOnline boolValue]){
         if ([self.currentDeviceId isEqualToString:aDevice.deviceIdentifier]) {
            [cell.selectButton setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
        }
        else {
             [cell.selectButton setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
         }
    }
    
    if ([aDevice.isOnline boolValue]) {
        [cell.onlineImageView setImage:[UIImage imageNamed:@"on"]];
        [cell.selectButton setEnabled:YES];
        [cell setUserInteractionEnabled:YES];
        [SVProgressHUD dismiss];
    }else{
        [cell.onlineImageView setImage:[UIImage imageNamed:@"off"]];
        [cell.selectButton setEnabled:NO];
        [cell setUserInteractionEnabled:NO];
        [cell.selectButton setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        
    }
    
    cell.delegate = self;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}

-(void)lostActiveDevice:(NSNotification *) note{
    
    [self.dataArray removeAllObjects];
    [self.dataArray addObjectsFromArray:[[CoreDataManager sharedInstance] getAllDevices]];
    
    [self.devicesList reloadData];
    
}

#pragma mark UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.text.length > 0) {
        self.userId = textField.text;
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];

    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

#pragma mark Button Actions

- (IBAction)updateTapped:(id)sender {
    
    //Delete Devices which are registered
    
   /*[[CoreDataManager sharedInstance] deleteRegisteredDevices];
    
    //Make Service Call and Relaod Table
    
    
    
    self.dataArray = [[CoreDataManager sharedInstance] getAllDevices];
    
    [self.devicesList reloadData];*/

    
}

- (IBAction)refreshTapped:(id)sender {
    
    
    [SVProgressHUD showWithStatus:kWaitMessage];
    [self startFindingDevices];
    
   // [self.discoveryTimer invalidate];
   // [self startFindingDevices];
    
    //[[AuthenticationManager sharedInstance] getRegisteredDevicesForId:self.userId onSuccess:^(NSArray *devicesList){
        
    //    [[CoreDataManager sharedInstance] addRegisteredDevices:devicesList];
        
     //   self.deviceManager = [StreamVisionDeviceManager sharedInstance];
     //   NSArray *allDevices = [self.deviceManager discoveredDvicesArray];
     //   [[CoreDataManager sharedInstance] updateOnlineStatus:allDevices];
        
    ////    [self.dataArray removeAllObjects];
     //   [self.dataArray addObjectsFromArray:[[CoreDataManager sharedInstance] getAllDevices]];
        
    //    [self.devicesList reloadData];
       
        
        
   // }onFailure:^(NSError *error){
        
      //  NSLog(@"Error");
       
   // }];
    


}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Device *aDevice = [self.dataArray objectAtIndex:indexPath.row];

    if ([aDevice.isOnline boolValue]) {
        
    //Disconnect Current Device and connect New device
    self.selectedDevice =[self.dataArray objectAtIndex:indexPath.row];
    
    self.currentDeviceId = self.selectedDevice.deviceIdentifier;
    self.dataArray = [NSMutableArray arrayWithArray:[[CoreDataManager sharedInstance] getAllDevices]];
    [self.devicesList reloadData];
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:kDisconnectDeviceNotification object:nil];
    
    [SVProgressHUD showWithStatus:kWaitMessage];
    self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    NSArray *allDevices = [self.deviceManager discoveredDvicesArray];
        for(ConnectableDevice *dev in allDevices){

        //NSLog(@" dev Id : %@   %@",dev.id,dev.friendlyName);
        NSLog(@" selected Dev Id : %@",self.selectedDevice.deviceIdentifier);
        NSArray *servicesArray = dev.services;
        DeviceService *service = [servicesArray objectAtIndex:0];

        ServiceDescription *serviceDesc = service.serviceDescription;
        NSLog(@"UUID : %@",serviceDesc.UUID);
        
        
        if ([serviceDesc.UUID containsString:self.currentDeviceId]) {
            //found device
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:self.currentDeviceId forKey:kCurrentSelectedDeviceIdKey];
            [defaults synchronize];
            
            self.deviceToConnect = dev;
            [self connectNewDevice];
            break;
        }

        
    }
        
    }else{
        
        return;
    }
    
    
}


-(void) selectedDeviceAtIndex:(NSUInteger)index{
    
    Device *aDevice = [self.dataArray objectAtIndex:index];
    
    if ([aDevice.isOnline boolValue]) {
    
    //Disconnect Current Device and connect New device
    self.selectedDevice =[self.dataArray objectAtIndex:index];
    self.currentDeviceId = self.selectedDevice.deviceIdentifier;
    self.dataArray = [NSMutableArray arrayWithArray:[[CoreDataManager sharedInstance] getAllDevices]];
    [self.devicesList reloadData];
    
    [SVProgressHUD showWithStatus:kWaitMessage];
    self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    NSArray *allDevices = [self.deviceManager discoveredDvicesArray];

    for(ConnectableDevice *dev in allDevices){
    
        NSLog(@" dev Id : %@   %@",dev.id,dev.friendlyName);
        NSLog(@" selected Dev Id : %@",self.selectedDevice.rokuDeviceId);
        
        
        NSArray *servicesArray = dev.services;
        DeviceService *service = [servicesArray objectAtIndex:0];
        ServiceDescription *serviceDesc = service.serviceDescription;
        NSLog(@"UUID : %@",serviceDesc.UUID);

        
        if ([serviceDesc.UUID containsString:self.currentDeviceId]) {
            //found device
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:self.currentDeviceId forKey:kCurrentSelectedDeviceIdKey];
            [defaults synchronize];
            
            self.deviceToConnect = dev;
            [self connectNewDevice];
            break;
        }
        
    }
        
    }else{
        
        return;
    }
    
}

-(void) connectNewDevice{
   // [self.devicesList reloadData];
    /* When pairing level is on, by default pairing type is DeviceServicePairingTypeFirstScreen.You can also set pairing type to DeviceServicePairingTypePinCode/DeviceServicePairingTypeMixed */
    self.deviceToConnect.delegate = self;
    [self.deviceToConnect setPairingType:DeviceServicePairingTypeFirstScreen];
    [self.deviceToConnect connect];
}

- (void) connectableDeviceReady:(ConnectableDevice *)device
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       //[[NSNotificationCenter defaultCenter] postNotificationName:kDisconnectDeviceNotification object:nil];
                       [self invalidateDiscovery];
                       //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                       //[defaults setValue:self.selectedDevice.deviceIdentifier forKey:kCurrentSelectedDeviceIdKey];
                       //[defaults synchronize];
                       [self.connectedDeviceManager setConnectedRokuDevice:self.deviceToConnect];
                       [self.connectedDeviceManager setConnectedSVDevice:self.selectedDevice];
                       [SVProgressHUD dismiss];
                       [self dismissViewControllerAnimated:YES completion:^(void){
                           NSLog(@"Need to Load New Device Apps n all");
                           [[NSNotificationCenter defaultCenter] postNotificationName:kDidConnectToNewDeviceNotification object:self.deviceToConnect];
                           [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
                       }];
                   });
}

- (void) connectableDeviceConnectionSuccess:(ConnectableDevice*)device forService:(DeviceService *)service{

    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       //[[NSNotificationCenter defaultCenter] postNotificationName:kDisconnectDeviceNotification object:nil];
                       [self invalidateDiscovery];
                       //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                       //[defaults setValue:self.selectedDevice.deviceIdentifier forKey:kCurrentSelectedDeviceIdKey];
                       //[defaults synchronize];
                       [self.connectedDeviceManager setConnectedRokuDevice:self.deviceToConnect];
                       [self.connectedDeviceManager setConnectedSVDevice:self.selectedDevice];
                       [SVProgressHUD dismiss];
                       [self dismissViewControllerAnimated:YES completion:^(void){
                           NSLog(@"Need to Load New Device Apps n all");
                           [[NSNotificationCenter defaultCenter] postNotificationName:kDidConnectToNewDeviceNotification object:self.deviceToConnect];
                           [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
                       }];
                   });

    
    
}

- (void) connectableDevice:(ConnectableDevice *)device service:(DeviceService *)service pairingRequiredOfType:(int)pairingType withData:(id)pairingData
{
    if (pairingType == DeviceServicePairingTypeAirPlayMirroring)
        [(UIAlertView *) pairingData show];
}

- (void) connectableDeviceDisconnected:(ConnectableDevice *)device withError:(NSError *)error
{
    self.deviceToConnect.delegate = nil;
    self.deviceToConnect = nil;
    
    NSLog(@"Connection Disconnected");
    [[NSNotificationCenter defaultCenter] postNotificationName:kLostDeviceNotification object:device];
    

    
   /* UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect to Device" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [errorAlert show];*/

    
}


- (void) connectableDevice:(ConnectableDevice *)device connectionFailedWithError:(NSError *)error{
    
    NSLog(@"Error");
    
    
    NSLog(@"Connection Failed");
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect to Device, please check your network." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [errorAlert show];
}

-(void) foundDeviceInNetwork:(NSNotification *) note{
    
    [self.dataArray removeAllObjects];
    self.dataArray = [NSMutableArray arrayWithArray:[[CoreDataManager sharedInstance] getAllDevices]];
    [self.devicesList reloadData];
}

-(void) invalidateDiscovery{
    
    [self.discoveryTimer invalidate];
   // [self.deviceManager stopFinding];
    
}

- (void) connectableDevice:(ConnectableDevice*)device service:(DeviceService *)service disconnectedWithError:(NSError*)error{
    
    NSLog(@"Service Disconnected With Error");
}





@end
