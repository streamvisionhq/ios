//
//  SettingsViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 08/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
 RokuListingViewController is used for displaying all the devices available for logged in Customer
                        ------
 */

#import <UIKit/UIKit.h>

@interface RokuListingViewController : UIViewController

@property  BOOL newUser;

@end
