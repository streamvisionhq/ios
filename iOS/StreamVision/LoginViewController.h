//
//  LoginViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 LoginViewController is used for Customer ID login with authontication functionality.
                        ------
 */

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnProfileDD;
@property (weak, nonatomic) IBOutlet UITextField *profileField;


-(void )doLogin;
- (IBAction)onProfileDD:(id)sender;

@end

