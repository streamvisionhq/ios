//
//  ViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
 LaunchViewController is the launching screen .
                        ------
 */

#import <UIKit/UIKit.h>

@interface LaunchViewController : UIViewController


@end

