//
//  Audio.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Show;

@interface Audio : NSManagedObject

@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * audioServiceType;
@property (nonatomic, retain) NSString * numberOfChannels;
@property (nonatomic, retain) NSString * bitRateKbps;
@property (nonatomic, retain) NSNumber * exactBitRate;
@property (nonatomic, retain) NSNumber * surround;
@property (nonatomic, retain) NSNumber * fullService;
@property (nonatomic, retain) Show *show;

@end
