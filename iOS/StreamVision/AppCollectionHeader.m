//
//  AppCollectionHeader.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AppCollectionHeader.h"
#import "UIImageView+WebCache.h"

@interface AppCollectionHeader()
    @property (nonatomic,strong) AppInfo *svApp;
@end
    
    
@implementation AppCollectionHeader

@synthesize deviceTargetURL;

- (void)awakeFromNib {
    // Initialization code
}


-(void) setUpwithApp:(AppInfo *) app{
    
    self.svApp = app;
    
    [self.imageView setUserInteractionEnabled:YES];
    
    NSString *imageURLStr = [NSString stringWithFormat:@"%@/query/icon/%@",self.deviceTargetURL,app.id];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageURLStr] placeholderImage:[UIImage imageNamed:@"sample_app_icon"] options:SDWebImageProgressiveDownload];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTapped:)];
    tap.numberOfTapsRequired = 1;
    [self.imageView addGestureRecognizer:tap];
    
}


-(void)imageViewTapped:(id) sender{
    
    if ([self.delegate respondsToSelector:@selector(headerSelected:)]) {
        
        [self.delegate headerSelected:self.svApp];
        
    }
}

@end
