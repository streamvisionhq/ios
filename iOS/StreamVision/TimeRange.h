//
//  TimeRange.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
#import <Foundation/Foundation.h>

/**
 *  A clase to model a time range, in a similar fashion to NSRange.
 */
@interface TimeRange : NSObject
/**
 *  Starting date of the time range
 */
@property (nonatomic, strong, readonly) NSDate *date;
/**
 *  Duration of the time range in seconds
 */
@property (nonatomic, assign, readonly) NSTimeInterval duration;

/**
 *  Designated initializer.
 *
 *  @param date     initial date
 *  @param duration time range duration in seconds.
 *
 *  @return a new instance of TimeRange, with initial date and duration already set
 */
- (instancetype) initWithDate: (NSDate *) date duration: (NSTimeInterval) duration;
@end

