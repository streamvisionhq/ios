//
//  ProgramGuideChannelView.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//


#import "ProgramGuideChannelView.h"
#import "UIView+AutolayoutExtensions.h"


static const NSUInteger kCellContentInset   = 6;
static NSString *const kCellContentInsetKey = @"contentInset";

static const NSUInteger kImageViewHeight     = 46;
static const NSUInteger kImageViewWidth      = 80;
static const NSUInteger kTitleLabelHeight    = 20;
static const NSUInteger kDetailLabelHeight   = 10;
static NSString *const kImageViewHeightKey   = @"imageViewWidth";

static NSString *const kCellReuseIdentifier = @"programGuideChannelCell";

@interface ProgramGuideChannelView( )
@property (nonatomic, strong, readwrite) UILabel *titleLabel;
@property (nonatomic, strong, readwrite) UILabel *detailLabel;
@property (nonatomic, strong, readwrite) UIImageView *imageView;

@property (nonatomic, strong, readwrite) NSMutableArray *iconOnlyStyleConstraints;
@property (nonatomic, strong, readwrite) NSMutableArray *iconAndLabelsStyleConstraints;

- (void) initConstraints;
- (void) applyConstraints;

- (NSArray *) layoutConstraintsForIconOnly;
- (NSArray *) layoutConstraintsForIconAndLabels;

@end

@implementation ProgramGuideChannelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.cellStyle = ProgramGuideHeaderStyleIconOnly;
        
        [ self addSubview: self.titleLabel ];
        [ self addSubview: self.detailLabel ];
        [ self addSubview: self.imageView ];
        
        self.iconOnlyStyleConstraints = [ [ NSMutableArray alloc ] init ];
        self.iconAndLabelsStyleConstraints = [ [ NSMutableArray alloc ] init ];
        
        [ self initConstraints ];
        
        [ self applyConstraints ];
    }
    return self;
}
- (void) setCellStyle:(ProgramGuideHeaderStyle)cellStyle
{
    if( _cellStyle != cellStyle )
    {
        _cellStyle = cellStyle;
        
        [ self applyConstraints ];
    }
}

- (void) applyConstraints
{
    if( _cellStyle == ProgramGuideHeaderStyleIconOnly )
    {
        [ self removeConstraints: self.iconAndLabelsStyleConstraints ];
        [ self addConstraints: self.iconOnlyStyleConstraints ];
    }
    else
    {
        [ self removeConstraints: self.iconOnlyStyleConstraints ];
        [ self addConstraints: self.iconAndLabelsStyleConstraints ];
    }
    
    [ self layoutIfNeeded ];
}

- (void) initConstraints
{
    [ self.iconOnlyStyleConstraints addObjectsFromArray: [ self layoutConstraintsForIconOnly ] ];
    
    [ self.iconAndLabelsStyleConstraints addObjectsFromArray: [ self layoutConstraintsForIconAndLabels ] ];
}

- (NSArray *) layoutConstraintsForIconOnly
{
    NSMutableArray *constraints = [ [ NSMutableArray alloc ] init ];
    
    [ constraints addObject: [ self.imageView centerInContainerOnAxis: NSLayoutAttributeCenterX ] ];
    [ constraints addObject: [ self.imageView centerInContainerOnAxis: NSLayoutAttributeCenterY ] ];
    [ constraints addObjectsFromArray: [ self.imageView constrainToMaximumSize:( CGSize ) { kImageViewWidth, kImageViewHeight } ] ];
    
    [ constraints addObjectsFromArray: [ self.titleLabel pinToSuperviewEdges: ViewLeftEdge inset: 0 ] ];
    [ constraints addObjectsFromArray: [ self.titleLabel pinToSuperviewEdges: ViewBottomEdge inset: 0 ] ];
    
    [ constraints addObjectsFromArray: [ self.detailLabel pinEdges: ViewLeftEdge | ViewRightEdge toSameEdgesOfView: self.titleLabel ] ];
    
    [ constraints addObject: [ self.detailLabel constrainToWidth: 0 ] ];
    
    return constraints;
}

- (NSArray *) layoutConstraintsForIconAndLabels
{
    NSMutableArray *constraints = [ [ NSMutableArray alloc ] init ];
    
    [ constraints addObject: [ self.imageView centerInContainerOnAxis: NSLayoutAttributeCenterX ] ];
    [ constraints addObjectsFromArray: [ self.imageView pinToSuperviewEdges: ViewTopEdge inset: kCellContentInset ] ];
    [ constraints addObjectsFromArray: [ self.imageView pinToSuperviewEdges: ViewLeftEdge | ViewRightEdge inset: kCellContentInset ] ];
    [ constraints addObjectsFromArray: [ self.imageView constrainToMinimumSize: ( CGSize ) { kImageViewHeight , kImageViewHeight } ] ];
    
    
    [ constraints addObjectsFromArray: [ self.titleLabel pinToSuperviewEdges: ViewLeftEdge | ViewRightEdge inset: kCellContentInset ] ];
    [ constraints addObject: [ self.titleLabel pinEdge: NSLayoutAttributeTop toEdge: NSLayoutAttributeBottom ofItem: self.imageView inset: kCellContentInset ] ];
    [ constraints addObject: [ self.titleLabel constrainToHeight: kTitleLabelHeight ] ];
    
    [ constraints addObjectsFromArray: [ self.detailLabel pinEdges: ViewLeftEdge | ViewRightEdge toSameEdgesOfView: self.titleLabel ] ];
    [ constraints addObject: [ self.detailLabel pinEdge: NSLayoutAttributeTop toEdge: NSLayoutAttributeBottom ofItem: self.titleLabel inset: kCellContentInset ] ];
    [ constraints addObject: [ self.detailLabel constrainToHeight: kDetailLabelHeight ] ];
    
    return constraints;
}


- (UILabel *) titleLabel
{
    if( !_titleLabel )
    {
        _titleLabel = [ UILabel autoLayoutView ];
        [ _titleLabel setFont: [ UIFont systemFontOfSize:15.0f ] ];
    }
    return _titleLabel;
}

- (UILabel *) detailLabel
{
    if( !_detailLabel )
    {
        _detailLabel = [ UILabel autoLayoutView ];
        [ _detailLabel setFont: [ UIFont systemFontOfSize:13.0f ] ];
    }
    
    return _detailLabel;
}

- (UIImageView *) imageView
{
    if( !_imageView )
    {
        _imageView = [ UIImageView autoLayoutView ];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _imageView;
}

- (void) prepareForReuse
{
    self.imageView.image = nil;
}

+ (NSString *) cellReuseIdentifier
{
    return kCellReuseIdentifier;
}
@end
