//
//  StreamVisionConstants.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 23/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "StreamVisionConstants.h"

NSString * const kIsLoggedIn = @"IsLoggedIn";
NSString * const kLoginSegue = @"LoginSegue";
NSString * const kDiscoverySegue =@"DiscoverySegue";
NSString * const kDeviceToMainSegue = @"DeviceToMainSegue";
NSString * const kLaunchToDeviceSegue = @"LaunchToDeviceSegue";
NSString * const kSVYes = @"YES";
NSString * const kSVNo = @"NO";
NSString * const kFoundDeviceNotification =@"FoundDeviceNotification";
NSString * const kLostDeviceNotification =@"LostDeviceNotification";
NSString * const kFindDeviceMessage =@"Finding Devices";
NSString * const kWaitMessage =@"Please Wait...";
NSString * const kSPVerifyMessage =@"Verifing Service Provider...";
NSString * const kPairingDeviceMessage =@"Pairing Device, Please Wait";
NSString * const kEPGSegue=@"EPGSegue";
NSString * const kDeviceAddress=@"DeviceAddress";
NSString * const kDeviceTargetURL=@"DeviceTargetURL";
NSString * const kNoNetworkMessage=@"No Network Available";
NSString * const kFailedChannelRequest=@"Failed to retrieve channels";
NSString * const kSettingsSegue = @"SettingsSegue";
NSString * const kFindDeviceFailureNotification =@"FindDeviceFailureNotification";
NSString * const kFindDeviceErrorMessage=@"Error in Finding Devices, Please Retry";
NSString * const kCurrentEPGDate =@"CurrentEPGDate";
NSString * const kValidRegNoFailureMessage =@"Please enter a valid registration number";
NSString * const kValidServiceProvider =@"Please enter a valid Service Provider id";
NSString * const kValidPassword =@"Please enter a valid password";
NSString * const kConnectionUnAvailable =@"Could not connect to network";
NSString * const kNoEPGFailureMessage =@"No EPG data available for any channel";
NSString * const kStreamVisionAppName=@"StreamVision";
NSString * const kRokuListingSegue=@"RokuListingSegue";
NSString * const kCurrentSelectedDeviceIdKey=@"CurrentSelectedDeviceIdKey";
NSString * const kDisconnectDeviceNotification=@"DisconectDeviceNotification";
NSString * const kDidConnectToNewDeviceNotification=@"DidConnectToNewDeviceNotification";
NSString * const kAdURLReceivedNotification =@"AdURLReceivedNotification";
NSString * const kWiFiChangedNotification=@"WiFiChangedNotification";
NSString * const kBeginPollingNotification =@"BeginPollingNotification";