//
//  ProgramGuideCurrentTimeIndicator.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 31/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramGuideTimeMarkerView.h"

@interface ProgramGuideCurrentTimeIndicator : ProgramGuideTimeMarkerView
@end
