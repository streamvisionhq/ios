//
//  AppCollectionViewCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AppCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation AppCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.imageView.layer.cornerRadius = 5.0;
}


@end
