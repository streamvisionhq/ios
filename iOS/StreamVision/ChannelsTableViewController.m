//
//  ChannelsTableViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ChannelsTableViewController.h"
#import "CoreDataManager.h"
#import "ChannelTableCell.h"
#import "UIImageView+WebCache.h"
#import "ChannelServiceManager.h"

static NSString *CellIdentifier = @"ChannelAppCell";

@interface ChannelsTableViewController ()

@property(nonatomic,strong) CoreDataManager *coreDataManager;
@property(nonatomic,strong) NSArray *channelsArray;
@property (nonatomic,strong) NSString *deviceTargetURL;

@end

@implementation ChannelsTableViewController

@synthesize channelsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    CoreDataManager *manager = [CoreDataManager sharedInstance];
    self.channelsArray = [NSArray arrayWithArray:[manager getAllChannels]];
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ChannelTableCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 90.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return self.channelsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    ChannelTableCell *cell = (ChannelTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*if (cell == nil)
        cell = [[ChannelTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];*/
    
    
    
    Channel *aChannel = [self.channelsArray objectAtIndex:indexPath.row];
   // NSLog(@"Channel Text : %@",aChannel.channelName);
    cell.appLabel.text = aChannel.channelName;
    //aChannel.channelName;
    [cell.appImageView sd_setImageWithURL:[NSURL URLWithString:aChannel.channelLogo]];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    [cell setSelected:NO];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [SVProgressHUD showWithStatus:kWaitMessage];
    NSLog(@"Launch Channel");
    Channel *aChannel = [self.channelsArray objectAtIndex:indexPath.row];
    
    [self launchChannel:aChannel];
}


-(void) launchChannel:(Channel *) channel{
    
    if ([self.delegate respondsToSelector:@selector(launchChannelWithId:)]) {
        [self.delegate launchChannelWithId:channel.channelId];
    }
    
    if ([self.delegate respondsToSelector:@selector(setStreaminNowLogo:)]) {
        [self.delegate setStreaminNowLogo:channel];
    }
     
    
}

-(void) deletaAllChannels{
    self.channelsArray = NULL;
    [self.tableView reloadData];
    CoreDataManager *manager = [CoreDataManager sharedInstance];
    [manager deleteAllChannelsData];
}

-(void) reloadChannels
{
    CoreDataManager *manager = [CoreDataManager sharedInstance];
    self.channelsArray = [NSArray arrayWithArray:[manager getAllChannels]];
    [self.tableView reloadData];
}
@end

