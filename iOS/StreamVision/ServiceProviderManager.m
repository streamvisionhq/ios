//
//  ServiceProviderManager.m
//  StreamVision
//
//  Created by irfan on 24/01/2016.
//  Copyright (c) 2016 TernionSoft. All rights reserved.
//

#import "ServiceProviderManager.h"
#import "AFHTTPRequestOperation.h"
#import "SVNetworkCheck.h"
#import "Utils.h"
#import "StreamVisionParserConstants.h"


//Staging endpoint
#define kServiceProviderEndpoint(spId,key) [NSString stringWithFormat:@"http://streamvision.coduplabs.com/api/web/serviceproviders/detail/%@?key=%@",spId,key]

//Production endpoint
//#define kServiceProviderEndpoint(spId,key) [NSString stringWithFormat:@"http://control.streamvisiontv.com/api/web/serviceproviders/detail/%@?key=%@",spId,key]

@implementation ServiceProviderManager

+(instancetype) sharedInstance
{
    static ServiceProviderManager *_spmanager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _spmanager = [ [ self alloc ] init ];
    });
    
    return _spmanager;
}

+(NSString *)getCMSEndPoint
{
   return [[ServiceProviderManager sharedInstance] serviceProvider].cmsUrl;
}

-(void) getServiceProvider:(NSString *)spId onSuccess:(void(^)(NSDictionary *json))completionBlock onFailure: (ErrorBlock)failureBlock{
    
    NSString *requestString = kServiceProviderEndpoint(spId, API_KEY);
    
    [self performRequest:requestString success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if( completionBlock )
        {
            NSError *error;
            
            NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            BOOL success = [[JSONResponseDict valueForKey:kSuccessKey] boolValue];
            
            if (success) {
              
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServiceProvider *serviceProvider = [ServiceProvider new];
                    NSDictionary *dic = [JSONResponseDict valueForKey:kServiceProviderKey];
                    [serviceProvider set:dic];
                    self.serviceProvider = serviceProvider;
                    
                    completionBlock(dic);
                   
                   });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *message = [JSONResponseDict objectForKey:@"message"];
                    if(message.length>0)
                       [[[UIAlertView  alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                    
                    if(failureBlock)
                    {
                        failureBlock([NSError new]);
                    }
                });
            }
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failureBlock)
        {
            failureBlock( error );
        }
        
    }];
}

- (void)performRequest: (NSString *) requestString
               success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    if ([self checkIfNetworkIsReachable]) {
        
        //Proceed
        
    }
    else
    {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kConnectionUnAvailable message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: @"quit",nil];
        
        [alert show];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
}

-(BOOL)checkIfNetworkIsReachable{
    
    return [SVNetworkCheck hasConnectivity];
}

@end
