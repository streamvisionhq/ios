//
//  StreamVisionConstants.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 23/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kIsLoggedIn;
extern NSString * const kLoginSegue;
extern NSString * const kDiscoverySegue;
extern NSString * const kDeviceToMainSegue;
extern NSString * const kEPGSegue;
extern NSString * const kSettingsSegue;
extern NSString * const kRokuListingSegue;
extern NSString * const kLaunchToDeviceSegue;
extern NSString * const kSVYes;
extern NSString * const kSVNo;
extern NSString * const kFoundDeviceNotification;
extern NSString * const kFindDeviceMessage;
extern NSString * const kWaitMessage;
extern NSString * const kPairingDeviceMessage;
extern NSString * const kDeviceTargetURL;
extern NSString * const kNoNetworkMessage;
extern NSString * const kFailedChannelRequest;
extern NSString * const kConnectionUnAvailable;
extern NSString * const kFindDeviceFailureNotification;
extern NSString * const kFindDeviceErrorMessage;
extern NSString * const kCurrentEPGDate;
extern NSString * const kValidRegNoFailureMessage;
extern NSString * const kLostDeviceNotification;
extern NSString * const kNoEPGFailureMessage;
extern NSString * const kStreamVisionAppName;
extern NSString * const kCurrentSelectedDeviceIdKey;
extern NSString * const kDisconnectDeviceNotification;
extern NSString * const kDidConnectToNewDeviceNotification;
extern NSString * const kAdURLReceivedNotification;
extern NSString * const kWiFiChangedNotification;
extern NSString * const kBeginPollingNotification;
extern NSString * const kValidServiceProvider;
extern NSString * const kValidPassword;
extern NSString * const kSPVerifyMessage;


/**
 *  Generic block that will be passed as parameter to remote services, to handle errors
 *
 *  @return void
 */
typedef void (^ErrorBlock)(NSError* error);

#define API_KEY @"K72kWVncihXmijxn2EAOlpabKu6-NGee"



