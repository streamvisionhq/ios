//
//  AsynchronousBlockOperation.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AsynchronousBlockOperation.h"

@interface AsynchronousBlockOperation()
@property (nonatomic, copy) AsynOperationBlock operationBlock;
@end

static NSString *const kIsExecutingKey  = @"isExecuting";
static NSString *const kIsFinishedKey   = @"isFinished";

@implementation AsynchronousBlockOperation

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isExecuting = NO;
        _isFinished = NO;
    }
    return self;
}

- (void)addOperationBlock:(AsynOperationBlock)block
{
    self.operationBlock = block;
}

- (void) markAsFinished
{
    [self finish];
}

- (BOOL)isConcurrent
{
    return YES;
}

- (void) start
{
    [self willChangeValueForKey: kIsExecutingKey ];
    _isExecuting = YES;
    [self didChangeValueForKey: kIsExecutingKey ];
    self.operationBlock( );
}

- (void) finish
{
    [self willChangeValueForKey: kIsExecutingKey ];
    [self willChangeValueForKey: kIsFinishedKey ];
    
    _isExecuting = NO;
    _isFinished = YES;
    
    [self didChangeValueForKey: kIsExecutingKey ];
    [self didChangeValueForKey: kIsFinishedKey ];
}

@end