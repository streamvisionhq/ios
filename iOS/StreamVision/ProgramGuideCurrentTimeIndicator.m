//
//  ProgramGuideCurrentTimeIndicator.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 31/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ProgramGuideCurrentTimeIndicator.h"

#import "UIView+AutolayoutExtensions.h"


@implementation ProgramGuideCurrentTimeIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        self.titleLabel.backgroundColor = [ UIColor redColor ];
        self.hairLine.backgroundColor = [ UIColor redColor ];
    }
    return self;
}

@end
