//
//  StreamVisionParserConstants.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "StreamVisionParserConstants.h"

/*
 "channelId": 13349,
 "channelNumber": 24,
 "description": "KTVK-DT Phoenix, Ariz. (IND)",
 "channelLogo": "http://streamvisionhq.com/assets/data/image/ABC_1426197998.jpg"
 },*/

 NSString * const kChannelNumberKey=@"channelNumber";
 NSString * const kChannelDescriptionKey =@"description";
 NSString * const kChannelIdKey = @"svChannelId"; //@"channelId";
 NSString * const kChannelLogoKey=@"channelLogo";

/*"startTimeUtc":"2015-05-29T07:00:00+0000","endTimeUtc":"2015-05-29T07:30:00+0000","Title":"The Flipside with Michael Loftus","Description":"Spinning political correctness on its head is comic Michael Loftus, as he shares a fresh point of view on the latest events in America with bold interpretations and engaging interviews featuring guests with opposing views on politics.","ShowType":"Series","Cast":"Michael Loftus","DisplayGenre":"Comedy","Rating":"TV-G","VideoURL":"https:\/\/streamvisionhq.a.cdnify.io\/rajaranis_1432818965.mp4"
*/

 NSString * const kChannelShowsParserKey=@"shows";
 NSString * const kStartTimeUtcKey=@"startTimeUtc";
 NSString * const kEndTimeUtcKey=@"endTimeUtc";
 NSString * const kTitleKey=@"Title";
 NSString * const kDescriptionKey=@"Description";
 NSString * const kShowTypeKey=@"ShowType";
 NSString * const kCastKey=@"Cast";
 NSString * const kDisplayGenreKey=@"DisplayGenre";
 NSString * const kRatingKey=@"Rating";
 NSString * const kVideoURLKey=@"VideoURL";

 NSString * const kDeviceIdKey=@"deviceIdentifier";
 NSString * const kisRegisteredIdKey=@"isRegistered";


NSString * const kSuccessKey=@"success";
NSString * const kDataKey=@"data";
NSString * const kMessageKey=@"message";
NSString * const kServiceProviderKey = @"ServiceProvider";

