//
//  ProgramGuideLayout.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ProgramGuideLayout.h"
#import "TimeRange.h"
#import "ProgramGuideDataSource.h"

/**
 *  Extern constants
 */
CGFloat const kDefaultMinuteWidth   = 8.0f;
CGFloat const kDefaultRowHeight     = 40.0f;

NSString* const kChannelCellKindIdentifier = @"ProgramGuideChannelCellIdentifier";
NSString* const kTimeRulerCellKindIdentifier = @"ProgramGuideRulerCellIdentifier";

/**
 *  Private constants
 */
static const CGFloat kRulerHeight               = 30.0f;
static const CGFloat kSecondsPerMinute          = 60.0f;
static const CGFloat kCellEdgeInset             = 1.0f;
static const CGFloat kHeaderWidth               = 345/2;
static const CGFloat kTimeIndicatorWidth        = 40.0f;
static const NSTimeInterval kDefaultRulerInterval = 30 * kSecondsPerMinute;

NSUInteger const kRulerzIndex              = 500;
NSUInteger const kTimeIndicatorZindex      = 1000;
NSUInteger const kSectionzIndex            = 2* kTimeIndicatorZindex ;


@interface ProgramGuideLayout( )
@property (nonatomic, assign) CGSize fullLayoutSize;
@property (nonatomic, strong) TimeRange *timeRange;
@property (nonatomic, strong) NSArray *layoutAttributes;
@property (nonatomic, assign) CGFloat minuteWidth;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, copy) NSString *timeMarkerReuseIdentifier;

@property (nonatomic, weak) NSObject<ProgramGuideDataSource>* dataSource;

@property (nonatomic, assign) BOOL shouldResetLayout;

@property (nonatomic, assign) NSTimeInterval timerRulerInterval;

- (void) resetLayout;
- (TimeRange*) timeRangeForItemAtIndexPath: (NSIndexPath *) indexPath;
@end


@implementation ProgramGuideLayout

-(instancetype) initWithDataSource: (NSObject<ProgramGuideDataSource>*) dataSource
                   timeMarkerClass: (Class) timeMarkerClass
                         rowHeight: (CGFloat) rowHeight
                       minuteWidth: (CGFloat) minuteWidth
                 timeRulerInterval: (NSTimeInterval) timeRulerInterval;
{
    self = [ super init ];
    
    if( self )
    {
        self.dataSource     = dataSource;
        self.rowHeight      = ( rowHeight > 0 ) ? rowHeight: kDefaultRowHeight;
        self.minuteWidth    = ( minuteWidth > 0) ? minuteWidth: kDefaultMinuteWidth;
        self.timerRulerInterval = ( timeRulerInterval > 0 )? timeRulerInterval: kDefaultRulerInterval;
        
        
        if( timeMarkerClass )
        {
            self.timeMarkerReuseIdentifier = [ [ [ timeMarkerClass alloc ] init ] performSelector: @selector( reuseIdentifier ) ];
            [ self registerClass: timeMarkerClass forDecorationViewOfKind: self.timeMarkerReuseIdentifier ];
        }
        
        self.shouldResetLayout = NO;
        
        [ self resetLayout ];
    }
    
    return self;
}

- (instancetype) init
{
    return [ self initWithDataSource: (NSObject<ProgramGuideDataSource>*) self.collectionView.dataSource
                     timeMarkerClass: nil
                           rowHeight: kDefaultRowHeight
                         minuteWidth: kDefaultMinuteWidth
                   timeRulerInterval: kDefaultRulerInterval ];
}

/**
 *  Perform the caculations needed to layout the ProgramGuide. This method will only be called after the collection view receives a reloadData message, not on scrolling
 */

- (void) resetLayout
{
    NSInteger numberOfChannels = [ self.dataSource numberOfSectionsInCollectionView: self.collectionView ];
    
    NSMutableArray *programsPerChannel = [ [ NSMutableArray alloc ] init ];
    
    TimeRange *programGuideRange = [ self.dataSource timeRangeOfCollectionView: self.collectionView layout: self ];
    NSDate *layoutInitDate = programGuideRange.date;
    NSDate *layoutEndDate = nil;
    /**
     *  Create an temporary list of number of programs per channel
     */
    for (int channelIndex = 0; channelIndex < numberOfChannels; channelIndex++)
    {
        
        NSInteger numberOfPrograms = [ self.dataSource collectionView: self.collectionView numberOfItemsInSection: channelIndex ];
        
        [ programsPerChannel addObject: @( numberOfPrograms ) ];
        
        if (!numberOfPrograms)
        {
            continue;
        }
    }
    
    
    NSMutableArray *localLayoutAttributesByChannel = [ [ NSMutableArray alloc ] init ];
    UICollectionViewLayoutAttributes *layoutAttributes;
    
    /**
     *  Calculate layout attributes for each program according to the TimeRanges provided by the collection datasource
     */
    for (int channelIndex = 0; channelIndex < numberOfChannels; channelIndex++)
    {
        NSMutableArray *localLayoutAttributesByProgram = [ [ NSMutableArray alloc ] init ];
        [ localLayoutAttributesByChannel addObject: localLayoutAttributesByProgram ];
        
        NSInteger numberOfPrograms = [ programsPerChannel[ channelIndex ] integerValue ];
        
        CGFloat top = kRulerHeight + ( channelIndex * self.rowHeight );
        
        NSDate *startTime = layoutInitDate;
        NSTimeInterval duration = 0;
        NSTimeInterval currentTime = 0;
        
        for (int programIndex = 0; programIndex < numberOfPrograms; programIndex++)
        {
            TimeRange *timeRange = [ self timeRangeForItemAtIndexPath: [ NSIndexPath indexPathForRow: programIndex inSection: channelIndex ] ];
            
            startTime = timeRange.date;
            
            duration = timeRange.duration;
            
            currentTime = [ startTime timeIntervalSinceDate: layoutInitDate ];
            
            layoutAttributes = [ UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath: [ NSIndexPath indexPathForRow: programIndex inSection: channelIndex ] ];
            
            CGRect cellRect = ( CGRect ) { ( currentTime / kSecondsPerMinute ) * self.minuteWidth, top, ( duration / kSecondsPerMinute ) * self.minuteWidth, self.rowHeight };
            
            
            UIEdgeInsets insets = UIEdgeInsetsMake( kCellEdgeInset, kCellEdgeInset, kCellEdgeInset, kCellEdgeInset );
            
            layoutAttributes.frame = UIEdgeInsetsInsetRect( cellRect, insets );
            
            [ localLayoutAttributesByProgram addObject: layoutAttributes ];
            
        }
        
        layoutEndDate = [ layoutInitDate dateByAddingTimeInterval: currentTime + duration ];
    }
    
    self.layoutAttributes = localLayoutAttributesByChannel;
    
    if ( programGuideRange.date && ( programGuideRange.duration > 0 ) )
    {
        layoutEndDate = [ programGuideRange.date dateByAddingTimeInterval: programGuideRange.duration ];
    }
    
    self.timeRange = [ [ TimeRange alloc ] initWithDate: layoutInitDate duration:[ layoutEndDate timeIntervalSinceDate: layoutInitDate ] ];
    
    self.fullLayoutSize = ( CGSize ) { ( self.timeRange.duration / kSecondsPerMinute ) * self.minuteWidth, kRulerHeight + ( numberOfChannels * self.rowHeight ) };
}

/**
 *  Invalidate the layout only when the user is not scrolling the collectionview.
 */
- (void) invalidateLayout
{
    [ super invalidateLayout ];
    if( !self.shouldResetLayout )
    {
      [ self resetLayout ];
    }
}

/**
 *  Mark the layout as to be refreshed only when the collection view is sent a reloadData message
 *
 */
- (void) invalidateLayoutWithContext:(UICollectionViewLayoutInvalidationContext *)context
{
    [ super invalidateLayoutWithContext: context ];
    self.shouldResetLayout = !( context.invalidateDataSourceCounts || context.invalidateEverything );
}


- (UICollectionViewLayoutAttributes *) layoutAttributesForItemAtIndexPath: (NSIndexPath *) indexPath
{
    return self.layoutAttributes[ indexPath.section ][ indexPath.row ];
}

- (NSArray *) layoutAttributesForElementsInRect: (CGRect) rect
{
    NSMutableArray *array = [ [ NSMutableArray alloc ] init ];
    
    NSMutableIndexSet *sections = [ NSMutableIndexSet indexSet ];
    
    NSMutableIndexSet *rulers = [ NSMutableIndexSet indexSet ];
    
    NSMutableIndexSet *decorations = [ NSMutableIndexSet indexSet ];
    
    for( NSArray *programLayoutAttributes in self.layoutAttributes )
    {
        for ( UICollectionViewLayoutAttributes *layoutAttributes in programLayoutAttributes )
        {
            if( ( CGRectIntersectsRect( rect, layoutAttributes.frame ) ) && ( layoutAttributes.representedElementCategory == UICollectionElementCategoryCell ) )
            {
                [ array addObject: layoutAttributes ];
                [ sections addIndex: layoutAttributes.indexPath.section ];
                [ decorations addIndex: layoutAttributes.indexPath.section ];
            }
            
            
            if ( [ layoutAttributes.representedElementKind isEqualToString: kChannelCellKindIdentifier ] )
            {
                [ array removeObject: layoutAttributes ];
            }
            
        }
    }
    
    /**
     *  Calculate the attributs for the channel cells (section headers)
     *
     */
    
    [ sections enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *indexPath = [ NSIndexPath indexPathForItem: 0 inSection: idx ];
        
        UICollectionViewLayoutAttributes *layoutAttributes = [ self layoutAttributesForSupplementaryViewOfKind: kChannelCellKindIdentifier
                                                                                                   atIndexPath: indexPath];
        
        if( layoutAttributes )
        {
            [ array addObject: layoutAttributes ];
        }
        
        
    }];
    
    /**
     *  Get the indexes of the cells in the time ruler that should be displayed for this frame
     */
   [ rulers addIndexes: [ self indexesForRuler: rect  ] ];
    
    /**
     *  And calculate their layout attributes
     *
     */
    
    [ rulers enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *indexPath = [ NSIndexPath indexPathForItem: idx inSection: 0 ];
        UICollectionViewLayoutAttributes *timeRulerlayoutAttributes = [ self layoutAttributesForSupplementaryViewOfKind: kTimeRulerCellKindIdentifier
                                                                                                            atIndexPath: indexPath];
        
        if( timeRulerlayoutAttributes )
        {
            [ array addObject: timeRulerlayoutAttributes ];
        }
    }];
    
    /**
     *  The only decoration view, at the moment, is the "Now" indicator
     *
     */
    
    [ decorations enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *indexPath = [ NSIndexPath indexPathForItem: 0 inSection: idx ];
        
        
        UICollectionViewLayoutAttributes *timeMarkerAttributes = [ self layoutAttributesForDecorationViewOfKind: self.timeMarkerReuseIdentifier
                                                                                                    atIndexPath: indexPath];
        
        if( timeMarkerAttributes )
        {
            [ array addObject: timeMarkerAttributes ];
        }
        
    }];
    
    
    return array;
}

/**
 *  Calculate the index paths of the celles in the time ruler that should be displayed for a given rect
 *
 */
- (NSMutableIndexSet *) indexesForRuler: (CGRect) rect
{
    CGFloat widthOfThirtyMinutes = 30*self.minuteWidth;
    NSMutableIndexSet *returnVal = [ NSMutableIndexSet indexSet ];

    CGFloat frameX = CGRectGetMinX( rect );
    CGFloat frameY = CGRectGetMaxX( rect );
    if(frameX < 0 || frameY < 0)
        return returnVal;
    
    NSUInteger leftMarkerIndex = floor( frameX/ widthOfThirtyMinutes );
    NSUInteger rightMarkerIndex = ceil(frameY/ widthOfThirtyMinutes );
    
    
    for( NSUInteger i = leftMarkerIndex; i<= rightMarkerIndex; i++  )
    {
        [ returnVal addIndex: i ];
    }
    
    return returnVal;
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForSupplementaryViewOfKind: (NSString *) kind atIndexPath: (NSIndexPath *) indexPath
{
    UICollectionView * const cv = self.collectionView;
    CGPoint const contentOffset = cv.contentOffset;
    
    UICollectionViewLayoutAttributes *attributes = [ UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind: kind
                                                                                                                   withIndexPath: indexPath ];
    
    /**
     *  Channel cells (section headers)
     */
    if ( [ kind isEqualToString: kChannelCellKindIdentifier ] )
    {
        CGRect frame = attributes.frame;
        frame.origin.x = contentOffset.x-45;
        frame.origin.y = kRulerHeight + ( self.rowHeight * indexPath.section );
        CGFloat oneFourthOfScreenWidth = [UIScreen mainScreen].bounds.size.width / 4;
        frame.size = ( CGSize ) { MIN(oneFourthOfScreenWidth, kHeaderWidth),  self.rowHeight };
        //frame.size = ( CGSize ) { 0,  0 };
        attributes.zIndex = kSectionzIndex + indexPath.section;
        attributes.frame = frame;
    }
    
    
    /**
     *  Time ruler
     */
    if( [ kind isEqualToString: kTimeRulerCellKindIdentifier ] )
    {
        CGFloat totalWidth = self.collectionViewContentSize.width;
        CGFloat totalWidthInSeconds = self.timeRange.duration;
        CGFloat widthOfTimeCell = self.timerRulerInterval*totalWidth / totalWidthInSeconds;
        
        CGFloat newX = (self.timerRulerInterval/kSecondsPerMinute)* self.minuteWidth * indexPath.item;
        
        NSCalendar *calendar = [ NSCalendar currentCalendar ];
        
        NSDateComponents *components ;
        if (self.timeRange.date) {
            components = [ calendar components: ( NSHourCalendarUnit | NSMinuteCalendarUnit ) fromDate: self.timeRange.date ];
        }else{
       components = [ calendar components: ( NSHourCalendarUnit | NSMinuteCalendarUnit ) fromDate: [NSDate new] ];
        }
        
        
        NSInteger minute = [ components minute ];
        
        CGFloat markerInMinutes = self.timerRulerInterval/kSecondsPerMinute;
        
        NSUInteger minutesToNextMark = ( minute <= markerInMinutes ) ? ( markerInMinutes - minute): ( 2* markerInMinutes - minute );
        
        CGRect frame = attributes.frame;
        frame.size = ( CGSize ) { widthOfTimeCell, kRulerHeight };
        frame.origin.y = contentOffset.y;
        frame.origin.x = newX + ( minutesToNextMark * self.minuteWidth );
        
        attributes.frame = frame;
        /**
         * Make sure that the zIndex for all channel cells is different (in particular, make sure that the two leftmost cells do not share the same zIndex
         */
        NSInteger zIndex = kRulerzIndex+(indexPath.item+1);
        attributes.zIndex = zIndex;
    }
    
    return attributes;
    
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionView * const cv = self.collectionView;
    CGPoint const contentOffset = cv.contentOffset;
    
    if( [ decorationViewKind isEqualToString: self.timeMarkerReuseIdentifier ] )
    {
        UICollectionViewLayoutAttributes *attributes = [ UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind: self.timeMarkerReuseIdentifier
                                                                                                                    withIndexPath: indexPath ];
        
        NSDate *now = [ NSDate date ];
        NSTimeInterval timeIntervalToNow = [ now timeIntervalSinceDate: self.timeRange.date ];
        CGFloat indicatorXPos = self.minuteWidth * timeIntervalToNow/ kSecondsPerMinute;
        CGRect frame = attributes.frame;
        frame.origin.x = indicatorXPos - kTimeIndicatorWidth/2;
        frame.origin.y = contentOffset.y;
        frame.size = ( CGSize ) { kTimeIndicatorWidth, cv.frame.size.height };
        attributes.frame = frame;
        attributes.zIndex = kTimeIndicatorZindex;
        
        return attributes;
    }
    
    return nil;
}

/**
 *  Invalidate the layout on scroll events, to force the relayout of section headers and time ruler. The layout won't be invalidated
 */
- (BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (CGSize)collectionViewContentSize
{
    return self.fullLayoutSize;
}


- (TimeRange*) timeRangeForItemAtIndexPath: (NSIndexPath *) indexPath
{
    return [ self.dataSource collectionView: self.collectionView layout: self timeRangeOfItemAtIndexPath: indexPath ];
}

@end