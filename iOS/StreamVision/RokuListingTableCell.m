//
//  SettingsTableViewCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 09/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "RokuListingTableCell.h"

@implementation RokuListingTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectButtonTapped:(id)sender {
    
    
    if ([self.delegate respondsToSelector:@selector(selectedDeviceAtIndex:)]) {
        [self.delegate selectedDeviceAtIndex:[sender tag]];
    }
    
    
}
@end
