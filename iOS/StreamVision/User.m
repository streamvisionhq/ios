//
//  User.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 11/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic identifier;
@dynamic isActive;
@dynamic userName;
@dynamic registrationNumber;
@dynamic serviceProvider;
@dynamic password;

@end
