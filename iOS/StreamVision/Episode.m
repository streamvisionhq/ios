//
//  Episode.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Episode.h"
#import "Show.h"


@implementation Episode

@dynamic title;
@dynamic language;
@dynamic shows;

@end
