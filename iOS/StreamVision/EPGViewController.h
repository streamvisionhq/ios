//
//  EPGViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
    EPGViewController is controller which displays all EPG data with there controlls.
                        ------
 */

#import <UIKit/UIKit.h>
#import <ConnectSDK/ConnectSDK.h>
#import "MainViewController.h"

@protocol EPGLaunchChannelDelegate;

@interface EPGViewController : UIViewController

@property (nonatomic,strong) NSString *todayString;
@property (nonatomic,strong) NSString *temperatureString;
@property (nonatomic,strong) ConnectableDevice *device;
@property (nonatomic,strong) AppInfo *svApp;
@property BOOL svAppRunning;
@property (nonatomic,strong)MainViewController* mainviewController;
@property (nonatomic,strong) NSString *deviceTargetURL;
@property BOOL isServerError;

@property (nonatomic,strong) id<EPGLaunchChannelDelegate> delegate;
- (void) setChannelArray:(NSArray *)channelArray;
-(NSArray *) oldChannels;
- (void) initFetchDataQueue;


@end

@protocol EPGLaunchChannelDelegate <NSObject>

@optional
-(void) launchedSVChannel;

@end
