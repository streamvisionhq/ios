//
//  Device.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Device : NSManagedObject

@property (nonatomic, retain) NSString * deviceIdentifier;
@property (nonatomic, retain) NSString * deviceLocation;
@property (nonatomic, retain) NSString * deviceServer;
@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSNumber * isOnline;
@property (nonatomic, retain) NSString * rokuDeviceId;
@property (nonatomic, retain) NSString * serviceType;
@property (nonatomic, retain) NSNumber * isRegistered;
@property (nonatomic, retain) NSString * accountId;

@end
