//
//  UIView+AutolayoutExtensions.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
#import "UIView+AutolayoutExtensions.h"

@implementation UIView ( AutolayoutExtensions )

+(instancetype) autoLayoutView
{
    UIView *viewToReturn = [ self new ];
    viewToReturn.translatesAutoresizingMaskIntoConstraints = NO;
    return viewToReturn;
}

- (NSArray *) centerInView: (UIView*) superview
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterX relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterX multiplier: 1.0 constant: 0 ] ];
    [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeCenterY relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeCenterY multiplier: 1.0 constant: 0 ] ];
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}

- (NSLayoutConstraint *) centerInContainerOnAxis: (NSLayoutAttribute) axis
{
    NSParameterAssert(axis == NSLayoutAttributeCenterX || axis == NSLayoutAttributeCenterY);
    
    UIView *superview = self.superview;
    NSParameterAssert(superview);
    NSLayoutConstraint *constraint = [ NSLayoutConstraint constraintWithItem: self
                                                                   attribute: axis
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: superview
                                                                   attribute: axis
                                                                  multiplier: 1.0
                                                                    constant: 0.0 ];
    [ superview addConstraint: constraint ];
    return constraint;
}

- (NSArray *) pinToSuperviewEdges: (ViewEdges) edges inset: (CGFloat) inset usingLayoutGuidesFrom: (UIViewController *) viewController
{
    UIView *superview = self.superview;
    NSAssert(superview,@"Can't pin to a superview if no superview exists");
    
    id topItem      = nil;
    id bottomItem   = nil;
    
    if( viewController && [ viewController respondsToSelector: @selector( topLayoutGuide ) ] )
    {
        topItem = viewController.topLayoutGuide;
        bottomItem = viewController.bottomLayoutGuide;
    }
    
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    if( edges & ViewTopEdge )
    {
        id item = topItem ? topItem : superview;
        NSLayoutAttribute attribute = topItem ? NSLayoutAttributeBottom : NSLayoutAttributeTop;
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeTop relatedBy: NSLayoutRelationEqual toItem: item attribute: attribute multiplier: 1.0 constant: inset ] ];
    }
    if( edges & ViewLeftEdge )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeLeft relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeLeft multiplier: 1.0 constant: inset ] ];
    }
    if( edges & ViewRightEdge )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeRight relatedBy: NSLayoutRelationEqual toItem: superview attribute: NSLayoutAttributeRight multiplier: 1.0 constant: -inset ] ];
    }
    if( edges & ViewBottomEdge )
    {
        id item = bottomItem ? bottomItem : superview;
        NSLayoutAttribute attribute = bottomItem ? NSLayoutAttributeTop : NSLayoutAttributeBottom;
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeBottom relatedBy: NSLayoutRelationEqual toItem: item attribute: attribute multiplier: 1.0 constant: -inset ] ];
    }
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}


- (NSArray*) pinToSuperviewEdges: (ViewEdges) edges inset: (CGFloat) inset
{
    return [ self pinToSuperviewEdges: edges inset: inset usingLayoutGuidesFrom: nil ];
}

- (NSArray*) pinToSuperviewEdgesWithInset: (UIEdgeInsets) insets
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    [ constraints addObjectsFromArray: [ self pinToSuperviewEdges: ViewTopEdge inset: insets.top ] ];
    [ constraints addObjectsFromArray: [ self pinToSuperviewEdges: ViewLeftEdge inset: insets.left ] ];
    [ constraints addObjectsFromArray: [ self pinToSuperviewEdges: ViewBottomEdge inset: insets.bottom ] ];
    [ constraints addObjectsFromArray: [ self pinToSuperviewEdges: ViewRightEdge inset: insets.right ] ];
    
    return [ constraints copy ];
}

- (NSArray *) pinEdges: (ViewEdges) edges toSameEdgesOfView: (UIView *) peerView
{
    return [ self pinEdges: edges toSameEdgesOfView: peerView inset: 0 ];
}

- (NSArray *) pinEdges: (ViewEdges) edges toSameEdgesOfView: (UIView *) peerView inset: (CGFloat) inset
{
    UIView *superview = [ self commonSuperviewWithView: peerView ];
    NSAssert(superview,@"Can't create constraints without a common superview");
    
    NSMutableArray *constraints = [ NSMutableArray arrayWithCapacity: 4 ];
    
    if( edges & ViewTopEdge )
    {
        [ constraints addObject: [ self pinEdge: NSLayoutAttributeTop toEdge: NSLayoutAttributeTop ofItem: peerView inset: inset ] ];
    }
    if( edges & ViewLeftEdge )
    {
        [ constraints addObject: [ self pinEdge: NSLayoutAttributeLeft toEdge: NSLayoutAttributeLeft ofItem: peerView inset: inset ] ];
    }
    if( edges & ViewRightEdge )
    {
        [ constraints addObject: [ self pinEdge: NSLayoutAttributeRight toEdge: NSLayoutAttributeRight ofItem: peerView inset: -inset ] ];
    }
    if( edges & ViewBottomEdge )
    {
        [ constraints addObject: [ self pinEdge: NSLayoutAttributeBottom toEdge: NSLayoutAttributeBottom ofItem: peerView inset: -inset ] ];
    }
    
    [ superview addConstraints: constraints ];
    return [ constraints copy ];
}

- (NSLayoutConstraint *) pinEdge: (NSLayoutAttribute) edge toEdge: (NSLayoutAttribute) toEdge ofItem: (id) peerItem inset: (CGFloat) inset
{
    UIView *superview;
    if( [ peerItem isKindOfClass: [ UIView class ] ] )
    {
        superview = [ self commonSuperviewWithView: peerItem ];
        NSAssert(superview,@"Can't create constraints without a common superview");
    }
    else
    {
        superview = self.superview;
    }
    
    NSAssert (edge >= NSLayoutAttributeLeft && edge <= NSLayoutAttributeBottom,@"Edge parameter is not an edge");
    NSAssert (toEdge >= NSLayoutAttributeLeft && toEdge <= NSLayoutAttributeBottom,@"Edge parameter is not an edge");
    
    NSLayoutConstraint *constraint = [ NSLayoutConstraint constraintWithItem: self
                                                                   attribute: edge
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: peerItem
                                                                   attribute: toEdge
                                                                  multiplier: 1.0
                                                                    constant: inset ];
    [ superview addConstraint: constraint ];
    return constraint;
}

- (UIView*) commonSuperviewWithView: (UIView*) peerView
{
    UIView *commonSuperview = nil;
    UIView *startView = self;
    do
    {
        if( [ peerView isDescendantOfView: startView ] )
        {
            commonSuperview = startView;
        }
        startView = startView.superview;
    } while ( startView && !commonSuperview );
    
    return commonSuperview;
}

- (NSLayoutConstraint *) constrainToWidth: (CGFloat) width
{
    NSLayoutConstraint *constraint = [ NSLayoutConstraint constraintWithItem: self
                                                                   attribute: NSLayoutAttributeWidth
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: nil
                                                                   attribute: 0
                                                                  multiplier: 0
                                                                    constant: width ];
    return constraint;
}

- (NSLayoutConstraint *) constrainToHeight: (CGFloat) height
{
    NSLayoutConstraint *constraint = [ NSLayoutConstraint constraintWithItem: self
                                                                   attribute: NSLayoutAttributeHeight
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: nil
                                                                   attribute: 0
                                                                  multiplier: 0
                                                                    constant: height ];
    return constraint;
}

-(NSLayoutConstraint *) pinCenterXToView: (UIView *) peerView
{
    NSLayoutConstraint *horizontalCenterConstraint = [ NSLayoutConstraint constraintWithItem: self
                                                                                   attribute: NSLayoutAttributeCenterX
                                                                                   relatedBy: NSLayoutRelationEqual
                                                                                      toItem: peerView
                                                                                   attribute: NSLayoutAttributeCenterX
                                                                                  multiplier: 1.0f
                                                                                    constant: 0.0f ];
    return horizontalCenterConstraint;
}

-(NSLayoutConstraint *) pinCenterYToView: (UIView *) peerView
{
    NSLayoutConstraint *verticalCenterConstraint = [ NSLayoutConstraint constraintWithItem: self
                                                                                 attribute: NSLayoutAttributeCenterY
                                                                                 relatedBy: NSLayoutRelationEqual
                                                                                    toItem: peerView
                                                                                 attribute: NSLayoutAttributeCenterY
                                                                                multiplier: 1.0f
                                                                                  constant: 0.0f ];
    return verticalCenterConstraint;
}

- (NSArray *) constrainToSize: (CGSize) size
{
    NSMutableArray *constraints = [ NSMutableArray new ];
    
    if( size.width )
    {
        [ constraints addObject: [ self constrainToWidth: size.width ] ];
    }
    if( size.height )
    {
        [ constraints addObject: [ self constrainToHeight: size.height ] ];
    }
    
    return [ constraints copy ];
}

- (NSArray *) constrainToMinimumSize: (CGSize) minimum maximumSize: (CGSize) maximum
{
    NSAssert(minimum.width <= maximum.width, @"maximum width should be strictly wider than or equal to minimum width");
    NSAssert(minimum.height <= maximum.height, @"maximum height should be strictly higher than or equal to minimum height");
    NSArray *minimumConstraints = [ self constrainToMinimumSize: minimum ];
    NSArray *maximumConstraints = [ self constrainToMaximumSize: maximum ];
    return [ minimumConstraints arrayByAddingObjectsFromArray: maximumConstraints ];
}

- (NSArray *) constrainToMinimumSize: (CGSize) minimum
{
    NSMutableArray *constraints = [ NSMutableArray array ];
    if( minimum.width )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeWidth relatedBy: NSLayoutRelationGreaterThanOrEqual toItem: nil attribute: 0 multiplier: 0 constant: minimum.width ] ];
    }
    if( minimum.height )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeHeight relatedBy: NSLayoutRelationGreaterThanOrEqual toItem: nil attribute: 0 multiplier: 0 constant: minimum.height ] ];
    }
    return [ constraints copy ];
}

- (NSArray *) constrainToMaximumSize: (CGSize) maximum
{
    NSMutableArray *constraints = [ NSMutableArray array ];
    if( maximum.width )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeWidth relatedBy: NSLayoutRelationLessThanOrEqual toItem: nil attribute: 0 multiplier: 0 constant: maximum.width ] ];
    }
    if ( maximum.height )
    {
        [ constraints addObject: [ NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeHeight relatedBy: NSLayoutRelationLessThanOrEqual toItem: nil attribute: 0 multiplier: 0 constant: maximum.height ] ];
    }
    return [ constraints copy ];
}

@end
