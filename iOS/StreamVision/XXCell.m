//
//  XXCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "XXCell.h"
#import "Utils.h"


static NSString * const kProgramCell = @"programCell";
static NSString * const kChannelCell = @"channelCell";

@interface XXCell( )
- (void) setupTitleLabel;
@end

@implementation XXCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [ [ UIScreen mainScreen ] scale ];
        self.backgroundColor = [ UIColor clearColor ];
        [ self setupTitleLabel ];
        
    }
    return self;
}

+ (NSString *) reuseIdentifier
{
    return kProgramCell;
}


- (void) setupTitleLabel
{
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.tag = kLabelTag;
    
    self.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.titleLabel setNumberOfLines:2];
    self.titleLabel.textColor = [ UIColor whiteColor ];
}

- (void) prepareForReuse
{
    self.backgroundColor = [ UIColor whiteColor ];
    [ super prepareForReuse ];
}

-(void) setSelected:(BOOL)selected
{
    if(selected)
        self.contentView.layer.borderColor = [[UIColor redColor] CGColor];
    else
        self.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    //self.contentView.layer.borderWidth = 1.0;
}

@end

@interface XXChannelCell( )
- (void) setupImageView;
@end

@implementation XXChannelCell

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [ self setupImageView ];
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [ [ UIScreen mainScreen ] scale ];
        //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tableBg"]];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

- (void) setupImageView
{
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;//UIViewContentModeScaleAspectFit;
    self.imageView.tag = kImageTag;
}

+ (NSString *) reuseIdentifier
{
    return kChannelCell;
}

@end