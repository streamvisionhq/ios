//
//  EPGViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "EPGViewController.h"
#import "EPGSegmentCollectionController.h"
#import "Utils.h"
#import "AsynchronousBlockOperation.h"
#import "EPGParser.h"
#import "ProgramGuideLayout.h"
#import "ProgramGuideCurrentTimeIndicator.h"
#import "ProgramGuideRulerView.h"
#import "XXCell.h"
#import "TimeRange.h"
#import "CoreDataManager.h"
#import "UIImageView+WebCache.h"
#import "EPGServiceManager.h"
#import "UserSettingViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <EventKit/EventKit.h>

#define TIMETONOTIFY 30

typedef void (^EPGBlock)(void);

static NSString * const kRuleCell           = @"rulerCell";
static NSString * const kStartTime          = @"startTime";
static NSString * const kEndTime            = @"endTime";
static const CGFloat kSecondsPerMinute      = 60;
static const NSUInteger kGridTimeSpan       = 24 * kSecondsPerMinute * kSecondsPerMinute;
static const CGFloat kWidthOfMinute         = 6;
static const CGFloat kOneDayContentWidth    = kWidthOfMinute * 60 * 24;
static const NSUInteger kNumberOfDays       = 3;
static const NSUInteger kInitTimeOffset     = 90;
static const CGFloat kTopBarheight          = 64.0;
static const CGFloat kWidthOfPreviewSection = 250;


@interface EPGViewController () <ProgramGuideDateSelectionDelegate,UICollectionViewDataSource,UICollectionViewDelegate,ProgramGuideDataSource,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSString *locationNameStr;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic,strong) EPGSegmentCollectionController *dateSelectionUI;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UICollectionView *programGuide;
@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, strong) NSMutableArray *programsByChannel;
@property (nonatomic, strong) NSOperationQueue *queue;

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, assign) NSUInteger loadedDaysCount;

@property (nonatomic, assign) NSUInteger currentDayIndex;

@property (nonatomic,retain) EPGServiceManager *serviceManager;
@property (weak, nonatomic) IBOutlet UITableView *ChannelTableView;


- (AsynchronousBlockOperation *) fetchDataForDayWithIndex: (NSUInteger) dayIndex completionBlock:(EPGBlock) completionBlock;


- (void) queueHasBeenExhausted;

- (CGPoint) gridInitialOffset;

- (void) sectionHeaderTapped: (UITapGestureRecognizer *) gesture;


//Preview Section
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UILabel *programTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *programDescriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *programTimeLabel;
@property (nonatomic,strong) NSDateFormatter *epgTimeFormatter;
@property (weak, nonatomic) IBOutlet UILabel *timelabel;
@property (weak, nonatomic) IBOutlet UILabel *templabel;
@property (nonatomic, strong) NSIndexPath *selectedItemIndexPath;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property BOOL isPaused;
@property (nonatomic,strong) Show *selectedShow;
@property (nonatomic,strong) Channel *selectedSectionChannel;
@property (nonatomic,strong) NSTimer *pollTimer;
@property NSUInteger pollTimerCount;
@property (nonatomic,strong) NSDate *currentDate;
@property (nonatomic,strong) UIColor* tableBackgroundColor;

@end

@implementation EPGViewController

@synthesize svAppRunning;


@synthesize todayString,temperatureString,device,svApp,mainviewController,deviceTargetURL,isServerError;

- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [ super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if( self )
    {
       
    }
    
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.channels.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"channelCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    Channel *channel = self.channels[ indexPath.row ];
    if(!channel)
        return cell;
    
    cell.backgroundColor = [UIColor colorWithRed:1.0 green:66.0 blue:113.0 alpha:0];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:channel.channelLogo]];
    return cell;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentDate = [NSDate date];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.deviceTargetURL = [defaults valueForKey:kDeviceTargetURL];
    [_programGuide setHidden:true];
    self.isServerError = NO;
   
    self.timelabel.text= self.todayString;
    self.templabel.text = self.temperatureString;
    self.tableBackgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tableBg"]];
    [self.previewImageView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(launchSelectedChannel:)];
    [doubleTap setNumberOfTapsRequired:2];
    [self.previewImageView addGestureRecognizer:doubleTap];

    self.navigationController.navigationBarHidden = YES;
    _programsByChannel = [ [ NSMutableArray alloc ] init ];
    
    self.epgTimeFormatter = [[NSDateFormatter alloc] init];
    [self.epgTimeFormatter setDateFormat:@"hh:mm a"];
    
    _queue = [ [ NSOperationQueue alloc ] init ];
    _queue.maxConcurrentOperationCount = 2;
    
    
    self.startDate = [ Utils startOfTheDay ];
    self.loadedDaysCount = 0;
    self.currentDayIndex = 0;
    
    
   // [ self addSpinner ];
    
    
   /* if([self shouldGetEPGData]){
   
        [ self initFetchDataQueue ];
   
    }else{
        
        [self loadExistingEPGData];
    }
    */
    self.isPaused = YES;
    self.serviceManager = [EPGServiceManager sharedInstance];
    [SVProgressHUD showWithStatus:kWaitMessage];

    [ self initFetchDataQueue ];
    [ self startPollTimer];
    //self.channels = [NSArray arrayWithArray:[coreDataManager getAllChannels]];
    //[self.ChannelTableView reloadData];
}
- (void)dealloc
{
    if(_queue ) {
        [_queue setSuspended:YES];
        [_queue cancelAllOperations];
        // you need to decide if you need to handle running operations
        // reasonably, but don't wait here because that may block the
        // main thread
    }
    // other dealloc stuff
}
- (void) viewWillDisappear:(BOOL)animated
{
    [ super viewWillDisappear: animated ];
    [ _queue cancelAllOperations ];
}

- (void) setChannelArray:(NSArray *)channelArray
{
    self.channels = channelArray;
}

-(NSArray *) oldChannels
{
    return self.channels;
}

-(void) loadExistingEPGData {
    
    CoreDataManager *coreDataManager = [CoreDataManager sharedInstance];
    self.channels = [NSArray arrayWithArray:[coreDataManager getChannelsForEPG]];
   [ self.view addSubview: self.dateSelectionUI.view ];
   [ self.view addSubview: self.programGuide ];
   [ self.programGuide setContentOffset: [ self gridInitialOffset ] animated: YES ];
    self.dateSelectionUI.loadingMode = NO;
    
    
    [ self.programGuide reloadData ];
    //[ self.ChannelTableView reloadData];

}

-(BOOL) shouldGetEPGData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDate *epgStoredDate = [defaults valueForKey:kCurrentEPGDate];
    
    if (epgStoredDate == nil) {
        return YES;
    }else{
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *nowDate = [NSDate date];
      
        NSDateComponents *TODAYCOMPONENTS = [calendar components:(  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:nowDate ];
   

        NSDateComponents *CURRENTEPGCOMPONENTS = [calendar components:(  NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:epgStoredDate ];
        
        if (TODAYCOMPONENTS.day == CURRENTEPGCOMPONENTS.day && TODAYCOMPONENTS.month == CURRENTEPGCOMPONENTS.month && TODAYCOMPONENTS.year == CURRENTEPGCOMPONENTS.year) {
            
            return NO;
        }else{
            
            return YES;
        }

        
    }
    
    
    return YES;
}

- (void) initFetchDataQueue
{
    if(!self.channels.count) {
        CoreDataManager *coreDataManager = [CoreDataManager sharedInstance];
        [coreDataManager deleteAllShows];
    }else{
        [SVProgressHUD dismiss];
    }
    
    AsynchronousBlockOperation *previousDay = nil;
    AsynchronousBlockOperation *currentDay = nil;

    for( int i = 0; i<kNumberOfDays; i++ )
    {
        if( i == 0 )
        {
            currentDay = [ self fetchDataForDayWithIndex: i completionBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:self.startDate forKey:kCurrentEPGDate];
                    [defaults synchronize];
                    
                    if(!self.channels.count)
                    {
                        CoreDataManager *coreDataManager = [CoreDataManager sharedInstance];
                        self.channels = [NSArray arrayWithArray:[coreDataManager getChannelsForEPG]];
                    }
                    
                    if(self.channels.count > 0){
                    
                    [ self.view addSubview: self.dateSelectionUI.view ];
                    [ self.view addSubview: self.programGuide ];
                    [self.programGuide setUserInteractionEnabled:false];

                    [ self.programGuide setContentOffset: [ self gridInitialOffset ] animated: YES ];
                    self.dateSelectionUI.loadingMode = YES;
                        
                    }else{
                        
                        [SVProgressHUD dismiss];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kNoEPGFailureMessage
                                                                        message:nil
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                    }
                });
            }];
            
            previousDay = currentDay;
        }
        else
        {
            currentDay = [ self fetchDataForDayWithIndex: i completionBlock:^{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    //[ self.programGuide reloadData ];
                    //[ self.ChannelTableView reloadData];
                });
            }];
            
            [ currentDay addDependency: previousDay ];
            if( i == kNumberOfDays -2 )
            {
                previousDay = currentDay;
            }
        }
        
        [ _queue addOperation:currentDay];

 }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_queue waitUntilAllOperationsAreFinished];
        dispatch_async(dispatch_get_main_queue(), ^{
            [ self queueHasBeenExhausted ];
            [ self.programGuide reloadData ];
            [ self.ChannelTableView reloadData];
            [ self.dateSelectionUI.delegate didSelectSegment:0];
            [self.programGuide setUserInteractionEnabled:true];
            });
        
    });
}


- (AsynchronousBlockOperation *) fetchDataForDayWithIndex: (NSUInteger) dayIndex completionBlock:(EPGBlock) completionBlock
{
    AsynchronousBlockOperation *operation = [ [ AsynchronousBlockOperation alloc ] init ];
    __weak AsynchronousBlockOperation *weakBlockOp = operation;
  //  __weak EPGViewController *weakSelf = self;
    
    NSDate *startOfToday = self.startDate;
    NSDate *startDate = [ NSDate dateWithTimeInterval: dayIndex* kGridTimeSpan sinceDate: startOfToday ];
    NSDate *endDate = [ NSDate dateWithTimeInterval: (dayIndex+2)* kGridTimeSpan sinceDate: startOfToday ];
   
        [ operation addOperationBlock:^{
            if(self.channels.count)
            {
                NSLog(@"Success !!!!!!");
                self.loadedDaysCount++;
                if( completionBlock )
                {
                    completionBlock( );
                }
                [ weakBlockOp markAsFinished ];
            }else
                [self.serviceManager getEPGForStartDate:startDate andEndDate:endDate onSuccess:^(void) {
            
                NSLog(@"Success !!!!!!");
                self.loadedDaysCount++;
                if( completionBlock )
                {
                    completionBlock( );
                }
                [ weakBlockOp markAsFinished ];

            
            
            } onFailure:^(NSError *error){
            
               //Handle Error
                NSLog(@"Failed To get Listing");
                
                [SVProgressHUD dismiss];
                self.isServerError = YES;

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                                message:@"Please try again later."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
               
            
            }];
          
            
        } ];
    self.isServerError = NO;
    return operation;
}

- (void) queueHasBeenExhausted
{
    [SVProgressHUD dismiss];

    self.dateSelectionUI.loadingMode = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getters & Setters
- (UICollectionView *) programGuide
{
    if( !_programGuide )
    {
        ProgramGuideLayout *layout = [ [ ProgramGuideLayout alloc ] initWithDataSource: self
                                                                       timeMarkerClass: [ ProgramGuideCurrentTimeIndicator class ]
                                                                             rowHeight: 52
                                                                           minuteWidth: kWidthOfMinute
                                                                     timeRulerInterval: 30*60 ];
        
        _programGuide = [ [ UICollectionView alloc ] initWithFrame: CGRectMake(0,kTopBarheight+ kSegmentHeight, CGRectGetWidth(MAIN_PAGE_FRAME)-kWidthOfPreviewSection-0, CGRectGetHeight(MAIN_PAGE_FRAME)-kSegmentHeight-kTopBarheight)
                                              collectionViewLayout: layout ];
        
        UITapGestureRecognizer *doubleTapFolderGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(processDoubleTap:)];
        [doubleTapFolderGesture setNumberOfTapsRequired:2];
        [_programGuide addGestureRecognizer:doubleTapFolderGesture];
        //_programGuide.backgroundColor =[ UIColor lightGrayColor ];
        _programGuide.backgroundColor = self.tableBackgroundColor;
        
        _programGuide.bounces = NO;
        
        [ _programGuide registerClass: [ XXCell class ] forCellWithReuseIdentifier: [ XXCell reuseIdentifier ] ];
        [ _programGuide registerClass: [ XXChannelCell class ] forSupplementaryViewOfKind: kChannelCellKindIdentifier withReuseIdentifier: [ XXChannelCell reuseIdentifier ] ];
        [ _programGuide registerClass: [ ProgramGuideRulerView  class] forSupplementaryViewOfKind: kTimeRulerCellKindIdentifier withReuseIdentifier: [ ProgramGuideTimeRulerCell cellReuseIdentifier ] ];
        
        
        _programGuide.dataSource    = self;
        _programGuide.delegate      = self;
    }
    
    return _programGuide;
}

- (void) processDoubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:_programGuide];
        NSIndexPath *indexPath = [_programGuide indexPathForItemAtPoint:point];
        if (indexPath)
        {
            NSLog(@"IndexPath : %d was double tapped",indexPath.row);
            
            if (self.selectedSectionChannel) {
                
                [self launchChannelWithId:self.selectedSectionChannel.channelId];
                
            }
            
        }
        else
        {
            //DoSomeOtherStuffHereThatIsntRelated;
        }
    }
}


- (EPGSegmentCollectionController *) dateSelectionUI
{
    if( !_dateSelectionUI )
    {
        
        _dateSelectionUI = [ [ EPGSegmentCollectionController alloc ] initWithCollectionViewLayout: [ [UICollectionViewFlowLayout alloc ] init ] ];
        
        _dateSelectionUI.delegate = self;
        _dateSelectionUI.view.frame = CGRectMake(0, kTopBarheight, CGRectGetWidth(SCREEN_FRAME_DEFAULT)-kWidthOfPreviewSection, kSegmentHeight);
    }
    
    return _dateSelectionUI;
}
#pragma mark - EPGSegmentCollectionDelegate

- (void) didSelectSegment:(NSInteger) index
{
    if (index == 0) {
        self.currentDayIndex = index;
        
    [ self.programGuide setContentOffset: [self gridInitialOffset] animated: YES ];
        
    }else{
    
    CGPoint currentOffset = self.programGuide.contentOffset;

    [ self.programGuide setContentOffset: CGPointMake(kOneDayContentWidth * index, currentOffset.y) animated: YES ];
    }
    
    NSLog(@"Selected a date");
}


#pragma mark - programguide helpers
- (CGPoint) gridInitialOffset;
{
    NSDate *now = [ NSDate date ];
    NSTimeInterval elapsedTimeSinceStartDate = [ now timeIntervalSinceDate:[ Utils startOfTheDay ] ];
    return CGPointMake( ((elapsedTimeSinceStartDate/kSecondsPerMinute) - kInitTimeOffset) * kWidthOfMinute, 0 );
}

- (void)updateSegmentCollectionSelection
{
    CGFloat currentOffset = self.programGuide.contentOffset.x;
    
    NSInteger newDayindex = (NSInteger) (currentOffset / kOneDayContentWidth);
    if (newDayindex >= 0 && newDayindex != self.currentDayIndex )
    {
        self.currentDayIndex = newDayindex;
        [ self.dateSelectionUI setSelectedIndex: newDayindex ];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [ self updateSegmentCollectionSelection ];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [ self updateSegmentCollectionSelection ];
}

- (void) sectionHeaderTapped: (UITapGestureRecognizer *) gesture
{

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)goBack:(id)sender {
    [SVProgressHUD dismiss];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


#pragma mark - program guide datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    Channel *sectionChannel = [self.channels objectAtIndex:section];
    NSArray *programs = [sectionChannel.shows allObjects];
    if (programs.count > 0) {
         return programs.count;
    }else{
        return 1;
    }
   
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.channels.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    XXCell *cell = (XXCell*) [collectionView dequeueReusableCellWithReuseIdentifier: [ XXCell reuseIdentifier ] forIndexPath:indexPath];
    Channel *sectionChannel = [self.channels objectAtIndex:indexPath.section];
    NSArray *programs = [sectionChannel.shows allObjects];
    if(programs.count > 0){
    
    Show *program = programs[ indexPath.row ];
    cell.titleLabel.text = program.title;
    //NSLog(@"Program Title : %@",program.title);
    cell.titleLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = self.tableBackgroundColor;
    cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.contentView.layer.borderWidth = 1.0;
   // NSDate * currentDate = [ NSDate date ];
   // NSDate *startDate   = [self getDate:program.startTimeUtc];
   // NSDate *endDate     = [self getDate:program.endTimeUTC];
    
   // if ( startDate && endDate && ( [ startDate compare: currentDate ] == NSOrderedAscending ) && ( [ endDate compare: currentDate ] == NSOrderedDescending ) )
   // {
       // cell.backgroundColor = [ UIColor orangeColor ];
   // }
   //cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
   //cell.contentView.layer.borderWidth = 1.0;
   if (self.selectedItemIndexPath != nil && [indexPath compare:self.selectedItemIndexPath] == NSOrderedSame) {
         cell.contentView.layer.borderColor = [[UIColor redColor] CGColor];
      }
  }else{
        
        cell.backgroundColor = self.tableBackgroundColor;
        cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];;
        cell.contentView.layer.borderWidth = 0.0;
    }
    
    return cell;
}

- (UICollectionReusableView *) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if( [ kind isEqualToString: kChannelCellKindIdentifier ] )
    {
       XXChannelCell *header = (XXChannelCell *) [ collectionView dequeueReusableSupplementaryViewOfKind: kChannelCellKindIdentifier withReuseIdentifier: [ XXChannelCell reuseIdentifier ] forIndexPath: indexPath ];
       header.sectionIndex = indexPath.section;
        
       UITapGestureRecognizer *tapGesture = [[ UITapGestureRecognizer alloc ] initWithTarget: self action: @selector( sectionHeaderTapped: ) ];
       tapGesture.numberOfTapsRequired = 1;
       [ header addGestureRecognizer: tapGesture ];
        
       Channel *channel = self.channels[ indexPath.section ];
       header.cellStyle = ProgramGuideHeaderStyleIconOnly;
        
       [header.imageView sd_setImageWithURL:[NSURL URLWithString:channel.channelLogo]];
       return header;
    }
    
    if( [ kind isEqualToString: kTimeRulerCellKindIdentifier ] )
    {
        
        ProgramGuideTimeRulerCell *ruler = (ProgramGuideTimeRulerCell *) [ collectionView dequeueReusableSupplementaryViewOfKind: kTimeRulerCellKindIdentifier withReuseIdentifier: [ ProgramGuideTimeRulerCell cellReuseIdentifier ] forIndexPath: indexPath ];
       
        ruler.backgroundColor = self.tableBackgroundColor;
        
        NSCalendar *calendar = [ NSCalendar currentCalendar ];
        NSDateComponents *components = [ calendar components: ( NSHourCalendarUnit | NSMinuteCalendarUnit ) fromDate: self.startDate ];
        NSInteger minute = [ components minute ];
        
        NSUInteger minutesToFirstMark = ( minute> 30 )? 60 - minute: 30 - minute;
        
        NSDate *actualDate = [ NSDate dateWithTimeInterval: indexPath.item*30*60 + ( minutesToFirstMark*60 ) sinceDate: self.startDate ];
        
        NSDateFormatter *formatter = [ Utils hoursMinutesAmPmFormatter ];
        
        ruler.label.text = [ formatter stringFromDate: actualDate ];
        return ruler;
    }
    
    return nil;
}

- (TimeRange *)collectionView: (UICollectionView *) collectionView layout: (UICollectionViewLayout *) layout timeRangeOfItemAtIndexPath: (NSIndexPath *)indexPath
{
    Channel *sectionChannel = [self.channels objectAtIndex:indexPath.section];
    NSArray *programs = [sectionChannel.shows allObjects];
    if (programs.count == 0) {
        
         return [ [ TimeRange alloc ] initWithDate: self.startDate duration: kGridTimeSpan * self.loadedDaysCount ];
        
    }else{
    Show *program = [programs objectAtIndex:indexPath.row];

    
    NSDate *startDate = [self getDate:program.startTimeUtc ];
    NSDate *endDate = [self getDate:program.endTimeUTC ];
    NSTimeInterval duration = [ endDate timeIntervalSinceDate: startDate ];
    
    return [ [ TimeRange alloc ] initWithDate: startDate duration: duration ];
        
    }
}

- (TimeRange *) timeRangeOfCollectionView: (UICollectionView *) collectionView layout: (UICollectionViewLayout *) layout
{
    return [ [ TimeRange alloc ] initWithDate: self.startDate duration: kGridTimeSpan * self.loadedDaysCount ];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithObject:indexPath];
    
    if (self.selectedItemIndexPath)
    {
        // if we had a previously selected cell
        
        if ([indexPath compare:self.selectedItemIndexPath] == NSOrderedSame)
        {
            // if it's the same as the one we just tapped on, then we're unselecting it
            
            self.selectedItemIndexPath = nil;
        }
        else
        {
            // if it's different, then add that old one to our list of cells to reload, and
            // save the currently selected indexPath
            
            [indexPaths addObject:self.selectedItemIndexPath];
            self.selectedItemIndexPath = indexPath;
        }
    }
    else
    {
        // else, we didn't have previously selected cell, so we only need to save this indexPath for future reference
        
        self.selectedItemIndexPath = indexPath;
    }
    
    // and now only reload only the cells that need updating

    
   // [collectionView reloadItemsAtIndexPaths:indexPaths];
    
    
    self.selectedSectionChannel= [self.channels objectAtIndex:indexPath.section];
    
    NSLog(@"Channel Name : %@",self.selectedSectionChannel.channelName);
    
    [self.previewImageView sd_setImageWithURL:[NSURL URLWithString:self.selectedSectionChannel.channelLogo]];
    
    NSArray *programs = [self.selectedSectionChannel.shows allObjects];
    
    if(programs.count == 0){
        
         [self setPreviewSectionWithShow:nil];
        
    }else{
    Show *program = programs[ indexPath.row ];
    self.selectedShow = program;
    
    //fill details
    
    [self setPreviewSectionWithShow:program];
        
    }

    
}

-(NSDate *) getDate:(NSString *) string{
    
    return [[Utils streamVsisionDateFormatter] dateFromString:string];
}


#pragma mark - spinner
- (void) addSpinner
{
    self.spinner = [[ UIActivityIndicatorView alloc ] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray ];
    self.spinner.center = SCREEN_CENTER;
    [ self.spinner startAnimating ];
    [ self.view addSubview: self.spinner ];
}

- (void) removeSpinner
{
    [ self.spinner stopAnimating ];
    [ self.spinner removeFromSuperview ];
    self.spinner = nil;
}

#pragma mark settings
- (IBAction)launchSettings:(id)sender {
    
    [self.mainviewController showSettingsFromEpg];
    //[self performSegueWithIdentifier:@"SettingsSegue1" sender:nil];
}

#pragma mark Preview

-(void) setPreviewSectionWithShow:(Show *) selectedShow{
    
    
    if(selectedShow!=nil){
    self.programTitleLabel.text = selectedShow.title;
    self.programDescriptionTextView.text = selectedShow.showDescription;
    
    NSDate *startDate   = [self getDate:selectedShow.startTimeUtc];
    NSDate *endDate     = [self getDate:selectedShow.endTimeUTC];

    [ self.programTimeLabel setText:[NSString stringWithFormat:@"%@ - %@",[self.epgTimeFormatter stringFromDate:startDate],[self.epgTimeFormatter stringFromDate:endDate]]];
        
    }
    
    // need to set the Image

    
    
}


- (IBAction)rewindButtonTapped:(id)sender {
    
    
    [self.device.mediaControl rewindWithSuccess:^(id responseObject)
     {
         NSLog(@"rewind success");
     } failure:^(NSError *error)
     {
         NSLog(@"rewind failure: %@", error.localizedDescription);
     }];
    
}

- (IBAction)pauseButtonTapped:(id)sender {
    
    
    
    if (self.isPaused==YES)
    {
        self.isPaused=NO;
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        
        
        [self.device.mediaControl playWithSuccess:^(id responseObject)
         {
             
             NSLog(@"play success");
         } failure:^(NSError *error)
         {
             NSLog(@"play failure: %@", error.localizedDescription);
         }];
        
    }
    else
    {
        [_pauseButton setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        self.isPaused=YES;
        [self.device.mediaControl pauseWithSuccess:^(id responseObject)
         {
             NSLog(@"pause success");
         } failure:^(NSError *error)
         {
             NSLog(@"pause failure: %@", error.localizedDescription);
         }];
        
        
    }
    
    
    
}

- (IBAction)forwardButtonTapped:(id)sender {
    
    
    [self.device.mediaControl fastForwardWithSuccess:^(id responseObject)
     {
         NSLog(@"fast forward success");
     } failure:^(NSError *error)
     {
         NSLog(@"fast forward failure: %@", error.localizedDescription);
     }];
    
    
}


-(void) launchChannelWithId:(NSString *)channelId{
    
    
    if(self.svAppRunning){
        
        
        [self launchChannel:channelId];
        
        
    }else{
           [self.device.launcher launchAppWithInfo:self.svApp success:^(LaunchSession *launchSession)
         {
             [SVProgressHUD dismiss];
             self.svAppRunning = YES;
             
             [self.delegate launchedSVChannel];
            
             [self launchChannel:channelId];
             
             NSLog(@"Launched application %@", launchSession);
         }                               failure:^(NSError *error)
         {
             [SVProgressHUD dismiss];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed to Launch Channel" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             
             
             NSLog(@"no launchApp %@", error);
         }];
    }
        
    }
    


-(void) launchChannel:(NSString *) channelId{
    
    NSString *post =[NSString stringWithFormat:@"%@/input?op=%@",self.deviceTargetURL,channelId];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:post]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:6];
    [request addValue:@"text/plain;charset=\"utf-8\"" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         
         if (connectionError)
         {
             [SVProgressHUD dismiss];
             //Handle Error
             
         } else
         {
             if ([httpResponse statusCode] < 200 || [httpResponse statusCode] >= 300)
             {
                 [SVProgressHUD dismiss];
                 //Handle Error
                 return;
             }
             
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             
             NSLog(@"Return Data : %@",dataString);
             
             [SVProgressHUD dismiss];
             
             
         }
         
     }];
    
    
}

- (IBAction)addToCalendar:(id)sender {
    
    [SVProgressHUD showWithStatus:kWaitMessage];
    
    [self addLocalNotification];
    
    EKEventStore *store = [[EKEventStore alloc] init];
    
    
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
           
            [SVProgressHUD dismiss];
             return;
            
        }
        
        EKEvent *event = [EKEvent eventWithEventStore:store];
        
        
       
            event.startDate = [self getDate:self.selectedShow.startTimeUtc];
       
        
        NSMutableString *message = [[NSMutableString alloc] init];
        [message appendString:[NSString stringWithFormat:@"Friendly reminder: %@",self.selectedShow.title]];
        
       
        
        [message appendString:@" will be airing on the RokuTV channel within the hour."];
        
        event.title = message;
        
        
        
        event.endDate = [self getDate:self.selectedShow.endTimeUTC];

    
        
        EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:[[self getDate:self.selectedShow.startTimeUtc] dateByAddingTimeInterval:-TIMETONOTIFY*60]];
        [event setAlarms:[NSArray arrayWithObject:alarm]];
        
        [event setCalendar:[store defaultCalendarForNewEvents]];
        
        NSError *err = nil;
        
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        
        if (error) {
            NSLog(@"error = %@", error);
            [SVProgressHUD dismiss];
        }
        
        UIAlertView *buyAlert = [[UIAlertView alloc] initWithTitle:@"Successfully Added to Calendar" message:@"Reminder has been set to device calendar" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [buyAlert show];
        
        [SVProgressHUD dismiss];
        
        return;
        
    }];
    

    
    
}

-(void) addLocalNotification{
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [[self getDate:self.selectedShow.startTimeUtc]  dateByAddingTimeInterval:-TIMETONOTIFY*60];
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendString:[NSString stringWithFormat:@"Friendly reminder: %@",self.selectedShow.title]];
    
    [message appendString:@" will be airing on the SundanceTV app within the hour."];
    
    localNotification.alertBody = message;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}

-(void) launchSelectedChannel:(id) sender{
    
    NSLog(@"Launching Selected Channel");
    
    //play channel:
   
    if (self.selectedSectionChannel) {
        
        [self launchChannelWithId:self.selectedSectionChannel.channelId];
    }
    
}

-(void) startPollTimer{
    
    self.pollTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                      target: self
                                                    selector: @selector(isPollingTimedOut:)
                                                    userInfo: nil
                                                     repeats: YES];
}

-(void)isPollingTimedOut:(NSTimer *)timer {
    
    NSDate* date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    if (![[dateFormat stringFromDate:self.currentDate] isEqualToString:[dateFormat stringFromDate:date]])
    {
        _currentDate = date;
       
        for (int i = 0; i< _queue.operationCount; ++i) {
            [_queue.operations[i] cancel];
        }
        
        [_queue cancelAllOperations];
        
       
        self.startDate = [ Utils startOfTheDay ];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.startDate forKey:kCurrentEPGDate];
        [defaults synchronize];
        
        [ self queueHasBeenExhausted];
        [SVProgressHUD showWithStatus:kWaitMessage];
        self.loadedDaysCount = 0;
        self.currentDayIndex = 0;
        self.isPaused = YES;
        self.channels = nil;
       //_queue = nil;
      // _queue = [ [ NSOperationQueue alloc ] init ];
       //_queue.maxConcurrentOperationCount = 2;
        [self.programGuide removeFromSuperview];
        [_dateSelectionUI.view removeFromSuperview];
        [self changeDateString];
        self.programGuide = nil;
        self.dateSelectionUI = nil;
        [self initFetchDataQueue];

       // [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void) changeDateString
{
    NSDateFormatter* theDateFormatter = [[NSDateFormatter alloc] init];
    [theDateFormatter setDateFormat:@"EEEE"];
    NSString *weekDay =  [theDateFormatter stringFromDate:[NSDate date]];
    [theDateFormatter setDateFormat:@"hh:mm a"];
    [theDateFormatter setAMSymbol:@"AM"];
    [theDateFormatter setPMSymbol:@"PM"];
    NSString *weekDay_Time =  [theDateFormatter stringFromDate:[NSDate date]];
    self.todayString =[NSString stringWithFormat:@"%@ , %@",weekDay,weekDay_Time];
}
@end
