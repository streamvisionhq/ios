//
//  LoginViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "LoginViewController.h"
#import "CoreDataManager.h"
#import "AuthenticationManager.h"
#import "ServiceProviderManager.h"
#import "NIDropDown.h"

@interface LoginViewController ()<UITextFieldDelegate, NIDropDownDelegate>
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *serviceProviderField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) NIDropDown *profileDropdown;

@end

@implementation LoginViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    User *currentUser = [[CoreDataManager sharedInstance] getUser];
    [self.userNameField setText:currentUser.registrationNumber];
    [self.serviceProviderField setText:currentUser.serviceProvider];
    

//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([[defaults valueForKey:kIsLoggedIn] isEqualToString:kSVYes])
//        [self performSegueWithIdentifier:kDiscoverySegue sender:self];
    
    self.containerView.layer.cornerRadius = 5.0f;
    [self.userNameField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)loginPressed:(id)sender {
    [self authenticateServiceProvider];
}

-(void)authenticateCustomer
{
//    if (self.userNameField.text != nil && self.userNameField.text.length > 0 ) {
    
        [[CoreDataManager sharedInstance] deleteCurrentUser];
        [[CoreDataManager sharedInstance] deleteRegisteredDevices];
        
        [[CoreDataManager sharedInstance] createUserWithRegistrationNumber:self.userNameField.text andServiceProvider:self.serviceProviderField.text password:self.passwordField.text];
        [SVProgressHUD showWithStatus:kWaitMessage];
        [[AuthenticationManager sharedInstance] setCallingViewControllerObj:self];
    [[AuthenticationManager sharedInstance] getRegisteredDevicesForId:self.userNameField.text Pass:self.passwordField.text onSuccess:^(NSArray *devicesList){
            
            [[CoreDataManager sharedInstance] addRegisteredDevices:devicesList];
            
            NSLog(@"Success");
            
            NSArray *regDevices = [[CoreDataManager sharedInstance] getRegisteredDevices];
            
            if (regDevices.count > 0) {
                [SVProgressHUD dismiss];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:kSVYes forKey:kIsLoggedIn];
                [defaults synchronize];
                
                [self performSegueWithIdentifier:kDiscoverySegue sender:self];
                
            }else{
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:kSVNo forKey:kIsLoggedIn];
                [defaults synchronize];
                
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Devices for user Id" message:@"The User Id you have entered does not have any devices registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                
            }
            
        }onFailure:^(NSError *error){
            
            NSLog(@"Error");
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:kSVNo forKey:kIsLoggedIn];
            [defaults synchronize];
            [SVProgressHUD dismiss];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                            message:@"Please try again later."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
        
        
        
        
//    }else{
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"try again" otherButtonTitles: nil];
//        [alert show];
//        
//    }
}

-(void)authenticateServiceProvider
{
    if (self.userNameField.text.length <= 0 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(_serviceProviderField.text.length <= 0)
        [[[UIAlertView alloc] initWithTitle:kValidServiceProvider message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    else if (_passwordField.text.length <= 0)
        [[[UIAlertView alloc] initWithTitle:kValidPassword message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    else
    {
        [SVProgressHUD showWithStatus:kSPVerifyMessage];

        [[ServiceProviderManager sharedInstance] getServiceProvider:_serviceProviderField.text onSuccess:^(NSDictionary *devicesList)
         {
             [SVProgressHUD dismiss];
             [self authenticateCustomer];
            
        } onFailure:^(NSError *error) {
            [SVProgressHUD dismiss];

        }];
    }


}

-(void )doLogin
{
    [self loginPressed:_loginButton];
}

- (IBAction)onProfileDD:(id)sender
{
    if(_profileDropdown)
    {
        [_profileDropdown hideDropDown:sender];
        _profileDropdown = nil;
    }
    else
    {
        _profileDropdown = [[NIDropDown alloc]init];
        _profileDropdown.delegate = self;
        [_profileDropdown showDropDown:sender :@[@"main"] :nil :@"down"];
    }
}


-(void)niDropDownDelegateMethod:(NIDropDown *)sender selectedIndex:(NSInteger)index
{
    [_profileDropdown hideDropDown:_btnProfileDD];
    _profileDropdown = nil;
}





#pragma mark UITextField Delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

@end
