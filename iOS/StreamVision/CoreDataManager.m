//
//  CoreDataManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 19/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "CoreDataManager.h"
#import "StreamVisionParserConstants.h"
#import "Utils.h"

@implementation CoreDataManager

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+(instancetype) sharedInstance
{
    static CoreDataManager *_coreDataManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _coreDataManager = [ [ self alloc ] init ];
    });
    
    return _coreDataManager;
}


-(id) init{
    
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newDeviceFound:) name:kFoundDeviceNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lostDevice:) name:kLostDeviceNotification object:nil];
        //k
        
        
       
        
    }
    return self;
}





-(void) newDeviceFound:(NSNotification *) note{
    
    NSLog(@"Found Device");
    
    ConnectableDevice *device = (ConnectableDevice *) note.object;
    [self addDeviceToLocalStore:device];

}

-(void) lostDevice:(NSNotification *) note{
    
    NSLog(@"Remove Device");
    
    ConnectableDevice *device = (ConnectableDevice *) note.object;
    [self removeDeviceFromLocalStore:device];
    

    
    
}



- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.ternionsoft.StreamVision" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"StreamVision" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"StreamVision.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


-(void)  deletAllData{
    
    NSError *error;
    // retrieve the store URL
    NSURL * storeURL = [[self.managedObjectContext persistentStoreCoordinator] URLForPersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject]];
    // lock the current context
    [self.managedObjectContext lock];
    [self.managedObjectContext reset];//to drop pending changes
    //delete the store from the current managedObjectContext
    
    if ([[self.managedObjectContext persistentStoreCoordinator] removePersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject] error:&error])
    {
        // remove the file containing the data
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
        
        //recreate the store like in the  appDelegate method
        [[self.managedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];//recreates the persistent store
    }
    [self.managedObjectContext unlock];
    //that's it !
    
}

-(BOOL) checkIfEntityExists:(NSString *)key value:(NSString*)value withEntity:(NSString*)entityName
{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", key,value];
    
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        return NO;
        
    }else{
        return YES;
    }
}




-(void) createChannelsWithData:(NSArray *) channelsArray{
    
    for(NSDictionary *channelDict in channelsArray){
        
        if (![self checkIfEntityExists:@"channelId" value:[channelDict valueForKey:kChannelIdKey] withEntity:NSStringFromClass([Channel class])]) {
        
        Channel *aChannel = [[Channel alloc] initWithEntity:[NSEntityDescription entityForName:@"Channel" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
        
        if ([channelDict valueForKey:kChannelIdKey]!=nil) {
            
            aChannel.channelId = [[channelDict valueForKey:kChannelIdKey] stringValue];
            
        }
        
        if ([channelDict valueForKey:kChannelDescriptionKey]!=nil) {
            
            aChannel.channelDescription = [channelDict valueForKey:kChannelDescriptionKey];
            aChannel.channelName =[channelDict valueForKey:kChannelDescriptionKey];
            
        }
        
        if ([channelDict valueForKey:kChannelNumberKey]!=nil) {
            if ([[channelDict valueForKey:kChannelNumberKey] isKindOfClass:[NSString class]]) {
                
            }else{
            aChannel.channelNumber = [[channelDict valueForKey:kChannelNumberKey] stringValue];
            }
            
        }
        
        if ([channelDict valueForKey:kChannelLogoKey]!=nil) {
            
            aChannel.channelLogo = [channelDict valueForKey:kChannelLogoKey];
            
        }
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
             
         }
        
    }
    
    
}

-(NSString*) conversionDate:(NSString*)dateString
{
    NSDateFormatter *inDateFormatter = [Utils streamVsisionDateFormatter];
    inDateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDate *inDate = [inDateFormatter dateFromString:dateString];
    
    // about output date local
    NSDateFormatter *outDateFormatter = [Utils streamVsisionDateFormatter];
    outDateFormatter.timeZone = [NSTimeZone localTimeZone];
    NSString *outDateStr = [outDateFormatter stringFromDate:inDate];
    return outDateStr;
}

-(void) createShowsForChannels:(NSArray *) channelAndShowsArray{
    
    /*"startTimeUtc":"2015-05-29T07:00:00+0000","endTimeUtc":"2015-05-29T07:30:00+0000","Title":"The Flipside with Michael Loftus","Description":"Spinning political correctness on its head is comic Michael Loftus, as he shares a fresh point of view on the latest events in America with bold interpretations and engaging interviews featuring guests with opposing views on politics.","ShowType":"Series","Cast":"Michael Loftus","DisplayGenre":"Comedy","Rating":"TV-G","VideoURL":"https:\/\/streamvisionhq.a.cdnify.io\/rajaranis_1432818965.mp4"
     */
    
    for(NSDictionary *channelDict in channelAndShowsArray){
        
        NSString *channelId = channelDict[kChannelIdKey];
        Channel *currentChannel = [self getChannelForChannelId:channelId];
        
        if (currentChannel != nil) {
            
            NSArray *allShows = channelDict[kChannelShowsParserKey];
            
            for(NSDictionary *showDict in allShows){
                
                Show *aShow = [[Show alloc] initWithEntity:[NSEntityDescription entityForName:@"Show" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                
                //aShow.startTimeUtc = showDict[kStartTimeUtcKey];
                aShow.startTimeUtc = [self conversionDate:showDict[kStartTimeUtcKey]];

                //aShow.endTimeUTC = showDict[kEndTimeUtcKey];
                aShow.endTimeUTC = [self conversionDate:showDict[kEndTimeUtcKey]];
                
                aShow.title = showDict[kTitleKey];
                aShow.showDescription = showDict[kDescriptionKey];
                aShow.showType = showDict[kShowTypeKey];
                
                //NSDate *test = [[Utils streamVsisionDateFormatter] dateFromString:showDict[kStartTimeUtcKey]];
                
                
                if (showDict[kCastKey]!=nil) {
                   
                    if ([showDict[kCastKey] isKindOfClass:[NSString class]]) {
                        
                         aShow.cast = showDict[kCastKey];
                        
                    }else{
                    NSArray *castArray = showDict[kCastKey];
                    
                    NSMutableString *castString = [[NSMutableString alloc] init ];
                    for(NSString *cast in castArray){
                        
                        [castString appendString:cast];
                        if (!([castArray lastObject] == cast)) {
                            [castString appendString:@", "];
                        }
                    }
                    
                    aShow.cast = castString;
                    }
                }
               
               
                
                aShow.displayGenre = showDict[kDisplayGenreKey];
                aShow.videoURL = showDict[kVideoURLKey];
                
                if (showDict[kRatingKey]!=nil) {
                    
                    Rating *aRating = [[Rating alloc] initWithEntity:[NSEntityDescription entityForName:@"Rating" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                    
                    aRating.tv = showDict[kRatingKey];
                    
                    aShow.rating = aRating;
                    
                }
                
                
                [currentChannel addShowsObject:aShow];
                
                NSError *error;
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                
                
            }
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
            
        }
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        
        
    }
    
    
    
}

-(Channel *) getChannelForChannelId:(NSString *) channelNumber{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Channel"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"channelId=%@",channelNumber]];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        return nil;
        
    }else{
        return [fetchedObjects objectAtIndex:0];
    }

    
    
    return nil;
}

-(NSArray *) getAllChannels{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Channel"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //[fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"channelId" ascending:YES]]];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        return nil;
        
    }else{
        return fetchedObjects;
    }
    

    
    return nil;
}

-(NSArray *) getChannelsForEPG{
    
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Channel"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //[fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"channelId" ascending:YES]]];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        return nil;
        
    }else{
       
       /* NSMutableArray *returnValue = [[NSMutableArray alloc] init];
        
        for(Channel *aChannel in fetchedObjects){
            
            if (aChannel.shows.count > 0) {
                [returnValue addObject:aChannel];
            }
            
        }
        
        return returnValue;*/
        
        return fetchedObjects;
    }

    
}

-(void) createUserWithRegistrationNumber:(NSString *)regNo andServiceProvider:(NSString *)spid password:(NSString*)pass{
    
    
    if (regNo != nil) {
        
        User *aUser = [[User alloc] initWithEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
        aUser.registrationNumber = regNo;
        aUser.serviceProvider = spid;
        aUser.password = pass;
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }

        
    }
}

-(User *) getUser{
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        return nil;
        
    }else{
        return [fetchedObjects objectAtIndex:0];
    }

    
   
}

-(void) addDeviceToLocalStore:(ConnectableDevice *) device{
    
  /*  if (![self checkIfEntityExists:kDeviceIdKey value:device.id withEntity:NSStringFromClass([Device class])]) {
        
        Device *aDevice = [[Device alloc] initWithEntity:[NSEntityDescription entityForName:@"Device" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
        
        aDevice.deviceIdentifier = device.id;
        aDevice.rokuDeviceId = device.friendlyName;
        aDevice.isOnline = [NSNumber numberWithBool:YES];
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }

        
    }else{ */
    
    
    NSArray *servicesArray = device.services;
    DeviceService *service = [servicesArray objectAtIndex:0];
    ServiceDescription *serviceDesc = service.serviceDescription;
    //NSLog(@"UUID : %@",serviceDesc.UUID);
    

        Device *onlineDevice = [self getDeviceForId:serviceDesc.UUID];
    
    
      if (onlineDevice) {
            [onlineDevice setIsOnline:[NSNumber numberWithBool:YES]];
            
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }
        
    //}
    
    
}


-(void) updateOfflineStatusForDevice:(ConnectableDevice *) device{
    
    
    NSArray *servicesArray = device.services;
    DeviceService *service = [servicesArray objectAtIndex:0];
    ServiceDescription *serviceDesc = service.serviceDescription;
    //NSLog(@"UUID : %@",serviceDesc.UUID);
    
    
    Device *onlineDevice = [self getDeviceForId:serviceDesc.UUID];
    
    
    if (onlineDevice) {
        [onlineDevice setIsOnline:[NSNumber numberWithBool:NO]];
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }

}



-(Device *) getDeviceForId:(NSString *) deviceId{
    
    //TCL TV
    
    NSArray *allDevices = [self getAllDevices];
    for(Device *dev in allDevices){
        
        if ([deviceId containsString:dev.deviceIdentifier]) {
            
            return dev;
        }
        
    }
    
    return nil;
}

-(void) removeDeviceFromLocalStore:(ConnectableDevice *) device{
    
   
    //NSArray *allDevices = [self getAllDevices];
    Device *toDelete = nil;
    
    
    NSArray *servicesArray = device.services;
    if (servicesArray.count > 0) {
        
        
        DeviceService *service = [servicesArray objectAtIndex:0];
        ServiceDescription *serviceDesc = service.serviceDescription;
        //NSLog(@"UUID : %@",serviceDesc.UUID);
        
        
        toDelete = [self getDeviceForId:serviceDesc.UUID];
        
        
        if(toDelete){
            
            [toDelete setIsOnline:[NSNumber numberWithBool:NO]];
            
            
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
            
        }
        

    }
    
    
    
}


-(void) setAllDevicesOffline{
    
    NSArray *allDevices = [self getAllDevices];
    
    for(Device *dev in allDevices){
        
        dev.isOnline = [NSNumber numberWithBool:NO];
    }
}

-(NSArray *) getAllDevices{
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Device"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
       
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        
        return nil;
        
    }else{
        
        return fetchedObjects;
    }
    

    
}

-(void) deleteAllChannelsData{
    
    [self deleteAllShows];
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Channel"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *aChannel in fetchedObjects) {
        [self.managedObjectContext deleteObject:aChannel];
    }
    

    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    
}

-(void) deleteAllShows{
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Show"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *aShow in fetchedObjects) {
        [self.managedObjectContext deleteObject:aShow];
    }
    
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }

    
}

-(void) addRegisteredDevices:(NSArray *) devices{
    
    /*
     "DeviceID": "1GN386030678",
     
     "AccountID": 34096,
     
     "Identifier": "Living Room",
     
     "ServiceType": "StreamVisionBronze"
     */
    
    for(NSDictionary *deviceInfoDict in devices){
        
        if (![self checkIfEntityExists:kDeviceIdKey value:deviceInfoDict[@"DeviceID"] withEntity:NSStringFromClass([Device class])]) {
        
            Device *aDevice = [[Device alloc] initWithEntity:[NSEntityDescription entityForName:@"Device" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
            aDevice.deviceIdentifier = deviceInfoDict[@"DeviceID"];
            aDevice.accountId =[deviceInfoDict[@"AccountID"] stringValue];
            if(deviceInfoDict[@"Identifier"]){
                if ([deviceInfoDict[@"Identifier"] length] > 0) {
                  aDevice.deviceLocation =[NSString stringWithFormat:@"%@",deviceInfoDict[@"Identifier"]];
                }else{
                    aDevice.deviceLocation=@"No Device Identifier Available";
                }
           
            }else{
                 aDevice.deviceLocation=@"No Device Identifier Available";
            }
            aDevice.serviceType = deviceInfoDict[@"ServiceType"];
            aDevice.isRegistered = [NSNumber numberWithBool:YES];
            
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
            
        }
        
        
    }
    
    
}


-(void) deleteRegisteredDevices{
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Device"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
   // [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"isRegistered==1"]];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *aChannel in fetchedObjects) {
        [self.managedObjectContext deleteObject:aChannel];
        
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    
    }
    
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    

    
}

-(NSArray *) getRegisteredDevices{
    
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Device"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"isRegistered==1"]];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if ([fetchedObjects count] == 0) {
        //handle error maybe throw an alert
        
        return nil;
        
    }else{
        
        return fetchedObjects;
    }


    
}

-(void) updateOnlineStatus:(NSArray *) discoveredDevices{
    
    for(ConnectableDevice *device in discoveredDevices){
        
        NSArray *servicesArray = device.services;
        
        DeviceService *service = [servicesArray objectAtIndex:0];
        // Search for roku tv service
        for (DeviceService *dService in servicesArray) {
            if([dService.serviceDescription.UUID rangeOfString:@"roku:ecp:"].location != NSNotFound)
                service = dService;
        }
        
        ServiceDescription *serviceDesc = service.serviceDescription;
        //NSLog(@"UUID : %@",serviceDesc.UUID);
        
        
        Device *onlineDevice = [self getDeviceForId:serviceDesc.UUID];
        
        
        if (onlineDevice) {
            [onlineDevice setIsOnline:[NSNumber numberWithBool:YES]];
            
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }

    }
    
}


-(void) deleteCurrentUser{
    
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *aUser in fetchedObjects) {
        [self.managedObjectContext deleteObject:aUser];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }

}

@end
