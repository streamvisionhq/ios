//
//  ProgramGuideTimeRulerCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ProgramGuideTimeRulerCell.h"
#import "UIView+AutolayoutExtensions.h"


@interface ProgramGuideTimeRulerCell( )
@property (nonatomic, strong, readwrite) UILabel *label;
@property (nonatomic, strong) NSArray *timeRulerConstraints;
@end

static NSString *const kCellReuseIdentifier = @"programGuideTimeRulerCell";

@implementation ProgramGuideTimeRulerCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [ self configureView ];
        
        [ self setLabelsFonts ];
        
        [ self initConstraints ];
        
        [ self applyConstraints ];
        
        [ self initFontListener ];
    }
    return self;
}
- (void) initFontListener
{
    [ [ NSNotificationCenter defaultCenter ] addObserver: self
                                                selector: @selector( setLabelsFonts )
                                                    name: UIContentSizeCategoryDidChangeNotification
                                                  object: nil ];
}

- (void) dealloc
{
    [ [ NSNotificationCenter defaultCenter ] removeObserver: self ];
}

- (void) configureView
{
    self.backgroundColor = [ UIColor blueColor ];
    [ self addSubview: self.label ];
}

- (UILabel *) label
{
    if( !_label )
    {
        _label = [ UILabel autoLayoutView ];
    }
    
    return _label;
}

- (void) setLabelsFonts
{
    [ self.label setFont:  [ UIFont systemFontOfSize:15.0 ] ];
}

- (void) initConstraints
{
    NSMutableArray *constraints = [ [ NSMutableArray alloc ] init ];
    
    [ constraints addObject: [ self.label centerInContainerOnAxis: NSLayoutAttributeCenterY ] ];
    
    self.timeRulerConstraints = [ NSArray arrayWithArray: constraints ];
}

- (void) applyConstraints
{
    [ self addConstraints: self.timeRulerConstraints ];
    [ self layoutIfNeeded ];
}

+ (NSString *) cellReuseIdentifier
{
    return kCellReuseIdentifier;
}
@end
