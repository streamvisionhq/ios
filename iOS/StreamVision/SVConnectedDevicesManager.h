//
//  SVDeviceManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 06/07/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllEntities.h"
#import <ConnectSDK/ConnectSDK.h>

@interface SVConnectedDevicesManager : NSObject

+(instancetype) sharedInstance;

-(ConnectableDevice *) getCurrentConnectedDevice;
-(Device *) getCurrentSVDevice;

@property (nonatomic,strong) ConnectableDevice *connectedRokuDevice;
@property (nonatomic,strong) Device *connectedSVDevice;

@end
