//
//  ChannelServiceManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ChannelServiceManager.h"
#import "ChannelParser.h"
#import "AFHTTPRequestOperation.h"
#import "SVNetworkCheck.h"
#import "ServiceProviderManager.h"


//static NSString *const kChannelEndPoint =  @"http://106.51.226.151/sv/code/api/epg/channels";

static NSString *const kChannelEndPoint = @"api/epg/channels";
//@"http://106.51.226.151/svapi/public/epg/channels”;

@interface ChannelServiceManager()

@property (nonatomic,strong) ChannelParser *parser;

@end

@implementation ChannelServiceManager



+(instancetype) sharedInstance
{
    static ChannelServiceManager *_manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _manager = [ [ self alloc ] init ];
    });
    
    return _manager;
}


-(ChannelParser *) parser{
    
    if (!_parser) {
        _parser = [[ChannelParser alloc] init];
    }
    
    return _parser;
    
}

-(void) getAllChannels: (void(^)(void))completionBlock
             onFailure: (ErrorBlock)failureBlock
{
    
    
    NSString *requestStr = [NSString stringWithFormat:@"%@%@",[ServiceProviderManager getCMSEndPoint],kChannelEndPoint];
    [self performRequest:requestStr success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if( completionBlock )
        {
            
            [self.parser parseChannelResponse:(NSData *) responseObject];
            completionBlock();
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
        if(failureBlock)
        {
            failureBlock( error );
        }
        
    }];
    

    
    
}



- (void)performRequest: (NSString *) requestString
                         success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    if ([self checkIfNetworkIsReachable]) {
        
        //Proceed
        
    }else{
        
        [SVProgressHUD showErrorWithStatus:kNoNetworkMessage maskType:SVProgressHUDMaskTypeGradient];
        
        
        return;
        
        
    }
    
    NSURL *url = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    
}


-(BOOL)checkIfNetworkIsReachable{
    
    return [SVNetworkCheck hasConnectivity];
}



@end
