//
//  ProgramGuideRulerView.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ProgramGuideRulerView.h"


@implementation ProgramGuideRulerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [ UIColor clearColor ];
        self.label.font = [UIFont systemFontOfSize:15.0f];
        self.label.textColor = [ UIColor whiteColor ];
    }
    return self;
}

@end