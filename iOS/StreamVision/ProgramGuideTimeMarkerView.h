//
//  ProgramGuideTimeMarkerView.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Time marker, will be positioned by ProgramGuideLayout. The label and hairline are exposed to facilitate customization.
 */
@interface ProgramGuideTimeMarkerView : UICollectionReusableView
/**
 *  The UILabel containing the time marker text. By default, it displays NSLocalizedString( @"Now")
 */
@property(nonatomic,strong) UILabel *titleLabel;
/**
 *  The hairline, by default 1px wide
 */
@property(nonatomic,strong) UIView *hairLine;
@end
