//
//  ClosedCaption.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ClosedCaption.h"
#import "Show.h"


@implementation ClosedCaption

@dynamic language;
@dynamic caption608;
@dynamic isDigital;
@dynamic easyReader;
@dynamic wideAspectRatio;
@dynamic ccCode;
@dynamic ccDescription;
@dynamic show;

@end
