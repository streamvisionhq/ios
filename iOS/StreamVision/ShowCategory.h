//
//  ShowCategory.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Show;

@interface ShowCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSSet *shows;
@end

@interface ShowCategory (CoreDataGeneratedAccessors)

- (void)addShowsObject:(Show *)value;
- (void)removeShowsObject:(Show *)value;
- (void)addShows:(NSSet *)values;
- (void)removeShows:(NSSet *)values;

@end
