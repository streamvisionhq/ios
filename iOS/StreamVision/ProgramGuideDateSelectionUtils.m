//
//  ProgramGuideDateSelectionUtils.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ProgramGuideDateSelectionUtils.h"

#import "Utils.h"

static NSString * const kToday    = @"Today";
static NSString * const kTomorrow = @"Tomorrow";

static const NSInteger kAdayInSeconds   = 24 * 60 * 60;

static NSDate * today = nil;

NSUInteger const ProgramGuideDateSelectionUtilsSegCapacity     = 4;

@implementation ProgramGuideDateSelectionUtils

+ (ProgramGuideDateSelectionDate *) segmentData:(NSInteger) index;
{
    //if (!today)
    {
        today = [NSDate date];
    }
    
    
    NSDate * currentDate = [NSDate dateWithTimeInterval:kAdayInSeconds * index sinceDate: today];
    NSString * calanderday = [[Utils calanderFormatter] stringFromDate: currentDate];
    
    NSString * weekday = nil;
    if (index == 0)
    {
        weekday = kToday;
    }
    else if (index == 1)
    {
        weekday = kTomorrow;
    }
    else
    {
        weekday =  [[Utils weekdayFormatter] stringFromDate: currentDate];
    }
    
    return [ [ ProgramGuideDateSelectionDate alloc ] initWithDayName: weekday date: calanderday ];
}
@end

@interface ProgramGuideDateSelectionDate( )
@property (nonatomic, strong) NSString *dayName;
@property (nonatomic, strong) NSString *date;
@end

@implementation ProgramGuideDateSelectionDate
-(instancetype) initWithDayName: (NSString *) dayName date: (NSString *) date
{
    self = [ super init ];
    if( self )
    {
        _dayName = dayName;
        _date = date;
    }
    
    return self;
}

@end