//
//  Channel.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "Channel.h"
#import "EPG.h"
#import "Show.h"


@implementation Channel

@dynamic broadcastType;
@dynamic callsign;
@dynamic channelDescription;
@dynamic channelId;
@dynamic channelNumber;
@dynamic majorChannelNumber;
@dynamic minorChannelNumber;
@dynamic network;
@dynamic channelLogo;
@dynamic channelName;
@dynamic epg;
@dynamic shows;

@end
