//
//  SVDeviceManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 06/07/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "SVConnectedDevicesManager.h"

@implementation SVConnectedDevicesManager

@synthesize connectedRokuDevice,connectedSVDevice;

+(instancetype) sharedInstance
{
    static SVConnectedDevicesManager *_deviceManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _deviceManager = [ [ self alloc ] init ];
    });
    
    return _deviceManager;
}


-(id) init{
    
    self = [super init];
    if (self) {
        

    }
    return self;
}

-(ConnectableDevice *) getCurrentConnectedDevice{
    
    return self.connectedRokuDevice;
}

-(Device *) getCurrentSVDevice{
    
    return self.connectedSVDevice;
}

@end
