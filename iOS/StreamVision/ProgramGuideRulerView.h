//
//  ProgramGuideRulerView.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 ProgramGuideRulerView is used for displaying ruler in EPG view.
                        ------
 */

#import "ProgramGuideTimeRulerCell.h"

@interface ProgramGuideRulerView : ProgramGuideTimeRulerCell

@end
