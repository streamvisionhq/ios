//
//  ServiceProvider.h
//  StreamVision
//
//  Created by irfan on 24/01/2016.
//  Copyright (c) 2016 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceProvider : NSObject

@property (nonatomic, strong) NSString *spId;
@property (nonatomic, strong) NSString *heading;
@property (nonatomic, strong) NSString *cmsUrl;
@property (nonatomic, strong) NSString *logoUrl;

-(void)set:(NSDictionary *)object;

@end
