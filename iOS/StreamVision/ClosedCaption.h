//
//  ClosedCaption.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Show;

@interface ClosedCaption : NSManagedObject

@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSNumber * caption608;
@property (nonatomic, retain) NSNumber * isDigital;
@property (nonatomic, retain) NSNumber * easyReader;
@property (nonatomic, retain) NSNumber * wideAspectRatio;
@property (nonatomic, retain) NSString * ccCode;
@property (nonatomic, retain) NSString * ccDescription;
@property (nonatomic, retain) Show *show;

@end
