//
//  ProgramGuideTimeMarkerView.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//


#import "ProgramGuideTimeMarkerView.h"
#import "UIView+AutolayoutExtensions.h"

const NSString *kProgramGuideTimeIndicatorViewKind = @"ProgramGuideTimeIndicatorView";

static NSString * const kNow = @"Now";
static const CGFloat kCornerRadius = 2.0f;
static const CGFloat kNowLabelHeight = 15.0f;
static const CGFloat kNowLabelWidth = 45.0f;
static const CGFloat kHairLineWidth = 1.0f;
static const CGFloat kNowFontSize   = 10.0f;

@implementation ProgramGuideTimeMarkerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [ self addSubview: self.titleLabel ];
        [ self addSubview: self.hairLine ];
    }
    return self;
}

- (NSString *) reuseIdentifier
{
    return (NSString *) kProgramGuideTimeIndicatorViewKind;
}

-(UILabel *) titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [[UILabel alloc] initWithFrame: ( CGRect ) {0, 0, kNowLabelWidth, kNowLabelHeight } ];
        _titleLabel.text = NSLocalizedString( kNow, @"ProgramGuideTimeIndicator" );
        _titleLabel.textColor = [ UIColor whiteColor ];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [ UIFont systemFontOfSize: kNowFontSize ];
        _titleLabel.backgroundColor = [ UIColor blueColor ];
        _titleLabel.layer.cornerRadius = kCornerRadius;
    }
    
    return _titleLabel;
}

-(UIView *) hairLine
{
    if (!_hairLine)
    {
        _hairLine = [ UIView autoLayoutView ];
        _hairLine.frame = ( CGRect ) { kNowLabelWidth/2, kNowLabelHeight, kHairLineWidth, CGRectGetHeight( self.frame ) };
        _hairLine.backgroundColor = [ UIColor blueColor ];
    }
    return _hairLine;
}

@end