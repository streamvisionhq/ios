//
//  SettingsTableViewCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 09/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
/*                      ------
 RokuListingTableCell is used for handelling device selection.
                        ------
 */

#import <UIKit/UIKit.h>

@protocol ListingCellDelegate;

@interface RokuListingTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceId;
@property (weak, nonatomic) IBOutlet UILabel *registered;
@property (weak, nonatomic) IBOutlet UILabel *identfier;
@property (weak, nonatomic) IBOutlet UILabel *serviceType;
@property (weak, nonatomic) IBOutlet UIImageView *onlineImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
- (IBAction)selectButtonTapped:(id)sender;

@property (nonatomic,strong) id<ListingCellDelegate> delegate;

@end

@protocol ListingCellDelegate <NSObject>

-(void) selectedDeviceAtIndex:(NSUInteger) index;

@end
