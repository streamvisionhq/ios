//
//  AuthenticationManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 17/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "AuthenticationManager.h"
#import "AFHTTPRequestOperation.h"
#import "SVNetworkCheck.h"
#import "Utils.h"
#import "StreamVisionParserConstants.h"
#import "LoginViewController.h"
#import "UserSettingViewController.h"
#import "ServiceProviderManager.h"
#import <UIKit/UIKit.h>
//static NSString *const kAuthenticationEndpoint = @"http://106.51.226.151/sv/code/api/customer/";
static NSString *const kAuthenticationEndpoint = @"api/customer/";


//@"http://106.51.226.151/svapi/public/customer/";

//@"http://106.51.226.151/sv/code/api/public/customer/"; //26773
static NSString *const kAuthenticationDevicesSuffix = @"/devices";

@implementation AuthenticationManager
UIViewController* senderViewController = 0;

+(instancetype) sharedInstance
{
    static AuthenticationManager *_manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _manager = [ [ self alloc ] init ];
    });
    
    return _manager;
}


-(void) getRegisteredDevicesForId:(NSString *) customerId Pass:(NSString *)pass onSuccess:(void(^)(NSArray *devicesList))completionBlock onFailure: (ErrorBlock)failureBlock{
    
    NSString *requestString = [NSString stringWithFormat:@"%@%@%@/%@%@",[ServiceProviderManager getCMSEndPoint],kAuthenticationEndpoint,customerId,pass,kAuthenticationDevicesSuffix];
    
   [self performRequest:requestString success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if( completionBlock )
        {
            NSError *error;
            
          /* NSString *filePath = [[NSBundle mainBundle] pathForResource:@"device" ofType:@"json"];
            NSData* jSONFileData = [NSData dataWithContentsOfFile:filePath];*/
            

          NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            //below for testing
           // NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData:jSONFileData options:kNilOptions error:&error];
           
            NSArray *allDevices = nil;
            BOOL success = [[JSONResponseDict valueForKey:kSuccessKey] boolValue];
           
            
            
            
            if (success) {
                allDevices = [JSONResponseDict valueForKey:kDataKey];
                 completionBlock(allDevices);

            }else{
                
                failureBlock(error);
                
            }
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failureBlock)
        {
            failureBlock( error );
        }
        
    }];
}



- (void)performRequest: (NSString *) requestString
               success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    if ([self checkIfNetworkIsReachable]) {
        
        //Proceed
        
    }else{
        
       // [SVProgressHUD showErrorWithStatus:kNoNetworkMessage maskType:SVProgressHUDMaskTypeGradient];
      [SVProgressHUD dismiss];
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kConnectionUnAvailable message:nil delegate:self cancelButtonTitle:@"try again" otherButtonTitles: @"quit",nil];

      [alert show];
      return;
    }
    
    NSURL *url = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"try again"])
    {
        LoginViewController* login  = (LoginViewController*)senderViewController;
        UserSettingViewController* userSetting  = (UserSettingViewController*)senderViewController;

        if(login)
            [login doLogin];
        else if(userSetting)
            [userSetting doLogin];

    }
    else if([title isEqualToString:@"quit"])
    {
        exit(0);
    }
}

-(void) setCallingViewControllerObj:(UIViewController*) controllerObj
{
    senderViewController =controllerObj;
}

-(BOOL)checkIfNetworkIsReachable{
    
    return [SVNetworkCheck hasConnectivity];
}

@end
