//
//  AppCollectionHeader.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ConnectSDK/ConnectSDK.h>

@protocol AppCollectionHeaderDelegate;

@interface AppCollectionHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (nonatomic,strong) NSString *deviceTargetURL;


@property (nonatomic,strong) id<AppCollectionHeaderDelegate> delegate;

-(void) setUpwithApp:(AppInfo *) app;

@end

@protocol AppCollectionHeaderDelegate <NSObject>

@optional
-(void) headerSelected:(AppInfo *) app;

@end
