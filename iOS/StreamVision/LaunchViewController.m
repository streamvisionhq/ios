//
//  ViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "LaunchViewController.h"
#import "CoreDataManager.h"
#import "AuthenticationManager.h"
#import "ServiceProviderManager.h"
#import "DeviceDiscoveryViewController.h"
#import "User.h"


#define TEST 0

@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
  
 }


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self performSelector:@selector(tryAutoLogin) withObject:nil afterDelay:0.5];
}

-(void)tryAutoLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[defaults valueForKey:kIsLoggedIn] isEqualToString:kSVYes]) {
        // launch Main Screen
        [self authenticateServiceProvider];
    }
    else
    {
        // Launch Login Screen
        [self performSegueWithIdentifier:kLoginSegue sender:self];
        
    }

}

-(void)performSegueOnError:(NSString*)error
{
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    [self performSegueWithIdentifier:kLoginSegue sender:self];
}

-(void)authenticateCustomer
{
    //    if (self.userNameField.text != nil && self.userNameField.text.length > 0 ) {
    User *user = [[CoreDataManager sharedInstance]getUser];

    [SVProgressHUD showWithStatus:kWaitMessage];
    [[AuthenticationManager sharedInstance] setCallingViewControllerObj:self];
    [[AuthenticationManager sharedInstance] getRegisteredDevicesForId:user.registrationNumber Pass:user.password onSuccess:^(NSArray *devicesList){
        
        [[CoreDataManager sharedInstance] addRegisteredDevices:devicesList];
        
        NSLog(@"Success");
        
        NSArray *regDevices = [[CoreDataManager sharedInstance] getRegisteredDevices];
        
        if (regDevices.count > 0) {
            [SVProgressHUD dismiss];
            
            [self performSegueWithIdentifier:kDiscoverySegue sender:self];
            
        }else{
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:kSVNo forKey:kIsLoggedIn];
            [defaults synchronize];
            
            [SVProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Devices for user Id" message:@"The User Id you have entered does not have any devices registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            
        }
        
    }onFailure:^(NSError *error){
        
        NSLog(@"Error");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:kSVNo forKey:kIsLoggedIn];
        [defaults synchronize];
        [SVProgressHUD dismiss];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                        message:@"Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    
    
    
    
    //    }else{
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"try again" otherButtonTitles: nil];
    //        [alert show];
    //
    //    }
}

-(void)authenticateServiceProvider
{
    User *user = [[CoreDataManager sharedInstance]getUser];

    //[SVProgressHUD showWithStatus:kSPVerifyMessage];
    
    [[ServiceProviderManager sharedInstance] getServiceProvider:user.serviceProvider onSuccess:^(NSDictionary *devicesList)
     {
//             [SVProgressHUD dismiss];
         [self performSegueWithIdentifier:kLaunchToDeviceSegue sender:self];
         
     } onFailure:^(NSError *error) {
//             [SVProgressHUD dismiss];
         [self performSegueWithIdentifier:kLoginSegue sender:self];
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



@end
