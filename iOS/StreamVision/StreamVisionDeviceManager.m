//
//  StreamVisionDeviceManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "StreamVisionDeviceManager.h"
#import <ConnectSDK/AirPlayService.h>
#import <ConnectSDK/DIALService.h>
#import <ConnectSDK/WebOSTVService.h>
#include<unistd.h>
#include<netdb.h>
#include <arpa/inet.h>
#import "CoreDataManager.h"
#import "GBPing.h"
#import "NSTimer+GBToolbox.h"
#import "SVNetworkCheck.h"



@interface StreamVisionDeviceManager()<DiscoveryManagerDelegate>{
 
    DiscoveryManager *_discoveryManager;
    DevicePicker *_devicePicker;
    ConnectableDevice *_device;
    
     NSMutableDictionary *_devices;
     NSArray *_generatedDeviceList;
    dispatch_queue_t _sortQueue;
    
}
@end

@implementation StreamVisionDeviceManager

+(instancetype) sharedInstance
{
    static StreamVisionDeviceManager *_manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _manager = [ [ self alloc ] init ];
    });
    
    return _manager;
}


-(id) init{
    
    self = [super init];
    if (self) {
      /*  _discoveryManager = [DiscoveryManager sharedManager];
        
         _devices = [[NSMutableDictionary alloc] init];
        _sortQueue = dispatch_queue_create("Custom Device Picker Sort", DISPATCH_QUEUE_SERIAL);

        
        AirPlayServiceMode airPlayServiceMode = (AirPlayServiceMode) [[NSUserDefaults standardUserDefaults] integerForKey:@"airPlayServiceMode"];
        [AirPlayService setAirPlayServiceMode:airPlayServiceMode];
        
        NSString *dialAppName = [[NSUserDefaults standardUserDefaults] stringForKey:@"dialAppName"];
        [DIALService registerApp:dialAppName];
        
        _discoveryManager.pairingLevel = DeviceServicePairingLevelOn;*/
        //[_discoveryManager startDiscovery];
    }
    return self;
}


-(void) findDevice{
    
    _discoveryManager = [DiscoveryManager sharedManager];
    
    _devices = [[NSMutableDictionary alloc] init];
    _sortQueue = dispatch_queue_create("Custom Device Picker Sort", DISPATCH_QUEUE_SERIAL);
    
    
    AirPlayServiceMode airPlayServiceMode = (AirPlayServiceMode) [[NSUserDefaults standardUserDefaults] integerForKey:@"airPlayServiceMode"];
    [AirPlayService setAirPlayServiceMode:airPlayServiceMode];
    
    NSString *dialAppName = [[NSUserDefaults standardUserDefaults] stringForKey:@"dialAppName"];
    [DIALService registerApp:dialAppName];
    
    _discoveryManager.pairingLevel = DeviceServicePairingLevelOn;
    
    _discoveryManager.delegate = self;
    
    [[CoreDataManager sharedInstance] setAllDevicesOffline];
    [_discoveryManager startDiscovery];

    
}

-(void) stopFinding{
    
    [_discoveryManager stopDiscovery];
}


# pragma mark - DiscoveryManagerDelegate methods

- (void)discoveryManager:(DiscoveryManager *)manager didFindDevice:(ConnectableDevice *)device
{
    if (_devices)
    {
        

        @synchronized (_devices) { [_devices setObject:device forKey:device.address]; }
        
        [self sortDevices];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
             [[NSNotificationCenter defaultCenter] postNotificationName:kFoundDeviceNotification object:device];
        });
    }
}

- (void)discoveryManager:(DiscoveryManager *)manager didLoseDevice:(ConnectableDevice *)device
{
    if (_devices)
    {
        
                
        @synchronized (_devices) { [_devices removeObjectForKey:device.address]; }
        
        [self sortDevices];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           // if (_tableViewController)
             //   [_tableViewController.tableView reloadData];
            
            NSLog(@"Lost Connection");
            [[NSNotificationCenter defaultCenter] postNotificationName:kLostDeviceNotification object:device];
            
           
        });
    }
}


- (void) discoveryManager:(DiscoveryManager *)manager didFailWithError:(NSError*)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kFindDeviceFailureNotification object:nil];
    });
    
}

- (void)discoveryManager:(DiscoveryManager *)manager didUpdateDevice:(ConnectableDevice *)device
{
    if (_devices)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           // if (_tableViewController)
             //   [_tableViewController.tableView reloadData];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kFoundDeviceNotification object:device];
        });
    }
}

- (void) sortDevices
{
    dispatch_async(_sortQueue, ^{
        NSArray *devices;
        
        @synchronized (_devices) { devices = [_devices allValues]; }
        
        @synchronized (_generatedDeviceList)
        {
            _generatedDeviceList = [devices sortedArrayUsingComparator:^NSComparisonResult(ConnectableDevice *device1, ConnectableDevice *device2) {
                NSString *device1Name = [[self nameForDevice:device1] lowercaseString];
                NSString *device2Name = [[self nameForDevice:device2] lowercaseString];
                
                return [device1Name compare:device2Name];
            }];
        }
    });
}

- (NSString *) nameForDevice:(ConnectableDevice *)device
{
    NSString *name;
    
    if (device.serviceDescription.friendlyName && device.serviceDescription.friendlyName.length > 0)
        name = device.serviceDescription.friendlyName;
    else if (device.serviceDescription.address && device.serviceDescription.address.length > 0)
        name = device.serviceDescription.address;
    else
        name = [[NSBundle mainBundle] localizedStringForKey:@"Connect_SDK_Unnamed_Device" value:@"Unnamed device" table:@"ConnectSDK"];
    
    return name;
}

-(NSArray *) discoveredDvicesArray{
    
    return _generatedDeviceList;
}

-(void) pollForDevice{
    
   
    /*for(ConnectableDevice *dev in self.discoveredDvicesArray){
        
        
        NSLog(@"Device ; %@ , Ip : %@",dev.friendlyName, dev.address);
        
        BOOL canReach = [SVNetworkCheck networkConnected:dev.address Port:8085];
        
        if (canReach) {
            NSLog(@"Can Reach ! : %@",dev.address);
        }else{
            
            NSLog(@"Diconnect Device : %@",dev.address);
        }
        
        
    }
   
    */
    
    [[CoreDataManager sharedInstance] setAllDevicesOffline];
    [_discoveryManager stopDiscovery];
    [_discoveryManager startDiscovery];
    
    
}




-(void) sendRequest:(NSString *) hostName{
    
    NSURLRequest *request = [[NSURLRequest alloc] init];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error) {
                               
                    }];
}


-(void) wiFiChanged{
    
    [_discoveryManager stopDiscovery];
    [[CoreDataManager sharedInstance] setAllDevicesOffline];
    
}



@end
