//
//  ProgramGuideDateSelectionDelegate.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Delegation protocol to be implemented by clients wanting to be notified of the result of the date selection in the peogram guide
 */
@protocol ProgramGuideDateSelectionDelegate <UIActionSheetDelegate>

- (void) didSelectSegment:(NSInteger) index;
@end