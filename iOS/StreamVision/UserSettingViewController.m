//
//  UserSettingViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 18/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "UserSettingViewController.h"
#import "CoreDataManager.h"
#import "AuthenticationManager.h"
#import "AuthenticationManager.h"
#import "StreamVisionDeviceManager.h"
#import "ServiceProviderManager.h"

@interface UserSettingViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userIdField;
@property (weak, nonatomic) IBOutlet UITextField *serviceProviderField;
@property (weak, nonatomic) IBOutlet UITextField *passField;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) StreamVisionDeviceManager *deviceManager;
@end

@implementation UserSettingViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.userIdField setDelegate:self];
    self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    
    User *currentUser = [[CoreDataManager sharedInstance] getUser];
    self.userId = currentUser.registrationNumber;
    [self.userIdField setText:self.userId];
    [self.serviceProviderField setText:currentUser.serviceProvider];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismiss:(id)sender {

    [self dismissViewControllerAnimated:YES completion:^(void){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
    }];
}

- (IBAction)updatePressed:(id)sender {

    //Delete Devices which are registered
        [self.view endEditing:YES];
        [self authenticateServiceProvider];
    
//    [SVProgressHUD showWithStatus:kWaitMessage];
//    
//    
//    if (self.userIdField.text != nil && self.userIdField.text.length > 0 ) {
//        
//        
//        //get All Devices:  //@"26685"
//        [[AuthenticationManager sharedInstance] setCallingViewControllerObj:self];
//        [[AuthenticationManager sharedInstance] getRegisteredDevicesForId:self.userIdField.text onSuccess:^(NSArray *devicesList){
//            
//        NSLog(@"Success");
//            
//            
//            [[CoreDataManager sharedInstance] deleteCurrentUser];
//            
//            [[CoreDataManager sharedInstance] createUserWithRegistrationNumber:self.userIdField.text];
//            
//
//            if (devicesList.count > 0) {
//                
//                
//                [[CoreDataManager sharedInstance] deleteRegisteredDevices];
//                
//                [[CoreDataManager sharedInstance] addRegisteredDevices:devicesList];
//                
//                [[CoreDataManager sharedInstance] updateOnlineStatus:[self.deviceManager discoveredDvicesArray]];
//                
//
//                
//               [SVProgressHUD dismiss];
//              
//                [self dismissViewControllerAnimated:YES completion:^(void){
//                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                    [defaults setValue:kSVYes forKey:kIsLoggedIn];
//                    [defaults synchronize];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
//                    [self.delegate launchDeviceSelection];
//                    
//                }];
//                
//                
//            }else{
//
//                [SVProgressHUD dismiss];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Devices for user Id" message:@"The User Id you have entered does not have any devices registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//                
//                
//            }
//            
//        }onFailure:^(NSError *error){
//            
//            [SVProgressHUD dismiss];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
//                                                            message:@"Please try again later."
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//        }];
//        
//        
//        
//        
//    }else{
//        
//        [SVProgressHUD dismiss];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
//
}


-(void)authenticateCustomer
{
    //    if (self.userNameField.text != nil && self.userNameField.text.length > 0 ) {
    

    [SVProgressHUD showWithStatus:kWaitMessage];
    [[AuthenticationManager sharedInstance] setCallingViewControllerObj:self];
    [[AuthenticationManager sharedInstance] getRegisteredDevicesForId:self.userIdField.text Pass:_passField.text onSuccess:^(NSArray *devicesList){
        
        if (devicesList.count > 0) {
            
            [[CoreDataManager sharedInstance] deleteCurrentUser];

            [[CoreDataManager sharedInstance] createUserWithRegistrationNumber:self.userIdField.text andServiceProvider:self.serviceProviderField.text password:self.passField.text];
            
            [[CoreDataManager sharedInstance] deleteRegisteredDevices];
            
            [[CoreDataManager sharedInstance] addRegisteredDevices:devicesList];
            
            [[CoreDataManager sharedInstance] updateOnlineStatus:[self.deviceManager discoveredDvicesArray]];
            
            
            
            [SVProgressHUD dismiss];
            
            [self dismissViewControllerAnimated:YES completion:^(void){
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:kSVYes forKey:kIsLoggedIn];
                [defaults synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:kBeginPollingNotification object:nil];
                [self.delegate launchDeviceSelection];
                
            }];
            
            
        }else{
            
            [SVProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Devices for user Id" message:@"The User Id you have entered does not have any devices registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Invalid input, please double check and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            
        }
        
    }onFailure:^(NSError *error){
        
        NSLog(@"Error");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:kSVNo forKey:kIsLoggedIn];
        [defaults synchronize];
        [SVProgressHUD dismiss];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error"
                                                        message:@"Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    
    
    
    
    //    }else{
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"try again" otherButtonTitles: nil];
    //        [alert show];
    //
    //    }
}

-(void)authenticateServiceProvider
{
    if (self.userIdField.text.length <= 0 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kValidRegNoFailureMessage message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if(_serviceProviderField.text.length <= 0)
        [[[UIAlertView alloc] initWithTitle:kValidServiceProvider message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    else if (_passField.text.length <= 0)
        [[[UIAlertView alloc] initWithTitle:kValidPassword message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    else
    {
        [SVProgressHUD showWithStatus:kSPVerifyMessage];
        
        [[ServiceProviderManager sharedInstance] getServiceProvider:_serviceProviderField.text onSuccess:^(NSDictionary *devicesList)
         {
             [SVProgressHUD dismiss];
             [self authenticateCustomer];
             
         } onFailure:^(NSError *error) {
             [SVProgressHUD dismiss];
             
         }];
    }
    
    
}


-(void )doLogin
{
    [self updatePressed:_update];
}

#pragma mark UITextFieldDelegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
     self.userId = self.userIdField.text;
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (self.userIdField.text && self.userIdField.text.length > 0) {
         self.userId = self.userIdField.text;
    }
    
   
}






@end
