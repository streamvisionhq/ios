//
//  ChannelTableCell.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "ChannelTableCell.h"

@implementation ChannelTableCell

- (void)awakeFromNib {
    // Initialization code
    [self.contentView setBackgroundColor:[UIColor clearColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
