//
//  CoreDataManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 19/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllEntities.h"
#import <ConnectSDK/ConnectSDK.h>

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+(instancetype) sharedInstance;

- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectContext *) managedObjectContext;

-(void) createChannelsWithData:(NSArray *) channelsArray;
-(void) deleteAllChannelsData;
-(void) deleteAllShows;
-(NSArray *) getAllChannels;
-(void) createShowsForChannels:(NSArray *) channelAndShowsArray;
-(NSArray *) getChannelsForEPG;

- (void)saveContext;
- (NSURL*)applicationDocumentsDirectory;
- (void)deletAllData;
-(User *) getUser;
-(void) addDeviceToLocalStore:(ConnectableDevice *) device;
-(void) removeDeviceFromLocalStore:(ConnectableDevice *) device;
-(NSArray *) getAllDevices;
-(void) addRegisteredDevices:(NSArray *) devices;
-(void) deleteRegisteredDevices;
-(NSArray *) getRegisteredDevices;
-(void) deleteCurrentUser;
-(void) updateOnlineStatus:(NSArray *) discoveredDevices;
-(void) setAllDevicesOffline;
-(void) updateOfflineStatusForDevice:(ConnectableDevice *) device;
-(void) createUserWithRegistrationNumber:(NSString *)regNo andServiceProvider:(NSString *)spid password:(NSString*)pass;

@end
