//
//  SVNetworkCheck.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVNetworkCheck : NSObject

+(BOOL)hasConnectivity;
+(BOOL)networkConnected: (NSString *)ipAdress Port:(int)port;

@end
