//
//  TimeRange.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//
#import "TimeRange.h"
@interface TimeRange( )
@property (nonatomic, strong, readwrite) NSDate *date;
@property (nonatomic, assign, readwrite) NSTimeInterval duration;
@end

@implementation TimeRange
- (instancetype) initWithDate: (NSDate *) date duration: (NSTimeInterval) duration
{
    self = [ super init ];
    if( self )
    {
        _date = date;
        _duration = duration;
    }
    
    return self;
}

- (instancetype) init
{
    return [ self initWithDate: [ NSDate date ] duration: 0 ];
}
@end
