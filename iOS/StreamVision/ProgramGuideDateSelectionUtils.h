//
//  ProgramGuideDateSelectionUtils.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 28/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
 ProgramGuideDateSelectionDate shows date selction in EPG view
                        ------
 */

#import <Foundation/Foundation.h>

extern NSUInteger const ProgramGuideDateSelectionUtilsSegCapacity;

/**
 * Helper object to encapsulate the date and day of the week
 */
@interface ProgramGuideDateSelectionDate : NSObject
-(instancetype) initWithDayName: (NSString *) dayName date: (NSString *) date;
@property( nonatomic, strong, readonly ) NSString *dayName;
@property( nonatomic, strong, readonly ) NSString *date;
@end


/**
 *  Helper class used by the program guide's date selection UI to build the day names
 */
@interface ProgramGuideDateSelectionUtils : NSObject
+ (ProgramGuideDateSelectionDate *) segmentData:(NSInteger) index;
@end
