//
//  EPG.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Channel;

@interface EPG : NSManagedObject

@property (nonatomic, retain) NSString * epgValue;
@property (nonatomic, retain) NSString * error;
@property (nonatomic, retain) NSString * utcOffset;
@property (nonatomic, retain) NSString * observesDls;
@property (nonatomic, retain) NSString * startTimeUtc;
@property (nonatomic, retain) NSString * duration;
@property (nonatomic, retain) NSString * rawData;
@property (nonatomic, retain) Channel *channels;

@end
