//
//  AdPopOverViewController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdPopOverViewController : UIViewController

@property (nonatomic,retain) NSString *adURL;

@end
