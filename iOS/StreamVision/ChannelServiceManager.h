//
//  ChannelServiceManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

/*                      ------
        ChannelServiceManager is used to fetch all channels.
                        ------                                                     */

#import <Foundation/Foundation.h>

@interface ChannelServiceManager : NSObject

+(instancetype) sharedInstance;

-(void) getAllChannels: (void(^)(void))completionBlock
               onFailure: (ErrorBlock)failureBlock;



@end
