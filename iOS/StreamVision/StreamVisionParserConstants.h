//
//  StreamVisionParserConstants.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

//COMMON

extern NSString * const kSuccessKey;
extern NSString * const kDataKey;
extern NSString * const kMessageKey;


//CHANNEL Parser Keys

extern NSString * const kChannelNumberKey;
extern NSString * const kChannelDescriptionKey;
extern NSString * const kChannelIdKey;
extern NSString * const kChannelLogoKey;

//SHOW parser keys


extern NSString * const kChannelShowsParserKey;
extern NSString * const kStartTimeUtcKey;
extern NSString * const kEndTimeUtcKey;
extern NSString * const kTitleKey;
extern NSString * const kDescriptionKey;
extern NSString * const kShowTypeKey;
extern NSString * const kCastKey;
extern NSString * const kDisplayGenreKey;
extern NSString * const kRatingKey;
extern NSString * const kVideoURLKey;

extern NSString * const kDeviceIdKey;
extern NSString * const kisRegisteredIdKey;
extern NSString * const kServiceProviderKey;