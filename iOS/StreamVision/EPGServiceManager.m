//
//  EPGServiceManager.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 04/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "EPGServiceManager.h"
#import "EPGParser.h"
#import "AFHTTPRequestOperation.h"
#import "SVNetworkCheck.h"
#import "Utils.h"
#import "ServiceProviderManager.h"

//static NSString *const kEPGEndPoint = @"http://106.51.226.151/sv/code/api/epg?";
//@"http://106.51.226.151/svapi/public/epg?";
static NSString *const kEPGEndPoint = @"api/epg?";

//http://106.51.226.151/svapi/public/epg?startTimeUTC=2015-06-03T07:00:00&endTimeUTC=2015-06-03T10:00:00

@interface EPGServiceManager()

@property (nonatomic,strong) EPGParser *parser;
@property (nonatomic,strong) NSDateFormatter *epgServiceDateFormatter;

@end

@implementation EPGServiceManager

+(instancetype) sharedInstance
{
    static EPGServiceManager *_manager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _manager = [ [ self alloc ] init ];
    });
    
    return _manager;
}

-(NSDateFormatter *) epgServiceDateFormatter{
    
    if(!_epgServiceDateFormatter){
       
        _epgServiceDateFormatter = [[NSDateFormatter alloc] init];
        [_epgServiceDateFormatter setDateFormat:@"yyyy-MM-dd'T'00:00:00"];
        
    }
    
    return _epgServiceDateFormatter;
}


-(EPGParser *) parser{
    
    if (!_parser) {
        _parser = [[EPGParser alloc] init];
    }
    
    return _parser;
    
}

-(void) getEPGForStartDate:(NSDate *) startDate andEndDate:(NSDate *) endDate onSuccess:(void(^)(void))completionBlock onFailure: (ErrorBlock)failureBlock{
    
    NSString *startDateStr = [self.epgServiceDateFormatter stringFromDate:startDate];
    NSString *endDateStr = [self.epgServiceDateFormatter stringFromDate:endDate];
    
    NSString *requestString = [NSString stringWithFormat:@"%@%@startTimeUTC=%@&endTimeUTC=%@",[ServiceProviderManager getCMSEndPoint],kEPGEndPoint,startDateStr,endDateStr];
    
   // NSString *encodedRequest =[Utils getEncodedString:requestString];
    
    
    [self performRequest:requestString success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if( completionBlock )
        {
            
            [self.parser parseEPGData:(NSData *)responseObject];
        
            completionBlock();
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failureBlock)
        {
            failureBlock( error );
        }
        
    }];
    

}



- (void)performRequest: (NSString *) requestString
               success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    if ([self checkIfNetworkIsReachable]) {
        
        //Proceed
        
    }else{
        
        [SVProgressHUD showErrorWithStatus:kNoNetworkMessage maskType:SVProgressHUDMaskTypeGradient];
        
        
        return;
        
        
    }
    
    NSURL *url = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    
}


-(BOOL)checkIfNetworkIsReachable{
    
    return [SVNetworkCheck hasConnectivity];
}





@end
