//
//  StreamVisionDeviceManager.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ConnectSDK/ConnectSDK.h>

@interface StreamVisionDeviceManager : NSObject

+(instancetype) sharedInstance;
-(void) findDevice;
-(void) stopFinding;
-(NSArray *) discoveredDvicesArray;
- (NSString *) nameForDevice:(ConnectableDevice *)device;

-(void) pollForDevice;
-(void) wiFiChanged;

@end
