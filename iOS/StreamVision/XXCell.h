//
//  XXCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramGuideCell.h"
#import "ProgramGuideChannelView.h"

@interface XXCell : ProgramGuideCell
+ (NSString *) reuseIdentifier;
@end


@interface XXChannelCell : ProgramGuideChannelView
@property (nonatomic, assign) NSUInteger sectionIndex;
+ (NSString *) reuseIdentifier;
@end