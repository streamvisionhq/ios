//
//  ServiceProviderManager.h
//  StreamVision
//
//  Created by irfan on 24/01/2016.
//  Copyright (c) 2016 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceProvider.h"

@interface ServiceProviderManager : NSObject

@property (strong, nonatomic) ServiceProvider *serviceProvider;

+(instancetype) sharedInstance;
+(NSString *)getCMSEndPoint;

-(void) getServiceProvider:(NSString *)spId onSuccess:(void(^)(NSDictionary *json))completionBlock onFailure: (ErrorBlock)failureBlock;


@end
