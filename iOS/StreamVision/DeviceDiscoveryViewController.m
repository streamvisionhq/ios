//
//  DeviceDiscoveryViewController.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 23/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "DeviceDiscoveryViewController.h"
#import "MainViewController.h"
#import "MainNavigationController.h"
#import "CoreDataManager.h"
#import "StreamVisionDeviceManager.h"
#import "AuthenticationManager.h"
#import "RokuListingTableCell.h"
#define TIMER_LIMIT 20
#import "SVConnectedDevicesManager.h"
#import "SVNetworkCheck.h"

static NSString *CellIdentifier = @"RokuListingCell";

@interface DeviceDiscoveryViewController ()<UITableViewDelegate,UITableViewDataSource,ConnectableDeviceDelegate,ListingCellDelegate>{
    
    NSArray *generatedDeviceList;
       
}

@property (nonatomic,strong) StreamVisionDeviceManager *deviceManager;
@property (weak, nonatomic) IBOutlet UITableView *devicesTableView;
@property (nonatomic, strong) ConnectableDevice *selectedDevice;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property BOOL connectAgainToSameDevice;
@property (nonatomic,strong)  NSString *lastCOnnectedDevice;
@property (nonatomic,strong) NSMutableArray *allRegisteredDevices;
@property (nonatomic,strong) NSTimer *discoveryTimer;
@property NSUInteger timeoutCounter;
@property (nonatomic,strong) NSString *deviceIdForButtonTitle;
@property NSUInteger selectedRow;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic,strong) SVConnectedDevicesManager *svDeviceManager;
@end

@implementation DeviceDiscoveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.svDeviceManager = [SVConnectedDevicesManager sharedInstance];
    self.selectedRow = -1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newDeviceFound:) name:kFoundDeviceNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(errorFindingDevice:) name:kFindDeviceFailureNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wiFiHadChanged:) name:kWiFiChangedNotification object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   self.lastCOnnectedDevice = [defaults valueForKey:kCurrentSelectedDeviceIdKey];
    
    if (self.lastCOnnectedDevice) {
        NSLog(@"Do Not Show Table View");
        self.connectAgainToSameDevice = YES;
        [self.backButton setEnabled:NO];
        //[self.devicesTableView setHidden:YES];
        
    }else{
        self.connectAgainToSameDevice = NO;
    }
   
    CoreDataManager *coreManager = [CoreDataManager sharedInstance];
    self.allRegisteredDevices = [NSMutableArray arrayWithArray:[coreManager getRegisteredDevices]];
    [self.devicesTableView reloadData];
    
    if (self.allRegisteredDevices.count == 0) {
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"You have no registered devices" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [errorAlert show];
    }
    
    
    
    
}




-(void) startTimer{
    
    self.timeoutCounter = 0;
  self.discoveryTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                     target: self
                                   selector: @selector(isDiscoveryTimedOut:)
                                   userInfo: nil
                                    repeats: YES];

}


-(void)isDiscoveryTimedOut:(NSTimer *)timer {

    self.timeoutCounter++;
    if (self.timeoutCounter > TIMER_LIMIT) {
        
        [self.discoveryTimer invalidate];
        [SVProgressHUD dismiss];
        if(! self.connectAgainToSameDevice){
        [[CoreDataManager sharedInstance] setAllDevicesOffline];
        [self.devicesTableView reloadData];
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"No registered devices found in your network" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [errorAlert show];
            
//            [self performSegueWithIdentifier:kDeviceToMainSegue sender:self];
        }

    }
    
}



-(void) startFindingDevices{
    bool comnnectivity = [SVNetworkCheck hasConnectivity];
    if (comnnectivity == false)
    {
        [SVProgressHUD dismiss];
        
        //  [SVProgressHUD showErrorWithStatus:kNoNetworkMessage maskType:SVProgressHUDMaskTypeGradient];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kConnectionUnAvailable message:nil delegate:self cancelButtonTitle:@"try again" otherButtonTitles: @"quit",nil];
        [alert show];
        return;
    }
    
    [[CoreDataManager sharedInstance] setAllDevicesOffline];
    if (self.connectAgainToSameDevice) {
        [SVProgressHUD showWithStatus:@"Connecting to device"];
    }else{
    [SVProgressHUD showWithStatus:kFindDeviceMessage];
    }
    [self startTimer];
       self.deviceManager = [StreamVisionDeviceManager sharedInstance];
    
    [self.deviceManager findDevice];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"try again"])
    {
        [SVProgressHUD showWithStatus:@"Connecting to network . . ."];
        [self performSelector:@selector(startFindingDevices) withObject:nil afterDelay:3];
    }
    else if([title isEqualToString:@"quit"])
    {
        exit(0);
    }
}

-(void) viewDidAppear:(BOOL)animated{
    
    if (self.allRegisteredDevices.count > 0) {
        
        [self startFindingDevices];
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:kLoginSegue] == false) {
        MainNavigationController *destinationNAVController = (MainNavigationController *)segue.destinationViewController;
        MainViewController *destination = (MainViewController *)[destinationNAVController.viewControllers firstObject];
        destination.device = self.selectedDevice;
        destination.connectButtonTitle = self.deviceIdForButtonTitle;
    }else
    {
    }
    

}



-(void) newDeviceFound:(NSNotification *) note{
    
    NSLog(@"Found Device");
    
    generatedDeviceList = [self.deviceManager discoveredDvicesArray];
    
    //[self filterFoundDeviceList];
    
    if (self.connectAgainToSameDevice) {
        
        for(ConnectableDevice *dev in generatedDeviceList){
            
            
            NSArray *servicesArray = dev.services;
            DeviceService *service = [servicesArray objectAtIndex:0];
            ServiceDescription *serviceDesc = service.serviceDescription;
            NSLog(@"UUID : %@",serviceDesc.UUID);

            
            if ([serviceDesc.UUID containsString:self.lastCOnnectedDevice]) {
                //connect to Device
                
               
                
                CoreDataManager *shardManager = [CoreDataManager sharedInstance];
                NSArray *allDevices = [shardManager getAllDevices];
                NSArray *servicesArray = dev.services;
                DeviceService *service = [servicesArray objectAtIndex:0];
                ServiceDescription *serviceDesc = service.serviceDescription;
               // NSLog(@"UUID : %@",serviceDesc.UUID);
                

                
                for(Device *device in allDevices){
                
                    if ([serviceDesc.UUID containsString:device.deviceIdentifier]) {
                        
                       [self.svDeviceManager setConnectedSVDevice:device];
                        [self.svDeviceManager setConnectedRokuDevice:dev];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setValue:device.deviceIdentifier forKey:kCurrentSelectedDeviceIdKey];
                        [defaults synchronize];
                        
                        break;
                    }
                }

                
                [self.discoveryTimer invalidate];
                //[self.deviceManager stopFinding];
                
                self.selectedDevice = dev;
                self.selectedDevice.delegate = self;
                
               
                
                
                /* When pairing level is on, by default pairing type is DeviceServicePairingTypeFirstScreen.You can also set pairing type to DeviceServicePairingTypePinCode/DeviceServicePairingTypeMixed */
                [self.selectedDevice setPairingType:DeviceServicePairingTypeFirstScreen];
                [self.selectedDevice connect];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:kFoundDeviceNotification object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:kFindDeviceErrorMessage object:nil];
                
                break;
            }
        }
        
    }else{
    
        [self.allRegisteredDevices removeAllObjects];
        CoreDataManager *coreManager = [CoreDataManager sharedInstance];
        self.allRegisteredDevices = [NSMutableArray arrayWithArray:[coreManager getRegisteredDevices]];
        [self.devicesTableView reloadData];
        
    }
}

-(void) errorFindingDevice:(NSNotification *) note{
    
    NSLog(@"Error Finding Device");
    
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:kFindDeviceErrorMessage message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [errorAlert show];
    
}

-(void)wiFiHadChanged:(NSNotification *) note{
 
    [self.deviceManager wiFiChanged];
    [self.discoveryTimer invalidate];
    
    CoreDataManager *coreManager = [CoreDataManager sharedInstance];
    self.allRegisteredDevices = [NSMutableArray arrayWithArray:[coreManager getRegisteredDevices]];
    [self.devicesTableView reloadData];

    
     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Wi Fi Network has changed" message:@"Please wait while we check for devices on this network." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [errorAlert show];
    
    
     [self startFindingDevices];
    
}


#pragma mark UITableViewDelegate methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Device *toConnect = [self.allRegisteredDevices objectAtIndex:indexPath.row];
    
    if([toConnect.isOnline boolValue]){
    
    self.selectedRow = indexPath.row;
    [self.devicesTableView reloadData];
    
  
    [self.svDeviceManager setConnectedSVDevice:toConnect];
    self.deviceIdForButtonTitle = toConnect.deviceLocation;
    
    self.selectedDevice = [self getConnectableDeviceForIdentifier:toConnect.deviceIdentifier];
   
    
    if (self.selectedDevice) {
        
    [self.svDeviceManager setConnectedRokuDevice:[self getConnectableDeviceForIdentifier:toConnect.deviceIdentifier]];
   
   // [self.deviceManager stopFinding];
    
    [SVProgressHUD showWithStatus:kPairingDeviceMessage];
    
    
    self.selectedDevice.delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:toConnect.deviceIdentifier forKey:kCurrentSelectedDeviceIdKey];
    [defaults synchronize];
    
    
    /* When pairing level is on, by default pairing type is DeviceServicePairingTypeFirstScreen.You can also set pairing type to DeviceServicePairingTypePinCode/DeviceServicePairingTypeMixed */
    [self.selectedDevice setPairingType:DeviceServicePairingTypeFirstScreen];
    [self.selectedDevice connect];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFoundDeviceNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFindDeviceErrorMessage object:nil];
        
    }
        
    }


}



#pragma mark UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    numberOfRows = self.allRegisteredDevices.count;
   
    return numberOfRows;
}

static NSString *cellIdentifier = @"DeviceDiscoveryCell";

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    RokuListingTableCell *cell = (RokuListingTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Device *aDevice = [self.allRegisteredDevices objectAtIndex:indexPath.row];
    
    cell.identfier.text = aDevice.deviceLocation;
    cell.deviceId.text = aDevice.deviceIdentifier;
    cell.serviceType.text = aDevice.serviceType;
    cell.registered.text = aDevice.accountId;
    
    [cell.onlineImageView setImage:[UIImage imageNamed:@"off"]];
    [cell.selectButton setEnabled:NO];
    [cell setUserInteractionEnabled:YES];
    
    
    if ([aDevice.isOnline boolValue]) {
        NSLog(@"ONLINE :%@",aDevice.deviceIdentifier);
        [cell.onlineImageView setImage:[UIImage imageNamed:@"on"]];
        [cell.selectButton setEnabled:YES];
        [cell setUserInteractionEnabled:YES];
        [self.discoveryTimer invalidate];
        [SVProgressHUD dismiss];
    }else{
        
        [cell.selectButton setEnabled:NO];
        [cell setUserInteractionEnabled:NO];
        
        
        
    }
   
    if (indexPath.row == self.selectedRow) {
        [cell.selectButton setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    }else{
        [cell.selectButton setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    }
    
    [cell.selectButton setTag:indexPath.row];
    [cell setBackgroundColor:[UIColor whiteColor]];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}

#pragma mark - ConnectableDeviceDelegate

- (void) connectableDeviceReady:(ConnectableDevice *)device
{
   
}

- (void) connectableDevice:(ConnectableDevice *)device service:(DeviceService *)service pairingRequiredOfType:(int)pairingType withData:(id)pairingData
{
    if (pairingType == DeviceServicePairingTypeAirPlayMirroring)
        [(UIAlertView *) pairingData show];
}

- (void) connectableDevice:(ConnectableDevice *)device connectionFailedWithError:(NSError *)error{
    
    
     NSLog(@"Connection Failed");
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Unable to Connect to Device, please check your network." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [errorAlert show];

}


- (void) connectableDeviceConnectionSuccess:(ConnectableDevice*)device forService:(DeviceService *)service{
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                       [defaults setValue:kSVYes forKey:kIsLoggedIn];
                       [defaults synchronize];
                       [SVProgressHUD dismiss];
                       [self performSegueWithIdentifier:kDeviceToMainSegue sender:self];
                   });

    
}

- (void) connectableDeviceDisconnected:(ConnectableDevice *)device withError:(NSError *)error
{
    NSLog(@"Connection Disconnected");
    [[NSNotificationCenter defaultCenter] postNotificationName:kLostDeviceNotification object:device];
    
    self.selectedDevice.delegate = nil;
    self.selectedDevice = nil;
    
   }

- (IBAction)refreshList:(id)sender {
     [self.deviceManager stopFinding];
     [self startFindingDevices];
}


-(void) selectedDeviceAtIndex:(NSUInteger)index{
    
    Device *toConnect = [self.allRegisteredDevices objectAtIndex:index];
    
    if([toConnect.isOnline boolValue]){
    
     self.deviceIdForButtonTitle = toConnect.deviceIdentifier;
    
    self.selectedDevice = [self getConnectableDeviceForIdentifier:toConnect.deviceIdentifier];
    
   
    if (self.selectedDevice) {
        
        
        //[self.deviceManager stopFinding];
        
        [SVProgressHUD showWithStatus:kPairingDeviceMessage];
        
        
        self.selectedDevice.delegate = self;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:toConnect.deviceIdentifier forKey:kCurrentSelectedDeviceIdKey];
        [defaults synchronize];
        
        
        /* When pairing level is on, by default pairing type is DeviceServicePairingTypeFirstScreen.You can also set pairing type to DeviceServicePairingTypePinCode/DeviceServicePairingTypeMixed */
        [self.selectedDevice setPairingType:DeviceServicePairingTypeFirstScreen];
        [self.selectedDevice connect];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kFoundDeviceNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kFindDeviceErrorMessage object:nil];
        
    }else{
        
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Device not online" message:@"Please select a device which is available in your network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [errorAlert show];
        
    }
        
    }
    
}

-(ConnectableDevice *) getConnectableDeviceForIdentifier:(NSString *) deviceId{
    
    ConnectableDevice *returnVal = nil;
    
    for(ConnectableDevice *dev in generatedDeviceList){
        
        NSArray *servicesArray = dev.services;
        DeviceService *service = [servicesArray objectAtIndex:0];
        ServiceDescription *serviceDesc = service.serviceDescription;
        NSLog(@"UUID : %@",serviceDesc.UUID);

        
        if ([serviceDesc.UUID containsString:deviceId]) {
            returnVal = dev;
            break;
        }
    }
    
  
    
    return returnVal;

}
- (IBAction)goBack:(id)sender {
    
    [self.deviceManager stopFinding];
    [self.discoveryTimer invalidate];
    [SVProgressHUD dismiss];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       [defaults setValue:kSVNo forKey:kIsLoggedIn];
       [defaults synchronize];
    }];
    
}

@end
