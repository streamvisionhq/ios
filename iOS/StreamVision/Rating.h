//
//  Rating.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 20/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Show;

@interface Rating : NSManagedObject

@property (nonatomic, retain) NSString * tv;
@property (nonatomic, retain) Show *show;

@end
