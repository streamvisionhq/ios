//
//  MainNavigationController.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 26/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainNavigationController : UINavigationController

@end
