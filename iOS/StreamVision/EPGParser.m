//
//  EPGParser.m
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 29/05/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import "EPGParser.h"
#import "Channel.h"
#import "Show.h"
#import "CoreDataManager.h"
#import "StreamVisionParserConstants.h"

@implementation EPGParser




-(void) parseEPGData:(NSData *) data{
    
    CoreDataManager *coreDataManager = [CoreDataManager sharedInstance];
   // [coreDataManager deleteAllShows];
    
    
    NSError *error;
    NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray *allChannelsData = nil;
    BOOL success = [[JSONResponseDict valueForKey:kSuccessKey] boolValue];
    
    if (success) {
        allChannelsData = [JSONResponseDict valueForKey:kDataKey];
        [coreDataManager createShowsForChannels:allChannelsData];
        
    }else{
        
       
        
    }

    
    
}

@end
