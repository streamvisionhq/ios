//
//  AppCollectionViewCell.h
//  StreamVision
//
//  Created by Eshwaran Balasubramanyam on 03/06/15.
//  Copyright (c) 2015 TernionSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
