//
//  ServiceProvider.m
//  StreamVision
//
//  Created by irfan on 24/01/2016.
//  Copyright (c) 2016 TernionSoft. All rights reserved.
//

#import "ServiceProvider.h"

@implementation ServiceProvider

-(void)set:(NSDictionary *)object
{
    self.spId = object[@"idServiceProvider"];
    self.heading = object[@"heading"];
    self.cmsUrl = object[@"cmsURL"];
    self.logoUrl = object[@"logo"];
}

@end
